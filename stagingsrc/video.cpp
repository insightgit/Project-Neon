/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../stagingsrc/video.h"

namespace jetfuel {
    namespace media {
        Video::Video() {
            std::string videocachepathstring;

            av_register_all();

            if(NEON_COMPILED_OS == "Windows"){
                // TODO(Bobby): Implement detection of current working directory
                // on Windows.
            }else{
                struct stat statstruct = {0};
                char currentpath[FILENAME_MAX];
                if(!getcwd(currentpath, sizeof(currentpath))){
                    throw exceptions::Get_cwd_exception();
                }

                // TODO(Bobby): Implement Video Boost UUID sub-folders.
                videocachepathstring = std::string()+currentpath+
                                            "/.videocache";

            }

            m_videocachepath = boost::filesystem::path(videocachepathstring);

            if(!boost::filesystem::is_directory(m_videocachepath)){
                boost::filesystem::create_directory(m_videocachepath);
            }
        }

        Video::~Video() {
            boost::filesystem::remove_all(m_videocachepath);

            if(Get_codec_context() != nullptr){
                avcodec_close(Get_codec_context());
            }

            exitsignalsent = true;
        }

        int Video::Video_audio_decode_frame(
         Video_state *videostate, uint8_t *audiobuffer,
         size_t buffersize, double *ptspointer){
            static AVPacket *packet;
            static uint8_t *audiopacketdata;
            static int audiopacketsize = 0;
            static AVFrame *frame;
            double pts;

            int length1;
            int datasize = 0;
            int ptsnumber;

            while(true){
                while(audiopacketsize > 0){
                    int hasgotframe = 0;

                    length1 = avcodec_decode_audio4(
                                   videostate->audiocodeccontext, frame,
                                                    &hasgotframe, packet);
                    if(length1 < 0){
                        audiopacketsize = 0;
                        break;
                    }

                    if(packet->pts != AV_NOPTS_VALUE){
                        videostate->audioclock = av_q2d(videostate->
                                   audiostream->time_base)*packet->pts;
                    }

                    audiopacketdata += length1;
                    audiopacketsize -= length1;
                    datasize = 0;

                    if(hasgotframe){
                        datasize = av_samples_get_buffer_size(nullptr,
                                   videostate->audiocodeccontext->channels,
                                   frame->nb_samples,
                                   videostate->audiocodeccontext->sample_fmt, 1);
                        if(datasize > buffersize){
                            throw
                            exceptions::Video_audio_buffer_too_small_exception();
                        }else{
                            std::memcpy(audiobuffer, frame->data[0], datasize);
                        };
                    }

                    if(datasize <= 0){
                        continue;
                    }

                    pts = videostate->audioclock;
                    *ptspointer = pts;
                    ptsnumber = videostate->audiostream->codec->channels * 2;
                    videostate->audioclock = static_cast<double>(datasize) /
                            static_cast<double>(ptsnumber * videostate->audiostream->
                                                codec->sample_rate);

                    return datasize;
                }

                if(packet->data != nullptr){
                    av_free_packet(packet);
                }

                if(exitsignalsent){
                    return -1;
                }

                if(Video::Get_audio_packet_queue().get(packet, 1) < 0){
                    return -1;
                }

                audiopacketdata = packet->data;
                audiopacketsize = packet->size;
            }
        }

        void Video::Video_audio_sdl_callback(void *userdata, Uint8 *stream, int len){
            Video_state *videostate =
                    static_cast<Video_state*>(userdata);
            int length1;
            int audiosize;
            double pts;

            static uint8_t audiobuffer[(MAX_AUDIO_FRAME_SIZE * 3) / 2];
            static unsigned int audiobuffersize = 0;
            static unsigned int audiobufferindex = 0;

            while(len > 0){
                if(audiobufferindex >= audiobuffersize){
                    audiosize = Video_audio_decode_frame(
                                  videostate, audiobuffer,
                                  sizeof(audiobuffer), &pts);

                    if(audiosize < 0){
                        audiobuffersize = 1024;
                        memset(audiobuffer, 0, audiobuffersize);
                    }else{
                        audiobuffersize = audiosize;
                    }

                    audiobufferindex = 0;
                    length1 = audiobuffersize - audiobufferindex;

                    if(length1 > len){
                        length1 = len;
                    }

                    std::memcpy(stream,
                     static_cast<uint8_t*>(audiobuffer + audiobufferindex),
                                length1);
                    len -= length1;
                    stream += length1;
                    audiobufferindex += length1;
                }
            }
        }

        bool Video::Packet_queue::put(AVPacket *packet){
            AVPacketList *packetlist1;

            if(av_dup_packet(packet) < 0){
                Set_last_error(std::string("Unable to allocate an AVPacket.")+
                               "Has this computer run out of memory?");
                return false;
            }

            packetlist1 = av_malloc(sizeof(AVPacketList));

            if(packetlist1 == nullptr){
                Set_last_error("Unable to allocate an AVPacketList.");
                return false;
            }

            packetlist1->pkt = *packet;
            packetlist1->next = nullptr;

            if(SDL_LockMutex(mutex) < 0){
                Set_last_error(std::string("Unable to lock PacketQueue")+
                  " SDL_Mutex. Error reported from SDL was: "+ SDL_GetError());
                return false;
            }

            if(lastpacket == nullptr){
                firstpacket = packetlist1;
            }else{
                lastpacket->next = packetlist1;
            }

            lastpacket = packetlist1;
            ++packetnumber;
            size += packetlist1->pkt.size;

            if(SDL_CondSignal(cond) < 0){
                Set_last_error(std::string("Unable to restart threads waiting")+
                 " on SDL condition variable. Error reported from SDL: "+
                               SDL_GetError());
                return false;
            }

            if(SDL_UnlockMutex(mutex) < 0){
                Set_last_error(std::string("Unable to unlock PacketQueue")+
                  " SDL_Mutex. Error reported from SDL was: "+ SDL_GetError());
                return false;
            }

            return true;
        }

        int Video::Packet_queue::get(AVPacket *packet, const bool block){
            AVPacketList *packetlist1;
            int returnvalue;

            if(SDL_LockMutex(mutex) < 0){
                Set_last_error(std::string("Unable to lock PacketQueue")+
                  " SDL_Mutex. Error reported from SDL was: "+ SDL_GetError());
                return false;
            }

            while(true){
                if(exitsignalsent){
                    returnvalue = -1;
                    break;
                }

                packetlist1 = firstpacket;

                if(packetlist1 != nullptr){
                    firstpacket = packetlist1->next;

                    if(firstpacket == nullptr){
                        lastpacket = nullptr;
                    }

                    --packetnumber;
                    size -= packetlist1->pkt.size;
                    *packet = packetlist1->pkt;
                    av_free(packetlist1);

                    returnvalue = 1;
                    break;
                }else if(!block){
                    returnvalue = 0;
                    break;
                }else{
                    SDL_CondWait(cond, mutex);
                }
            }

            SDL_UnlockMutex(mutex);
            return returnvalue;
        }

        int Video::Find_video_stream(Video_state *videostate){
            int returnvalue = -1;

            for(int i = 0; videostate->videoformatcontext->nb_streams > i; ++i){
                if(videostate->videoformatcontext->streams[i]->codec->codec_type
                   == AVMEDIA_TYPE_VIDEO && returnvalue < 0){
                    returnvalue = i;
                    break;
                }
            }

            return returnvalue;
        }

        int Video::Find_audio_stream(Video_state *videostate){
            int returnvalue = -1;

            for(int i = 0; videostate->videoformatcontext->nb_streams > i; ++i){
                if(videostate->videoformatcontext->streams[i]->codec->codec_type
                   == AVMEDIA_TYPE_AUDIO && returnvalue < 0){
                    returnvalue = i;
                    break;
                }
            }

            return returnvalue;
        }

        bool Video::Save_video_frame(AVFrame *frame, int width, int height,
                                          int framenum){
            FILE *videoframefile;

            std::string filename = Get_video_cache_path()+"/frame_"+
                                   std::to_string(framenum)+".ppm";
            char filenamesize[filename.size()];

            // Open file
            sprintf(filenamesize, filename.c_str());
            videoframefile=fopen(filenamesize, "wb");
            if(videoframefile == nullptr){
                Set_last_error(std::string()+"Unable to open video frame file:"+
                               filename);
                return false;
            }

            // Write header
            fprintf(videoframefile, "P6\n%d %d\n255\n", width, height);

            // Write pixel data
            for(int y = 0; y<height; ++y){
                fwrite(frame->data[0]+y*frame->linesize[0], 1, width*3,
                       videoframefile);
            }

            // Close file
            fclose(videoframefile);

            return true;
        }

        bool Video::Allocate_pixel_buffers(Video_state *videostate,
                                           Uint8 *yplane, Uint8 *uplane,
                                           Uint8 *vplane){
            size_t yplanesize;
            size_t uvplanesize;
            yplanesize = videostate->videocodeccontext->width *
                    videostate->audiocodeccontext->height;
            uvplanesize = videostate->videocodeccontext->width *
                    videostate->videocodeccontext->height / 4;

            yplane = (Uint8*)malloc(yplanesize);
            uplane = (Uint8*)malloc(uvplanesize);
            vplane = (Uint8*)malloc(uvplanesize);

            if(yplane == nullptr && uplane == nullptr && vplane == nullptr){
                Set_last_error("Could not allocate pixel buffers.");
                return false;
            }else{
                return true;
            }
        }

        void Video::Setup_av_picture_to_draw(Video_state *videostate,
                                             AVPicture *picturetodraw,
                                             Uint8 *yplane, Uint8 *uplane,
                                             Uint8 *vplane, int uvpitch){
            picturetodraw->data[0] = yplane;
            picturetodraw->data[1] = uplane;
            picturetodraw->data[2] = vplane;
            picturetodraw->linesize[0] =
             videostate->videostream->codec->width;
            picturetodraw->linesize[1] = uvpitch;
            picturetodraw->linesize[2] = uvpitch;
        }

        void Video::Allocate_video_frame(Video_state *videostate){
            Video_picture *currentvideopicture =
             &videostate->picturequeue[videostate->picturequeuewriteindex];


            if(currentvideopicture->video != nullptr){
                SDL_DestroyTexture(currentvideopicture->video);
            }



            SDL_LockMutex(videostate->picturequeuemutex);


            SDL_CreateTexture(videostate->renderer,
                              SDL_PIXELFORMAT_YV12,
                              SDL_TEXTUREACCESS_STREAMING,
                              videostate->videostream->codec->width,
                              videostate->videostream->codec->height);


            SDL_UnlockMutex(videostate->picturequeuemutex);

            currentvideopicture->size.x = videostate->videostream->codec->width;
            currentvideopicture->size.y = videostate->videostream->codec->height;
            currentvideopicture->allocated = true;
        }

        bool Video::Queue_video_frame(Video_state *videostate,
                                      AVFrame *currentframe,
                                      double pts){
            Video_picture *currentvideopicture;
            int dst_pix_fmt;
            AVPicture currentpicture;

            if(SDL_LockMutex(videostate->picturequeuemutex) < 0){
                Set_last_error(std::string("Unable to lock picturequeuemutex")+
                  " SDL_Mutex. Error reported from SDL was: "+ SDL_GetError());
                return false;
            };

            while(videostate->picturequeuesize >= VIDEO_PICTURE_QUEUE_SIZE &&
                  exitsignalsent){
                SDL_CondWait(videostate->picturequeueconditionvariable,
                             videostate->picturequeuemutex);
            }

            SDL_UnlockMutex(videostate->picturequeuemutex);

            if(exitsignalsent){
                return false;
            }

            currentvideopicture = &videostate->picturequeue[
                                  videostate->picturequeuewriteindex];

            if(currentvideopicture->video == nullptr ||
               currentvideopicture->size.x !=
               videostate->videostream->codec->height){
                currentvideopicture->allocated = false;
                Allocate_video_frame(videostate);
            }

            if(currentvideopicture->video != nullptr){
                Uint8 *yplane;
                Uint8 *uplane;
                Uint8 *vplane;
                int uvpitch = videostate->videostream->codec->width / 2;
                AVPicture *picturetodraw;

                Allocate_pixel_buffers(videostate, yplane, uplane, vplane);
                Setup_av_picture_to_draw(videostate, picturetodraw, yplane,
                                         uplane, vplane, uvpitch);

                sws_scale(videostate->swscontext,
                        const_cast<uint8_t const * const *>(currentframe->data),
                        currentframe->linesize, 0,
                        videostate->videostream->codec->height, picturetodraw->data,
                        picturetodraw->linesize);

                SDL_LockMutex(videostate->picturequeuemutex);

                SDL_UpdateYUVTexture(currentvideopicture->video, nullptr, yplane,
                                     videostate->videostream->codec->width,uplane,
                                     uvpitch,vplane,uvpitch);

                currentvideopicture->pts = pts;

                if(++videostate->picturequeuewriteindex == VIDEO_PICTURE_QUEUE_SIZE){
                    videostate->picturequeuewriteindex = 0;
                }

                ++videostate->picturequeuesize;

                SDL_UnlockMutex(videostate->picturequeuemutex);

                free(yplane);
                free(uplane);
                free(vplane);
            }

            return true;
        }

        double Video::Sync_video(Video_state *videostate, AVFrame *sourceframe,
                                 double pts){
            double framedelay;

            if(pts != 0){
                videostate->videoclock = pts;
            }else{
                pts = videostate->videoclock;
            }

            framedelay = av_q2d(videostate->videostream->codec->time_base);

            framedelay += sourceframe->repeat_pict * (framedelay * 0.5);
            videostate->videoclock += framedelay;

            return pts;
        }

        int Video::Process_video_thread(void *data){
            Video_state *videostate = static_cast<Video_state*>(data);
            AVPacket *packet;
            AVPacket packet1;

            int hasframefinished;
            double pts;

            AVFrame *currentframe = av_frame_alloc();

            packet = &packet1;

            while(true){
                if(videostate->videoqueue.get(packet, 1) < 0){
                    break;
                }
                pts = 0;

                avcodec_decode_video2(videostate->videostream->codec,
                             currentframe, &hasframefinished, packet);

                if(packet->dts != AV_NOPTS_VALUE){
                    pts = av_frame_get_best_effort_timestamp(currentframe);
                }

                pts *= av_q2d(videostate->videostream->time_base);

                if(hasframefinished){
                    pts = Sync_video(videostate, currentframe, pts);

                    if(!Queue_video_frame(videostate, currentframe, pts)){
                        break;
                    }
                }

                av_free_packet(packet);
            }

            av_free(currentframe);
            return 0;
        }

        bool Video::Open_stream_component(Video_state *videostate,
                                         int streamindex){
            AVCodecContext *codeccontext;
            AVCodec *codec;
            SDL_AudioSpec wantedaudiospec;
            SDL_AudioSpec audiospec;

            if(streamindex < 0 || streamindex >=
                    videostate->videoformatcontext->nb_streams){
                Set_last_error("Tried to open invalid stream index.");
                return false;
            }

            codec = avcodec_find_decoder(
             videostate->videoformatcontext->streams[streamindex]->
             codec->codec_id);

            if(codec == nullptr){
                Set_last_error("Unsupported video or audio codec!");
                return false;
            }

            codeccontext = avcodec_alloc_context3(codec);
            if(avcodec_copy_context(codeccontext,
                                    videostate->videoformatcontext->
                                    streams[streamindex]->codec) != 0){
                Set_last_error("Could not copy video or audio codec contexts.");
                return false;
            }

            if(codeccontext->codec_type == AVMEDIA_TYPE_AUDIO){
                wantedaudiospec.freq =
                        videostate->audiocodeccontext->sample_rate;
                wantedaudiospec.format = AUDIO_S16SYS;
                wantedaudiospec.channels =
                        videostate->audiocodeccontext->channels;
                wantedaudiospec.silence = 0;
                wantedaudiospec.samples = 1024;
                wantedaudiospec.callback = Video_audio_sdl_callback;
                wantedaudiospec.userdata = videostate;

                if(SDL_OpenAudio(&wantedaudiospec, &audiospec) < 0){
                    Set_last_error(std::string("Error obtaining SDL audio spec.")
                     +"Error reported from SDL was: "+SDL_GetError());
                    return false;
                }
            }

            if(avcodec_open2(codeccontext, codec, nullptr) < 0){
                Set_last_error("Unsupported video or audio codec!");
                return false;
            }

            videostate->frametimer = static_cast<double>(av_gettime()
                                                         / 1000000.0);
            videostate->lastframedelay = 40e-3;

            switch(codeccontext->codec_type){
                case AVMEDIA_TYPE_AUDIO:
                    videostate->audiostreamlocation = streamindex;
                    videostate->audiostream = videostate->videoformatcontext->
                                              streams[streamindex];
                    videostate->audiocodeccontext = codeccontext;
                    videostate->audiobuffersize = 0;
                    videostate->audiobufferindex = 0;
                    std::memset(&videostate->audiopacket, 0,
                                sizeof(videostate->audiopacket));
                    while(!videostate->streamactive){}
                    if(videostate->streamactive){
                        SDL_PauseAudio(0);
                    }
                    break;
                case AVMEDIA_TYPE_VIDEO:
                    videostate->videostreamlocation = streamindex;

                    videostate->videostream =
                            videostate->videoformatcontext->streams[streamindex];
                    videostate->videocodeccontext = codeccontext;

                    while(!videostate->streamactive){}
                    if(videostate->streamactive){
                        videostate->videothread = SDL_CreateThread(
                                Process_video_thread, "VideoFrameRetrieval",
                                videostate);
                    }

                    videostate->swscontext = sws_getContext(
                     videostate->videostream->codec->width,
                     videostate->videostream->codec->height,
                     videostate->videostream->codec->pix_fmt,
                     videostate->videostream->codec->width,
                     videostate->videostream->codec->height,
                     AV_PIX_FMT_RGB24, SWS_BILINEAR, nullptr, nullptr, nullptr);
                    break;
                default:
                    break;
            }

            return true;
        }

        int Video::Video_decoding_thread(void *data){

            Video_state *videostate = static_cast<Video_state*>(data);

            if(avformat_open_input(&videostate->videoformatcontext,
                                   videostate->filename.c_str(),
                                   nullptr, nullptr) == 0){
                if(avformat_find_stream_info(videostate->videoformatcontext,
                                             nullptr) < 0){
                    int videostreamindex = Find_video_stream(videostate);
                    int audiostreamindex = Find_audio_stream(videostate);
                    if(videostreamindex >= 0){
                        if(Open_stream_component(videostate, videostreamindex)){
                            if(audiostreamindex >= 0){
                                if(Open_stream_component(videostate,
                                                   audiostreamindex)){
                                    AVPacket *currentpacket;

                                    while(true){
                                        if(exitsignalsent){
                                            break;
                                        }

                                        if(videostate->audioqueue.size >
                                           MAX_AUDIOQUEUE_SIZE ||
                                           videostate->videoqueue.size >
                                           MAX_VIDEOQUEUE_SIZE){
                                            SDL_Delay(10);
                                            continue;
                                        }else{
                                            break;
                                        }

                                        if(av_read_frame(
                                                videostate->videoformatcontext,
                                                currentpacket) < 0){
                                            if((videostate->videoformatcontext->
                                                pb->error) == 0){
                                                SDL_Delay(100);
                                                continue;
                                            }else{
                                                break;
                                            }
                                        }

                                        if(currentpacket->stream_index ==
                                           videostate->videostreamlocation){
                                            videostate->
                                            videoqueue.put(currentpacket);
                                        }else if(currentpacket->stream_index ==
                                                videostate->audiostreamlocation){
                                            videostate->audioqueue.put(
                                                        currentpacket);
                                        }else{
                                            av_free_packet(currentpacket);
                                        }
                                    }
                                }
                            }else{
                                Set_last_error(
                                 "Could not find audio stream in file.");
                            }
                        }
                    }else{
                        Set_last_error("Could not find video stream in file.");
                    }
                }else{
                    Set_last_error("Could not process video stream data.");
                }
            }else{
                Set_last_error("Could not open video file.");
            }

            return -1;
        }

        Uint32 Video::Sdl_video_refresh_timer_callback(Uint32 interval,
                                                       void *dataparam){
            videorefreshwaiting = true;
            return 0;
        }

        bool Video::Load_video(const std::string videofilepath){

            if(Should_video_stream_be_active()){
                Schedule_video_refresh(40);
            }

            Set_video_file_name(videofilepath);
            if(!Create_video_parsing_thread()){
                Set_last_error("Could not create video parsing thread");
                return false;
            }else{
                return true;
            }
        }

        void Video::Draw_video_texture(Video_state *videostate){
            Video_picture *videopicturetodraw =
                    &videostate->picturequeue[videostate->picturequeuereadindex];
            SDL_Rect destrect;

            destrect.x = Get_position().x;
            destrect.y = Get_position().y;
            destrect.w = Get_rect_to_draw().width;
            destrect.h = Get_rect_to_draw().height;

            SDL_LockMutex(videostate->picturequeuemutex);

            SDL_RenderCopy(Get_renderer(), videopicturetodraw->video, nullptr,
                           &destrect);

            SDL_UnlockMutex(videostate->picturequeuemutex);
        }

        double Video::Get_audio_clock(Video_state *videostate){
            double pts = videostate->audioclock;
            int hardwarebuffersize = videostate->audiobuffersize -
                                     videostate->audiobufferindex;
            int bytespersecond = 0;
            int ptsnumber = videostate->audiostream->codec->channels * 2;
            if(videostate->audiostream != nullptr){
                bytespersecond = videostate->audiostream->codec->sample_rate * 2;
                if(videostate->audiostream){
                    bytespersecond = videostate->audiostream->codec->sample_rate
                                     * ptsnumber;
                }

                if(bytespersecond){
                    pts -= static_cast<double>(hardwarebuffersize)
                           / bytespersecond;
                }
            }

            return pts;
        }

        void Video::Refresh_video(Video_state *videostate){
            Video_picture *videopicturetodraw;
            double actualdelay;
            double delay;
            double syncthreshold;
            double referenceclock;
            double difference;

            if(videostate->videostream != nullptr){
                if(videostate->picturequeuesize == 0){
                    Schedule_video_refresh(1);
                }else{
                    videopicturetodraw = &videostate->picturequeue[
                                         videostate->picturequeuereadindex];

                    delay = videopicturetodraw->pts - videostate->lastframepts;

                    if(delay <= 0 || delay >= 1.0){
                        delay = videostate->lastframedelay;
                    }

                    videostate->lastframedelay = delay;
                    videostate->lastframepts = videopicturetodraw->pts;

                    referenceclock = Get_audio_clock(videostate);
                    difference = videopicturetodraw->pts - referenceclock;

                    syncthreshold = (delay > AV_SYNC_THRESHOLD)
                                    ? delay : AV_SYNC_THRESHOLD;
                    if(fabs(difference) < AV_NOSYNC_THRESHOLD){
                        if(difference <= -syncthreshold){
                            delay = 0;
                        }else if(difference >= syncthreshold){
                            delay *= 2;
                        }
                    }

                    videostate->frametimer += delay;
                    actualdelay = videostate->frametimer - (av_gettime()
                                                            / 1000000.0);
                    if(actualdelay < 0.010){
                        actualdelay = 0.010;
                    }

                    Schedule_video_refresh(static_cast<int>
                                           (actualdelay*1000+0.5));

                    Draw_video_texture(videostate);

                    if(++videostate->picturequeuereadindex ==
                       VIDEO_PICTURE_QUEUE_SIZE){
                        videostate->picturequeuereadindex = 0;
                    }

                    SDL_LockMutex(videostate->picturequeuemutex);

                    --videostate->picturequeuesize;

                    SDL_CondSignal(videostate->picturequeueconditionvariable);
                    SDL_UnlockMutex(videostate->picturequeuemutex);
                }
            }else{
                Schedule_video_refresh(100);
            }
        }

        bool Video::Draw(){
            if(Should_video_stream_be_active()){
                if(Is_refresh_waiting()){
                    Refresh_video_after_timer();
                }
            }else if(Get_video_file_name() != std::string()){
                Schedule_video_refresh(40);
            }

            return true;
        }

    } /* namespace media */
} /* namespace jetfuel */
