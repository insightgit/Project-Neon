/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NEONMEDIA_VIDEO_H_
#define NEONMEDIA_VIDEO_H_

//#ifdef JETFUEL_USELPGLCODE

#define MAX_AUDIO_FRAME_SIZE 192000

#define MAX_AUDIOQUEUE_SIZE (5 * 16 * 1024)
#define MAX_VIDEOQUEUE_SIZE (5 * 256 * 1024)

#define VIDEO_PICTURE_QUEUE_SIZE 1

#define AV_SYNC_THRESHOLD 0.01
#define AV_NOSYNC_THRESHOLD 10.0

#include <stdio.h>
#ifdef _WIN32
    #include <windows.h>
#else
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <unistd.h>
#endif

#include <string>
#include <stdexcept>
#include <fstream>

extern "C"{
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libswscale/swscale.h>
    #include <libavutil/time.h>
}

#include <boost/filesystem.hpp>

#include "../config.h"

#include "../jetfueldraw/scenemanager.h"
#include "../jetfueldraw/drawable.h"
#include "../jetfueldraw/rectangleinterface.h"

namespace jetfuel {
    namespace media {

        namespace exceptions{
            class Get_cwd_exception : public std::runtime_error{
                public:
                    Get_cwd_exception() :
                    std::runtime_error
                    ("[!]Getting current working directory function failed!"){}
            };

            class Video_audio_buffer_too_small_exception : public std::runtime_error{
                public:
                    Video_audio_buffer_too_small_exception() :
                    std::runtime_error
                    (std::string("[!]Video audio data buffer was bigger")+
                                 "than the buffer allocated!"){}
            };
        }

        static bool exitsignalsent = false;
        static bool videorefreshwaiting = false;

        class Video : public jetfuel::draw::Drawable,
        public jetfuel::draw::Rectangle_interface {
        public:
            class Packet_queue{
            public:
                Packet_queue(){
                    mutex = SDL_CreateMutex();
                    cond = SDL_CreateCond();
                }

                bool put(AVPacket *packet);

                int get(AVPacket *packet, const bool block);

                AVPacketList *firstpacket;
                AVPacketList *lastpacket;

                int packetnumber = 0;
                int size = 0;

                SDL_mutex *mutex;
                SDL_cond *cond;
            };

            Video();

            virtual ~Video();

            bool Does_error_exist() const{
                return !m_lasterror.empty();
            }

            std::string Get_last_error() const{
                return m_lasterror;
            }

            std::string Get_video_file_name() const {
                return m_videostate.filename;
            }

            bool Load_video(const std::string videofilepath);

            jetfuel::draw::Rect2d_int Get_rect_to_draw()override{
                /*if(Get_codec_context() != nullptr){
                    return jetfuel::draw::Rect2d_int(Get_position(),
                            jetfuel::draw::Vector2d_int(Get_codec_context()->width,
                            Get_codec_context()->height));
                }else{
                    return jetfuel::draw::Rect2d_int(Get_position(),
                            jetfuel::draw::Vector2d_int(0,0));
                }*/

                jetfuel::draw::Vector2d_int videosize(0,0);

                SDL_DisplayMode windowsize;
                bool gottenwindowsize = false;

                Video_picture *currentvideopicture = &m_videostate.picturequeue[
                                            m_videostate.picturequeuereadindex];
                float aspectratio;

                if(currentvideopicture->video != nullptr){
                    if(m_videostate.videostream->codec->sample_aspect_ratio.num
                       == 0){
                        aspectratio = 0;
                    }else{
                        aspectratio = av_q2d(m_videostate.videostream->codec->
                                             sample_aspect_ratio) *
                                      m_videostate.videostream->codec->width /
                                      m_videostate.videostream->codec->height;
                    }

                    if(aspectratio <= 0.0){
                        aspectratio = static_cast<float>
                         (m_videostate.videostream->codec->width) /
                         static_cast<float>
                         (m_videostate.videostream->codec->height);
                    }

                    videosize.x = (static_cast<int>(
                                   rint(videosize.y * aspectratio)) & -3);

                    if(jetfuel::draw::Scene_manager::Is_window_full_screen(0)){
                        if(SDL_GetDesktopDisplayMode(0, &windowsize) == 0){
                            gottenwindowsize = true;
                        }else{

                        }
                    }else if(SDL_GetCurrentDisplayMode(0, &windowsize) == 0){
                        gottenwindowsize = true;
                    }else{
                        Set_last_error(std::string()+
                         "Could not get SDL screen size. SDL reported that"+
                         " the error was: "+SDL_GetError());

                    }

                    if(gottenwindowsize){
                        videosize.y = m_videostate.videostream->codec->height;
                        videosize.x = (static_cast<int>(
                                rint(videosize.y * aspectratio))) & -3;

                        if(videosize.x > windowsize.w){
                            videosize.x = m_videostate.videostream->codec->width;
                            videosize.y = (static_cast<int>(
                                    rint(videosize.x * aspectratio))) & -3;
                        }
                    }

                }

                return jetfuel::draw::Rect2d_int(Get_position(),videosize);
            }

            static Packet_queue Get_audio_packet_queue(){
                return m_audioqueue;
            }

            void Play(){
                m_videostate.streamactive = true;
            }

            bool Draw()override;
        protected:
            struct Video_picture{
                SDL_Texture *video;
                jetfuel::draw::Vector2d_int size;
                int allocated;
                double pts;
            };

            class Video_state{
            public:
                Video_state(){
                    picturequeuemutex = SDL_CreateMutex();
                    picturequeueconditionvariable = SDL_CreateCond();
                }

                SDL_Renderer *renderer;

                AVFormatContext *videoformatcontext;
                int videostreamlocation;
                int audiostreamlocation;
                AVStream *audiostream;
                AVCodecContext *audiocodeccontext;
                Packet_queue audioqueue;
                uint8_t audiobuffer[(MAX_AUDIO_FRAME_SIZE * 3) / 2];
                size_t audiobuffersize;
                int audiobufferindex;
                AVPacket audiopacket;
                uint8_t *audiopacketdata;
                int audiopacketsize;
                AVStream *videostream;
                AVCodecContext *videocodeccontext;
                Packet_queue videoqueue;
                struct SwsContext *swscontext;
                double videoclock;
                double frametimer;
                double lastframepts;
                double lastframedelay;
                double audioclock;

                Video_picture picturequeue[VIDEO_PICTURE_QUEUE_SIZE];
                int picturequeuesize;
                int picturequeuereadindex;
                int picturequeuewriteindex;
                bool streamactive = false;

                SDL_mutex *picturequeuemutex;
                SDL_cond *picturequeueconditionvariable;

                SDL_Thread *parsingthread;
                SDL_Thread *videothread;

                std::string filename;
            };

            /*AVFormatContext *Get_format_context(){
                return m_formatcontext;
            }*/

            AVCodecContext *Get_codec_context(){
                return m_codeccontext;
            }

            void Set_codec_context(AVCodecContext *codeccontext){
                m_codeccontext = codeccontext;
            }

            AVCodecContext *Get_old_codec_context(){
                return m_oldcodeccontext;
            }

            AVCodecContext *Get_audio_codec_context(){
                return m_audiocodeccontext;
            }

            void Set_audio_codec_context(AVCodecContext *audiocodeccontext){
                m_audiocodeccontext = audiocodeccontext;
            }

            AVCodecContext *Get_old_audio_codec_context(){
                return m_oldaudiocodeccontext;
            }


            void Set_old_audio_codec_context(AVCodecContext *oldaudiocodeccontext){
                m_oldaudiocodeccontext = oldaudiocodeccontext;
            }

            AVCodec *Get_codec(){
                return m_codec;
            }

            void Set_codec(AVCodec *codec){
                m_codec = codec;
            }

            AVCodec *Get_audio_codec(){
                return m_audiocodec;
            }

            void Set_audio_codec(AVCodec *audiocodec){
                m_codec = audiocodec;
            }

            AVFrame *Get_current_video_frame(){
                return m_currentvideoframe;
            }

            void Set_current_video_frame(AVFrame *currentvideoframe){
                m_currentvideoframe = currentvideoframe;
            }

            AVFrame *Get_current_rgb_video_frame(){
                return m_currentrgbvideoframe;
            }

            void Set_current_rgb_video_frame(AVFrame *currentrgbvideoframe){
                m_currentrgbvideoframe = currentrgbvideoframe;
            }

            uint8_t *Get_video_frame_buffer(){
                return m_videoframebuffer;
            }

            void Set_video_frame_buffer(uint8_t *videoframebuffer){
                m_videoframebuffer = videoframebuffer;
            }

            static void Set_last_error(const std::string lasterror){
                m_lasterror = lasterror;
            }

            std::string Get_video_cache_path(){
                return m_videocachepath.string();
            }

            bool Should_video_stream_be_active(){
                return m_videostate.streamactive;
            }

            void Set_video_file_name(std::string filename) {
                m_videostate.filename = filename;
            }

            bool Create_video_parsing_thread(){
                m_videostate.renderer = Get_renderer();

                m_videostate.parsingthread = SDL_CreateThread(
                                            Video_decoding_thread,
                                            "VideoDecoding",
                                            &m_videostate);
                return m_videostate.parsingthread != nullptr;
            }

            bool Create_screen_texture(int width, int height){
                m_screentexture = SDL_CreateTexture(Get_renderer(),
                        SDL_PIXELFORMAT_YV12, SDL_TEXTUREACCESS_STREAMING,
                        width, height);

                if(m_screentexture != nullptr){
                    Set_last_error(SDL_GetError());

                    return true;
                }else{
                    return false;
                }
            }

            bool Update_screen_texture(Uint8 *yplane, Uint8 *uplane,
                                       Uint8 *vplane, int uvpitch){
                int sdlreturnvalue = SDL_UpdateYUVTexture(
                                      m_screentexture,nullptr,
                                      yplane,
                                      Get_codec_context()->width,
                                      uplane,uvpitch,vplane,uvpitch);

                if(sdlreturnvalue==-1){
                    Set_last_error(std::string("SDL Texture Error.")
                                   +"Error reported from SDL was: "
                                   +SDL_GetError());
                    return false;
                }else{
                    return true;
                }
            }

            bool Draw_screen_texture(){
                SDL_Rect destrect;

                destrect.x = Get_rect_to_draw().x;
                destrect.y = Get_rect_to_draw().y;
                destrect.w = Get_rect_to_draw().width;
                destrect.h = Get_rect_to_draw().height;

                int sdldrawreturnvalue =
                        SDL_RenderCopy(Get_renderer(), m_screentexture, nullptr,
                               &destrect);

                if(sdldrawreturnvalue != 0){
                    Set_last_error(std::string("SDL Drawing returned error!")+
                                   "Error was:"+SDL_GetError());
                    return false;
                }else{
                    return true;
                }
            }

            bool Is_video_stream_active() const{
                return m_isstreamactive;
            }

            bool Is_refresh_waiting(){
                if(videorefreshwaiting){
                    videorefreshwaiting = false;
                }

                return videorefreshwaiting;
            }

            bool Should_video_stream_be_active() const{
                return m_videostate.streamactive;
            }

            bool Put_into_packet_queue(AVPacket *packet){
                return m_audioqueue.put(packet);
            }

            void Schedule_video_refresh(unsigned int delay){
                m_isstreamactive = true;
                m_videostate.streamactive = true;

                SDL_AddTimer(delay, Sdl_video_refresh_timer_callback, nullptr);
            }

            static int Find_video_stream(Video_state *videostate);

            static int Find_audio_stream(Video_state *videostate);

            static bool Allocate_pixel_buffers(Video_state *videostate, Uint8 *yplane,
                                        Uint8 *uplane, Uint8 *vplane);

            static void Setup_av_picture_to_draw(Video_state *videostate,
                                          AVPicture *picturetodraw,
                                       Uint8 *yplane, Uint8 *uplane,
                                       Uint8 *vplane, int uvpitch);

            static void Clean_up_drawable_video_stream_variables(Uint8 *yplane,
                                                          Uint8 *uplane,
                                                          Uint8 *vplane);

            bool Save_video_frame(AVFrame *frame, int width, int height,
                                  int framenum);

            static void Allocate_video_frame(Video_state *videostate);

            static bool Queue_video_frame(Video_state *videostate,
                                   AVFrame *currentframe, double pts);

            static double Sync_video(Video_state *videostate,
                                     AVFrame *sourceframe, double pts);

            static int Process_video_thread(void *data);

            static bool Open_stream_component(Video_state *videostate, int streamindex);

            static int Video_decoding_thread(void *data);

            void Refresh_video_after_timer(){
                Refresh_video(&m_videostate);
            }
        private:
            static Uint32 Sdl_video_refresh_timer_callback(Uint32 interval,
                                                           void *dataparam);
            static int Video_audio_decode_frame(
                    Video_state *videostate, uint8_t *audiobuffer,
                    size_t buffersize, double *ptspointer);

            static void Video_audio_sdl_callback(void *userdata,
                                         Uint8 *stream, int len);

            void Draw_video_texture(Video_state *videostate);

            double Get_audio_clock(Video_state *videostate);

            void Refresh_video(Video_state *videostate);

            Video_state m_videostate;

            static Packet_queue m_audioqueue;
            bool m_isstreamactive = false;

            static std::string m_lasterror;

            boost::filesystem::path m_videocachepath;

            SDL_Texture *m_screentexture;

            //AVFormatContext *m_formatcontext;
            AVCodecContext *m_codeccontext;
            AVCodecContext *m_oldcodeccontext;

            AVCodecContext *m_audiocodeccontext;
            AVCodecContext *m_oldaudiocodeccontext;

            AVCodec *m_codec;
            AVCodec *m_audiocodec;

            uint8_t *m_videoframebuffer;
            AVFrame *m_currentvideoframe;
            AVFrame *m_currentrgbvideoframe;
        };

    } /* namespace media */
} /* namespace jetfuel */

//#endif

#endif /* NEONMEDIA_VIDEO_H_ */
