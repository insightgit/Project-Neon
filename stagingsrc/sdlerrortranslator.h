/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef JETFUELCORE_SDLERRORTRANSLATOR_H_
#define JETFUELCORE_SDLERRORTRANSLATOR_H_

#include <SDL2/SDL.h>
#include <string>

namespace jetfuel {
    namespace core{
        static std::string Get_sdl_error();
    }
}


#endif /* JETFUELCORE_SDLERRORTRANSLATOR_H_ */
