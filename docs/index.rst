..  This file is part of Project-Neon.

    Project-Neon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Project-Neon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Project-Neon.  If not, see <http://www.gnu.org/licenses/>.

.. index:

Project-Neon
============

Documentation:
.. toctree::
   :maxdepth: 2
   NeonCore/Documentation/messagebus

Project-Neon is a 2D moddable Game Engine made for the modern era.
Project-Neon allows for python integration with your pre-existing C++ code,
so you can allow you or a modder to mod your RPG to your, or their,
heart's content.

Features
--------

- Python 3.6 Integration
- SDL-based Scene Manager
- Json saving
- Can export to Windows, macOS, Linux, Android, iOS, and HTML5 WebGL (via Emscripten).

Installation
------------

TODO

Contribute
----------

TODO

Support
-------

TODO

License
-------

The project is licensed under the AGPLV3+ license.
