/*
 *   Project Neon Console Example - A text-only example of Project Neon, the moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon Console Example is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon Console Example is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon Console Example.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <jetfuelinspire.h>

//#include <python3.6/Python.h>

int main() {
    jetfuel::core::Message_bus messagebus;

    jetfuel::core::Timer *timerexample = new jetfuel::core::Timer(5,true);
    bool timerexists = true;

    std::string email;
    while(true){
        if(messagebus.Do_messages_exist()){
            if(messagebus.Does_message_exist("consoleinit")){
                std::cout << "Console Inited! What is your email address?\n";
                std::cin >> email;
                std::cout << "OK! Launching Python 3 smtp script...\n";
                messagebus.Post_message("emailrecieved");
            }else if(messagebus.Does_message_exist("emailrecieved")){
                jetfuel::inspire::Python_module_loader smtploader = jetfuel::inspire::Python_module_loader("smtpexample","sendemail","./Scripts/");
                bool executedstatus;
                std::string error;
                smtploader.Execute(&executedstatus,&error,PyTuple_Pack(1, PyUnicode_FromString(email.c_str())));
                if(!executedstatus){
                    std::cout << "Python Interpreter ERROR! Error is:" << error << "\n";
                    return 1;
                }
                messagebus.Post_message("emailprocessed");
            }else if(messagebus.Does_message_exist("emailprocessed")){
                jetfuel::inspire::Python_module_loader isequalloader = jetfuel::inspire::Python_module_loader("smtpexample","isequal","./Scripts/");
                bool executedstatus;
                std::string error;
                bool result = isequalloader.Execute_bool(&executedstatus,&error,PyTuple_Pack(2,PyLong_FromLong(5),PyLong_FromLong(5)));
                if(!executedstatus){
                    std::cout << "Python Interpreter ERROR! Error is:" << error << "\n";
                    return 1;
                }
                std::cout << "RESULT==" << result << "\n";

                jetfuel::inspire::Python_module_loader addloader = jetfuel::inspire::Python_module_loader("smtpexample","add","./Scripts/");
                executedstatus = bool();
                error = std::string();
                long longresult = addloader.Execute_long(&executedstatus,&error,(PyTuple_Pack(2,PyLong_FromLong(3),PyLong_FromLong(3))));
                if(!executedstatus){
                     std::cout << "Python Interpreter ERROR! Error is:" << error << "\n";
                     return 1;
                }

                std::cout << "RESULT==" << longresult << "\n";

                addloader = jetfuel::inspire::Python_module_loader("smtpexample","add","./Scripts/");
                executedstatus = bool();
                error = std::string();
                double doubleresult = addloader.Execute_double(&executedstatus,&error,(PyTuple_Pack(2,PyFloat_FromDouble(4.23),PyFloat_FromDouble(5.67))));
                if(!executedstatus){
                     std::cout << "Python Interpreter ERROR! Error is:" << error << "\n";
                     return 1;
                }

                std::cout << "RESULT==" << doubleresult << "\n";

                addloader = jetfuel::inspire::Python_module_loader("smtpexample","add","./Scripts/");
                executedstatus = bool();
                error = std::string();
                char *stringresult = addloader.Execute_cstring(&executedstatus,&error,(PyTuple_Pack(2,PyUnicode_FromString("ABC"),PyUnicode_FromString("Poop"))));
                if(!executedstatus){
                     std::cout << "Python Interpreter ERROR! Error is:" << error << "\n";
                     return 1;
                }

                std::cout << "RESULT==" << stringresult << "\n";

                messagebus.Post_message("functiontestscompleted");
            }else if(messagebus.Does_message_exist("functiontestscompleted")){
                bool executedstatus;
                std::string error;
                std::string filename = "classexample";
                std::string directory = "./Scripts/";
                jetfuel::inspire::Python_class_loader classtester = jetfuel::inspire::Python_class_loader(NULL,&filename,&filename,
                                                                                                   &executedstatus, &error,
                                                                                                   &directory);
                if(!executedstatus){
                    std::cout << "Python Interpreter ERROR! Error is:" << error << "\n";
                    return 1;
                }
                jetfuel::inspire::Python_module_loader classfunctiontester = jetfuel::inspire::Python_module_loader(&classtester,std::string("sayhello"));
                executedstatus = bool();

                bool returnvalue = classfunctiontester.Execute_bool(&executedstatus,&error,PyTuple_Pack(1,PyUnicode_FromString("G'day")));
                if(!executedstatus){
                    std::cout << "Python Interpreter ERROR! Error is:" << error << "\n";
                    return 1;
                }

                std::cout << "RESULT==" << returnvalue << "\n";

                messagebus.Post_message("quit");
            }else if(messagebus.Does_message_exist("quit")){
                std::cout << "Shutting down...\n";
                return 0;
            }
        }
        if(timerexists){
            if(timerexample->Has_timer_finished()){
                messagebus.Post_message("consoleinit");
                delete timerexample;
                timerexists = false;
            }
        }
    }
	return 0;
}
