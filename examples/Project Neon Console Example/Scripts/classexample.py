 #
 #   Project Neon Console Example - A text-only example of Project Neon, the moddable 2D RPG game engine
 #   Copyright (C) 2017 InfernoStudios
 #
 #  Project Neon Console Example is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  Project Neon Console Example is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with Project Neon Console Example.  If not, see <http://www.gnu.org/licenses/>.
 #


class classexample:
    def __init__(self):
        print("classexample inited!");
    def sayhello(self, name):
        print("Hello"+name);