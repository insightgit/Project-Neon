#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_int
from ctypes import c_bool

from jetfuel.draw.color import color

class circle_2d_shape_characteristics_replacement(object):
    _jetfuel = None;
    circle2dshapecharsref = None;
    
    def __init__(self, jetfuelsoloader, customref=None):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        
        if(customref is None):
            self._jetfuel.Circle_2d_shape_characteristics_replacement_new.restype = \
                c_void_p;
                
            self.circle2dshapecharsref = self._jetfuel.\
            Circle_2d_shape_characteristics_replacement_new();
        else:
            self.circle2dshapecharsref = customref;
            
    def get_position_x(self):
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_position_x.argtypes = [
                                                                    c_void_p];
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_position_x.restype = \
                                                                        c_int;
                                                                        
        return self._jetfuel.\
            Circle_2d_shape_characteristics_replacement_get_position_x(
                                            self.circle2dshapecharsref);
                                            
    def get_position_y(self):
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_position_y.argtypes = [
                                                                    c_void_p];
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_position_y.restype = \
                                                                        c_int;
                                                                        
        return self._jetfuel.\
            Circle_2d_shape_characteristics_replacement_get_position_y(
                                            self.circle2dshapecharsref);
                                            
    def set_position(self, x, y):
        self._jetfuel.Circle_2d_shape_characteristics_replacement_set_position.\
                                            argtypes = [c_void_p, c_int, c_int];
        self._jetfuel.Circle_2d_shape_characteristics_replacement_set_position(
                                            self.circle2dshapecharsref, x, y);
                                            
    def get_radius(self):
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_radius.argtypes = [
                                                                    c_void_p];
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_radius.restype = c_int;
                                                                        
        return self._jetfuel.\
            Circle_2d_shape_characteristics_replacement_get_radius(
                                            self.circle2dshapecharsref);
                                            
    def set_radius(self, radius):
        self._jetfuel.Circle_2d_shape_characteristics_replacement_set_radius.\
                                                    argtypes = [c_void_p, c_int];
        self._jetfuel.Circle_2d_shape_characteristics_replacement_set_radius(
                                            self.circle2dshapecharsref, radius);
                                            
    def get_aa_status(self):
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_aa_status.argtypes = [
                                                                    c_void_p];
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_aa_status.restype = \
                                                                        c_bool;
                                                                        
        return self._jetfuel.\
            Circle_2d_shape_characteristics_replacement_get_aa_status(
                                            self.circle2dshapecharsref);
                                            
    def set_aa_status(self, aastatus):
        self._jetfuel.Circle_2d_shape_characteristics_replacement_set_aa_status.\
                                                    argtypes = [c_void_p, c_bool];
        self._jetfuel.Circle_2d_shape_characteristics_replacement_set_aa_status(
                                        self.circle2dshapecharsref, aastatus);
                                        
    def get_filled_ciricle_status(self):
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_filled_circle_status.\
                                                        argtypes = [c_void_p];
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_filled_circle_status.\
                                                            restype = c_bool;
                                                                        
        return self._jetfuel.\
            Circle_2d_shape_characteristics_replacement_get_filled_circle_status(
                                                    self.circle2dshapecharsref);
                                            
    def set_filled_circle_status(self, filledcirclestatus):
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_set_filled_circle_status.\
                                                argtypes = [c_void_p, c_bool];
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_set_filled_circle_status(
                                self.circle2dshapecharsref, filledcirclestatus);
                                
                                
    def get_color(self, jetfuelsoloader):
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_color.\
        argtypes = [c_void_p];
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_color.restype = c_void_p;
        
        returnvalue = color(jetfuelsoloader.jetfuelso);
        
        self._jetfuel.Color_delete.argtypes = [c_void_p];
        
        self._jetfuel.Color_delete(returnvalue.colorref);
        
        returnvalue.colorref = self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_get_color(
                                    self.circle2dshapecharsref);
                                                 
        return returnvalue;
    
    def set_color(self, color):
        self._jetfuel.\
        Circle_2d_shape_characteristics_replacement_set_color.argtypes = [
                                                        c_void_p, c_void_p];
        
        self._jetfuel.Circle_2d_shape_characteristics_replacement_set_color(
                                self.circle2dshapecharsref, color.colorref);
                                            
    