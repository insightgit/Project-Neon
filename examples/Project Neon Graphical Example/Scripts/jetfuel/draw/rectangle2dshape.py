#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from jetfuel.draw.rectangleinterface import rectangle_interface

from ctypes import c_void_p
from ctypes import c_int

from jetfuel.draw.color import color

class rectangle_2d_shape(rectangle_interface):

    def __init__(self, jetfuelsoloader, x=None, y=None, width=None,
                 height=None):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        
        if(x is not None and y is not None and width is not None and 
           height is not None):
            self._jetfuel.Rectangle_2d_shape_new_from_rect.restype = c_void_p;
            self._jetfuel.Rectangle_2d_shape_new_from_rect.argtypes = [c_int,      
                                                            c_int, c_int, c_int];
            self.drawableref = self._jetfuel.Rectangle_2d_shape_new_from_rect(
                                                            x, y, width, height);
        else:
            self._jetfuel.Rectangle_2d_shape_new.restype = c_void_p;
            
            self.drawableref = self._jetfuel.Rectangle_2d_shape_new();                                                  
            
    def get_fill_color(self):
        self._jetfuel.Rectangle_2d_shape_get_fill_color.argtypes = [c_void_p];
        self._jetfuel.Rectangle_2d_shape_get_fill_color.restype = c_void_p;
        
        returnvalue = color();
        
        self._jetfuel.Color_delete.argtypes = [c_void_p];
        
        self._jetfuel.Color_delete(returnvalue.colorref);
        
        returnvalue.colorref = self._jetfuel.Rectangle_2d_shape_get_fill_color(
                                                               self.drawableref);
        
        return returnvalue;
    
    def set_fill_color(self, fillcolor):
        self._jetfuel.Rectangle_2d_shape_set_fill_color.argtypes = [c_void_p,
                                                                    c_void_p];
        self._jetfuel.Rectangle_2d_shape_set_fill_color(self.drawableref,
                                                        fillcolor.colorref);
                                                        
    def get_outline_color(self):
        self._jetfuel.Rectangle_2d_shape_get_outline_color.argtypes = [c_void_p];
        self._jetfuel.Rectangle_2d_shape_get_outline_color.restype = c_void_p;
        
        returnvalue = color();
        
        self._jetfuel.Color_delete.argtypes = [c_void_p];
        
        self._jetfuel.Color_delete(returnvalue.colorref);
        
        returnvalue.colorref = self._jetfuel.Rectangle_2d_shape_get_outline_color(
                                                               self.drawableref);
        
        return returnvalue;
    
    def set_outline_color(self, outlinecolor):
        self._jetfuel.Rectangle_2d_shape_set_outline_color.argtypes = [c_void_p,
                                                                       c_void_p];
        self._jetfuel.Rectangle_2d_shape_set_outline_color(self.drawableref,
                                                        outlinecolor.colorref);
                                                        
    def disable_drawing_outline_color(self):
        self._jetfuel.Rectangle_2d_shape_disable_drawing_outline_color.argtypes =[
                                                                     c_void_p];
        
        self._jetfuel.Rectangle_2d_shape_disable_drawing_outline_color(
                                                    self._drawableref);
                                                        
    def get_position_x(self):
        self._jetfuel.Rectangle_2d_shape_get_position_x.argtypes = [c_void_p];
        self._jetfuel.Rectangle_2d_shape_get_position_x.restype = c_int;
        
        return self._jetfuel.Rectangle_2d_shape_get_position_x(self.drawableref);
    
    def get_position_y(self):
        self._jetfuel.Rectangle_2d_shape_get_position_y.argtypes = [c_void_p];
        self._jetfuel.Rectangle_2d_shape_get_position_y.restype = c_int;
        
        return self._jetfuel.Rectangle_2d_shape_get_position_y(self.drawableref);
    
    def set_position(self, x, y):
        self._jetfuel.Rectangle_2d_shape_set_position.argtypes = [c_void_p, 
                                                                  c_int,
                                                                  c_int];
                                                                  
        self._jetfuel.Rectangle_2d_shape_set_position(self.drawableref, x, y);
        
    def get_size_width(self):
        self._jetfuel.Rectangle_2d_shape_get_size_width.argtypes = [c_void_p];
        self._jetfuel.Rectangle_2d_shape_get_size_width.restype = c_int;
        
        return self._jetfuel.Rectangle_2d_shape_get_size_width(self.drawableref);
    
    def get_size_height(self):
        self._jetfuel.Rectangle_2d_shape_get_size_height.argtypes = [c_void_p];
        self._jetfuel.Rectangle_2d_shape_get_size_height.restype = c_int;
        
        return self._jetfuel.Rectangle_2d_shape_get_size_height(
                                                 self.drawableref);
    
    def set_size(self, width, height):
        self._jetfuel.Rectangle_2d_shape_set_size.argtypes = [c_void_p, 
                                                                  c_int,
                                                                  c_int];
                                                                  
        self._jetfuel.Rectangle_2d_shape_set_size(self.drawableref, width, 
                                                      height);
        