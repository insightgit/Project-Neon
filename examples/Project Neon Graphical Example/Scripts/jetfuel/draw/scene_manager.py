from ctypes import c_void_p
from ctypes import c_int

class scene_manager(object):
    _jetfuel = None;
    _scenemanagerref = None;

    def __init__(self, jetfuelsoloader, scenemanagerpointer):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        self._scenemanagerref = scenemanagerpointer;
        
    def attach_drawable(self, drawablepointer, drawableweight):
        self._jetfuel.Scene_attach_drawable.argtypes = [c_void_p,
                                                        c_void_p,
                                                        c_int];
        self._jetfuel.Scene_attach_drawable(self._scenemanagerref,
                                           drawablepointer,
                                           drawableweight);