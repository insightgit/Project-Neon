#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_wchar_p

class pointer_bridge(object):
    
    _jetfuel = None;
    _pointerbridgeref = None;

    def __init__(self, jetfuelsoloader, pointerbridgeref):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        
        self._pointerbridgeref = pointerbridgeref;
        
    def recieve_pointer(self, pointerid, found):
        self._jetfuel.Pointer_bridge_recieve_pointer.argtypes = [c_void_p,
                                                                 c_wchar_p,
                                                                 c_void_p];
        self._jetfuel.Pointer_bridge_recieve_pointer.restype = c_void_p;
        
        return self._jetfuel.Pointer_bridge_recieve_pointer(
                                     self._pointerbridgeref, pointerid,
                                     id(found));
                                     
    def send_pointer(self, pointerid, pointer):
        self._jetfuel.Pointer_bridge_send_pointer.argtypes = [c_void_p,
                                                              c_wchar_p,
                                                              c_void_p];
        self._jetfuel.Pointer_bridge_send_pointer(self._pointerbridgeref,
                                                  pointerid, pointer);