#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_wchar_p
from ctypes import c_int

class image(object):
    _jetfuel = None;
    imageref = None;
    
    def __init__(self, jetfuelsoloader):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        self._jetfuel.Image_new.restype = c_void_p;
        
        self.imageref = self._jetfuel.Image_new();
        
    def delete_ref(self):
        if(self._imageref != None):
            self._jetfuel.Image_delete.argtype = [c_void_p];
            
            self._jetfuel.Image_delete(self.imageref); 
         
    def get_image_size_x(self):
        self._jetfuel.Image_get_image_size_x.argtypes = [c_void_p];
        self._jetfuel.Image_get_image_size_x.restype = c_int;
        
        return self._jetfuel.Image_get_image_size_x(self.imageref);
        
    def get_image_size_y(self):
        self._jetfuel.Image_get_image_size_y.argtypes = [c_void_p];
        self._jetfuel.Image_get_image_size_y.restype = c_int;
        
        return self._jetfuel.Image_get_image_size_y(self.imageref);
        
    def get_image_location(self):
        self._jetfuel.Image_get_image_location.argtypes = [c_void_p];
        self._jetfuel.Image_get_image_location.restype = c_wchar_p;
        
        return  self._jetfuel.Image_get_image_location(self.imageref);
        
    def set_image_location(self, imagefilepath, scenemanager):
        self._jetfuel.Image_set_image_location.argtypes = [c_void_p, 
                                                           c_void_p, 
                                                           c_wchar_p];
        self._jetfuel.Image_set_image_location(self.imageref,
                                               scenemanager.scenemanagerref,
                                               imagefilepath);
                                               
    def set_image_location_and_size(self, imagefilepath, imagesizex,
                                    imagesizey):
        self._jetfuel.Image_set_image_location.argtypes = [c_void_p, 
                                                           c_wchar_p, 
                                                           c_int,
                                                           c_int];
        self._jetfuel.Image_set_image_location(self.imageref,
                                               imagefilepath,
                                               imagesizex,
                                               imagesizey);