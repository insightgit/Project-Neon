#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from sys import path
from os.path import abspath
from ctypes import c_char_p

from jetfuel.media.music import music
from jetfuel.jetfuelsoloader import jetfuelsoloader

def playmusic():
    jetfuelpythonapiso = abspath("libJetfuel Game Engine Python API.so");
    musicpath = abspath("music.ogg");
    
    print("jetfuelso="+jetfuelpythonapiso+
      " musicpath="+musicpath);
    
    jetfuelso = jetfuelsoloader(jetfuelpythonapiso);
    
    currentmusic = music(jetfuelso);
    
    if(currentmusic.load_audio_file(musicpath) == False):
        print("Could not open music file. Error was: "+
              jetfuelso.get_sdl_error());
    
    if(currentmusic.play() == False):
        print("Could not play music. Error was: "+
              jetfuelso.get_sdl_error());
              
    #while(currentmusic.is_music_playing() == True):
        #print("Music is playing");
        
if(__name__== "__main__"):
    playmusic()
