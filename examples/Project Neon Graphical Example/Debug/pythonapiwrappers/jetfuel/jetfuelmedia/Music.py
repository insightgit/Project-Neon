#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import cdll

class Music(object):
    m_jetfuel = None;
    
    def __init__(self, jetfuelsofilepath):
        m_jetfuel = cdll.LoadLibrary(jetfuelsofilepath);
    
    def is_music_playing(self):
        m_jetfuel.Music_is_music_playing();
        
    def is_music_paused(self):
        m_jetfuel.Music_is_music_paused();

    def play(self,musicfilepath):
        m_jetfuel.Music_play(musicfilepath);
        
    def pause(self):
        m_jetfuel.Music_pause();
        
    def resume(self):
        m_jetfuel.Music_resume();