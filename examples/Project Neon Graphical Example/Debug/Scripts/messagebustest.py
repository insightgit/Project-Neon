#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from sys import path
from os.path import abspath

from jetfuel.core.message_bus import message_bus
from jetfuel.jetfuelsoloader import jetfuelsoloader


def testmessagebus(messagebuspointer):
    jetfuelpythonapiso = abspath("libJetfuel Game Engine Python API.so");
    
    print("jetfuelso="+jetfuelpythonapiso+" type="+
          type(messagebuspointer).__name__);
    
    jetfuelso = jetfuelsoloader(jetfuelpythonapiso);
    
    currentmessagebus = message_bus(jetfuelso,messagebuspointer);
    
    if(currentmessagebus.post_message("Test from python.") == True):
        print("Posted message.");
    else:
        print("Test failed because of no message bus passed");
    
    if(currentmessagebus.does_message_exist("Test from python.") == True):
        print("Test passed.");
    else:
        print("Test failed because of no message existing.");
