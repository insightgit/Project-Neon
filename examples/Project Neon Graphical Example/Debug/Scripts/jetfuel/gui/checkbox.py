#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_bool
from ctypes import c_int
from ctypes import c_wchar_p
from ctypes import c_uint

from jetfuel.draw.rectangleinterface import rectangle_interface

class check_box(rectangle_interface):
    _jetfuel = None;
    
    def __init__(self, jetfuelsoloader):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        
        self._jetfuel.Check_box_new.restype = c_void_p;
        
        self.drawableref = self._jetfuel.Check_box_new();
        
    def load_check_box_images(self, activeimage, disabledimage):
        self._jetfuel.Check_box_load_check_box_images.argtypes = [c_void_p,
                                                                  c_void_p,
                                                                  c_void_p];
                                                                  
        self._jetfuel.Check_box_load_check_box_images(self.drawableref,
                                                      activeimage.imageref,
                                                      disabledimage.imageref);
    
    def is_checked(self):
        self._jetfuel.Check_box_is_checked.argtypes = [c_void_p];
        
        self._jetfuel.Check_box_is_checked.restype = c_bool;
        
        return self._jetfuel.Check_box_is_checked(self.drawableref);
    
    def get_position_x(self):
        self._jetfuel.Check_box_get_position_x.argtypes = [c_void_p];
        
        self._jetfuel.Check_box_get_position_x.restype = c_int;
        
        return self._jetfuel.Check_box_get_position_x(self.drawableref);
    
    def get_position_y(self):
        self._jetfuel.Check_box_get_position_y.argtypes = [c_void_p];
        
        self._jetfuel.Check_box_get_position_y.restype = c_int;
        
        return self._jetfuel.Check_box_get_position_y(self.drawableref);
    
    def set_position(self, x, y):
        self._jetfuel.Check_box_set_position.argtypes = [c_void_p, c_int, c_int];
        
        self._jetfuel.Check_box_set_position(self.drawableref, x, y);
        
    def get_label_characteristics(self):
        self._jetfuel.Check_box_get_label_characteristics.argtypes = [c_void_p];
       
        self._jetfuel.Check_box_get_label_characteristics.restype = c_void_p;
        
        return self._jetfuel.Check_box_get_label_characteristics(
                                                        self.drawableref);
                                                        
    def set_label_characteristics(self, labelchars, left, labelgap):
        self._jetfuel.Check_box_set_label_characteristics.argtypes = [c_void_p,
                                                                      c_void_p,
                                                                      c_bool,
                                                                      c_uint];
                                                                      
        self._jetfuel.Check_box_set_label_characteristics(self.drawableref,
                                        labelchars.textcharsreplacementref, left,
                                        labelgap);
                                        
    def set_uis_action_to_watch(self, action):
        self._jetfuel.Check_box_set_UIS_action_to_watch.argtypes = [c_void_p,
                                                                    c_wchar_p];
        
        self._jetfuel.Check_box_set_UIS_action_to_watch(self.drawableref,
                                                        action);
                                                        
    def get_checkbox_rect_to_draw_x(self):
        self._jetfuel.Check_box_get_checkbox_rect_to_draw_x.argtypes = [
                                                     c_void_p];
        
        self._jetfuel.Check_box_get_checkbox_rect_to_draw_x.restype = c_int;
        
        return self._jetfuel.Check_box_get_checkbox_rect_to_draw_x(self.drawableref);
    
    def get_checkbox_rect_to_draw_y(self):
        self._jetfuel.Check_box_get_checkbox_rect_to_draw_y.argtypes = [
                                                     c_void_p];
        
        self._jetfuel.Check_box_get_checkbox_rect_to_draw_y.restype = c_int;
        
        return self._jetfuel.Check_box_get_checkbox_rect_to_draw_y(self.drawableref);
    
    def get_checkbox_rect_to_draw_width(self):
        self._jetfuel.Check_box_get_checkbox_rect_to_draw_width.argtypes = [
                                                     c_void_p];
        
        self._jetfuel.Check_box_get_checkbox_rect_to_draw_width.restype = c_int;
        
        return self._jetfuel.Check_box_get_checkbox_rect_to_draw_width(
                                                      self.drawableref);
    
    def get_checkbox_rect_to_draw_height(self):
        self._jetfuel.Check_box_get_checkbox_rect_to_draw_height.argtypes = [
                                                     c_void_p];
        
        self._jetfuel.Check_box_get_checkbox_rect_to_draw_height.restype = c_int;
        
        return self._jetfuel.Check_box_get_checkbox_rect_to_draw_height(
                                                       self.drawableref);
        