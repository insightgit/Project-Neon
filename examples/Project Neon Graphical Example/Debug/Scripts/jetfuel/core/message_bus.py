#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
from ctypes import c_void_p
from ctypes import c_wchar_p
from ctypes import c_bool

class message_bus(object):
    _jetfuel = None;
    _messagebus = None;

    def initialize_message_bus(self, jetfuelsoloader):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        self._jetfuel.Get_python_var_message_bus.restype = c_void_p;
        
        _messagebus = self._jetfuel.Get_python_var_message_bus();
        
        if(_messagebus is None):
            return False;
        else:
            return True;
        
    def post_message(self, message):
        if(self._messagebus is None):
            return False;
        else:
            self._jetfuel.Message_bus_post_message_to_message_bus.argtypes = [
                          c_void_p, c_wchar_p];
                          
            self._jetfuel.Message_bus_post_message_to_message_bus(
                self._messagebus, message);
            
            return True;
        
    def does_message_exist(self, message):
        if(self._messagebus is None):
            return "NULL message bus pointer";
        else:
            self._jetfuel.Message_bus_does_message_exist.argtypes = [
              c_void_p, c_wchar_p];
                          
            self._jetfuel.Message_bus_does_message_exist.restype = c_bool;
                          
            return self._jetfuel.Message_bus_does_message_exist(
                self._messagebus, message);