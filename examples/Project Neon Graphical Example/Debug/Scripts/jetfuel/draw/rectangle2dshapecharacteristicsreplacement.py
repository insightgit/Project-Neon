#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_int

from jetfuel.draw.color import color

class rectangle_2d_shape_characteristics_replacement(object):
    _jetfuel = None;
    rect2dshapecharsref = None;
    
    def __init__(self, jetfuelsoloader, customref=None):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        
        if(customref is None):
            self._jetfuel.\
            Rectangle_2d_shape_characteristics_replacement_new.restype = \
                                                                c_void_p;                                                
            self.rect2dshapecharsref = self._jetfuel.\
            Rectangle_2d_shape_characteristics_replacement_new();
        else:
            self.rect2dshapecharsref = customref;
                        
    def get_position_x(self):
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_position_x.\
        argtypes = [c_void_p];
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_position_x.\
        restype = c_int;
        
        return self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_position_x(
                                                 self.rect2dshapecharsref);
            
    def get_position_y(self):
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_position_y.\
        argtypes = [c_void_p];
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_position_y.\
        restype = c_int;
        
        return self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_position_y(
                                                self.rect2dshapecharsref);
            
    def set_position(self, x, y):
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_set_position.argtypes = [
                                                        c_void_p, c_int, c_int];
                                                        
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_set_position(
                                    self.rect2dshapecharsref, x, y);
                                    
    def get_size_width(self):
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_size_width.\
        argtypes = [c_void_p];
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_size_width.\
        restype = c_int;
        
        return self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_size_width(
                                                 self.rect2dshapecharsref);
            
    def get_size_height(self):
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_size_height.\
        argtypes = [c_void_p];
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_size_height.\
        restype = c_int;
        
        return self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_size_height(
                                                self.rect2dshapecharsref);
            
    def set_size(self, width, height):
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_set_size.argtypes = [
                                                        c_void_p, c_int, c_int];
                                                        
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_set_size(
                        self.rect2dshapecharsref, width, height);
                        
    def get_fill_color(self, jetfuelsoloader):
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_fill_color.\
        argtypes = [c_void_p];
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_fill_color.\
        restype = c_void_p;
        
        returnvalue = color(jetfuelsoloader.jetfuelso);
        
        self._jetfuel.Color_delete.argtypes = [c_void_p];
        
        self._jetfuel.Color_delete(returnvalue.colorref);
        
        returnvalue.colorref = self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_fill_color(
                                                self.rect2dshapecharsref);
                                                 
        return returnvalue;
    
    def set_fill_color(self, fillcolor):
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_set_fill_color.\
        argtypes = [c_void_p, c_void_p];
        
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_set_fill_color(
                        self.rect2dshapecharsref, fillcolor.colorref);
                        
    def get_outline_color(self, jetfuelsoloader):
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_outline_color.\
        argtypes = [c_void_p];
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_outline_color.\
        restype = c_void_p;
        
        returnvalue = color(jetfuelsoloader.jetfuelso);
        
        self._jetfuel.Color_delete.argtypes = [c_void_p];
        
        self._jetfuel.Color_delete(returnvalue.colorref);
        
        returnvalue.colorref = self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_get_outline_color(
                                                self.rect2dshapecharsref);
                                                 
        return returnvalue;
    
    def set_outline_color(self, outlinecolor):
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_set_outline_color.\
        argtypes = [c_void_p, c_void_p];
        
        self._jetfuel.\
        Rectangle_2d_shape_characteristics_replacement_set_outline_color(
                        self.rect2dshapecharsref, outlinecolor.colorref);
                            