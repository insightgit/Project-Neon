#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_int
from ctypes import c_bool

class drawable(object):
    _jetfuel = None;
    drawableref = None;
    
    def get_position_x(self):
        self._jetfuel.Drawable_get_position_x.argtypes = [c_void_p];
        self._jetfuel.Drawable_get_position_x.restype = c_int;
        
        return self._jetfuel.Drawable_get_position_x(self.drawableref);
    
    def get_position_y(self):
        self._jetfuel.Drawable_get_position_y.argtypes = [c_void_p];
        self._jetfuel.Drawable_get_position_y.restype = c_int;
        
        return self._jetfuel.Drawable_get_position_y(self.drawableref);
    
    def set_position(self, x, y):
        self._jetfuel.Drawable_set_position.argtypes = [c_void_p, c_int, c_int];
        
        self._jetfuel.Drawable_set_position(self.drawableref, x, y);
    
    def delete_ref(self):
        if(self.drawableref != None):
            self._jetfuel.Drawable_delete.argtypes = [c_void_p];
            self._jetfuel.Drawable_delete(self.drawableref);

    
    def draw(self):
        self._jetfuel.Drawable_draw.argtypes = [c_void_p];
        self._jetfuel.Drawable_draw.restype = c_bool;
        
        return self._jetfuel.Drawable_draw(self.drawableref);