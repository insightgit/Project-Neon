#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_wchar_p

from jetfuel.core.messagebus import message_bus

class uis_input_actions_replacement(object):
    _jetfuel = None;
    uisinputactionsref = None;
    
    def __init__(self, jetfuelsoloader, customref=None):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        
        self._jetfuel.Uis_input_actions_replacement_new.restype = c_void_p;
        
        if(customref is None):
            self.uisinputactionsref = self._jetfuel.\
                Uis_input_actions_replacement_new();
        else:
            self.uisinputactionsref = customref;
                                        
    def get_keyboard_message_to_watch(self):
        self._jetfuel.\
        Uis_input_actions_replacement_get_keyboard_message_to_watch.argtypes = [
                                                                    c_void_p];
        self._jetfuel.\
        Uis_input_actions_replacement_get_keyboard_message_to_watch.restype = \
                                                                    c_wchar_p;
        
        return self._jetfuel.\
        Uis_input_actions_replacement_get_keyboard_message_to_watch(
                                            self.uisinputactionsref);
                                            
    def set_keyboard_message_to_watch(self, keyboardmessage):
        self._jetfuel.\
        Uis_input_actions_replacement_set_keyboard_message_to_watch.argtypes = [
                                                            c_void_p, c_wchar_p];
                                                            
        self._jetfuel.\
        Uis_input_actions_replacement_set_keyboard_message_to_watch(
                            self.uisinputactionsref, keyboardmessage);
                    
    def get_mouse_message_to_watch(self):
        self._jetfuel.\
        Uis_input_actions_replacement_get_mouse_message_to_watch.argtypes = [
                                                                    c_void_p];
        self._jetfuel.\
        Uis_input_actions_replacement_get_mouse_message_to_watch.restype = \
                                                                    c_wchar_p;
        
        return self._jetfuel.\
        Uis_input_actions_replacement_get_mouse_message_to_watch(
                                            self.uisinputactionsref);
                                            
    def set_mouse_message_to_watch(self, mousemessage):
        self._jetfuel.\
        Uis_input_actions_replacement_set_mouse_message_to_watch.argtypes = [
                                                        c_void_p, c_wchar_p];
                                                            
        self._jetfuel.\
        Uis_input_actions_replacement_set_mouse_message_to_watch(
                            self.uisinputactionsref, mousemessage);
                            
    def get_joystick_message_to_watch(self):
        self._jetfuel.\
        Uis_input_actions_replacement_get_joystick_message_to_watch.argtypes = [
                                                                    c_void_p];
        self._jetfuel.\
        Uis_input_actions_replacement_get_joystick_message_to_watch.restype = \
                                                                    c_wchar_p;
        
        return self._jetfuel.\
        Uis_input_actions_replacement_get_joystick_message_to_watch(
                                            self.uisinputactionsref);
                                            
    def set_joystick_message_to_watch(self, joystickmessage):
        self._jetfuel.\
        Uis_input_actions_replacement_set_joystick_message_to_watch.argtypes = [
                                                         c_void_p, c_wchar_p];
                                                            
        self._jetfuel.\
        Uis_input_actions_replacement_set_joystick_message_to_watch(
                            self.uisinputactionsref, joystickmessage);
                            
    def get_touch_message_to_watch(self):
        self._jetfuel.\
        Uis_input_actions_replacement_get_touch_message_to_watch.argtypes = [
                                                                    c_void_p];
        self._jetfuel.\
        Uis_input_actions_replacement_get_touch_message_to_watch.restype = \
                                                                    c_wchar_p;
        
        return self._jetfuel.\
        Uis_input_actions_replacement_get_touch_message_to_watch(
                                            self.uisinputactionsref);
                                            
    def set_touch_message_to_watch(self, touchmessage):
        self._jetfuel.\
        Uis_input_actions_replacement_set_touch_message_to_watch.argtypes = [
                                                         c_void_p, c_wchar_p];
                                                            
        self._jetfuel.\
        Uis_input_actions_replacement_set_touch_message_to_watch(
                                self.uisinputactionsref, touchmessage);
                                
    def get_message_to_send_upon_click(self):
        self._jetfuel.\
        Uis_input_actions_replacement_get_message_to_send_upon_click.argtypes = [
                                                                    c_void_p];
        self._jetfuel.\
        Uis_input_actions_replacement_get_message_to_send_upon_click.restype = \
                                                                    c_wchar_p;
        
        return self._jetfuel.\
        Uis_input_actions_replacement_get_message_to_send_upon_click(
                                                self.uisinputactionsref);
                                            
    def set_message_to_send_upon_click(self, messagetosenduponclick):
        self._jetfuel.\
        Uis_input_actions_replacement_set_message_to_send_upon_click.argtypes = [
                                                            c_void_p, c_wchar_p];
                                                            
        self._jetfuel.\
        Uis_input_actions_replacement_set_message_to_send_upon_click(
                                self.uisinputactionsref, messagetosenduponclick);

    def get_message_bus_to_send_message_to(self, jetfuelsoloader):
        self._jetfuel.\
        Uis_input_actions_replacement_get_message_bus_to_send_message_to.\
                                                        argtypes = [c_void_p];
        self._jetfuel.\
        Uis_input_actions_replacement_get_message_bus_to_send_message_to.\
                                                        restype = c_void_p;
        
        messagebuspointer = self._jetfuel.\
        Uis_input_actions_replacement_get_message_bus_to_send_message_to(
                                                    self.uisinputactionsref);
                                                    
        return message_bus(jetfuelsoloader.jetfuelso, messagebuspointer);
                                            
    def set_message_bus_to_send_message_to(self, messagebustosendmessageto):
        self._jetfuel.\
        Uis_input_actions_replacement_set_message_bus_to_send_message_to.\
                                                argtypes = [c_void_p, c_void_p];
                                                            
        self._jetfuel.\
        Uis_input_actions_replacement_set_message_bus_to_send_message_to(
        self.uisinputactionsref, messagebustosendmessageto.messagebusref);