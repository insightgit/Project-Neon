#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from os.path import abspath
from time import sleep

from jetfuel.jetfuelsoloader import jetfuelsoloader

from jetfuel.control.uisinputactionsreplacement import \
                        uis_input_actions_replacement

from jetfuel.core.messagebus import message_bus

from jetfuel.inspire.pointerbridge import pointer_bridge

from jetfuel.draw.image import image
from jetfuel.draw.sprite import sprite
from jetfuel.draw.scenemanager import scene_manager
from jetfuel.draw.rectangle2dshape import rectangle_2d_shape
from jetfuel.draw.color import color
from jetfuel.draw.circle2dshape import circle_2d_shape
from jetfuel.draw.font import font
from jetfuel.draw.text import text
from jetfuel.draw.textcharacteristicsreplacement import \
    text_characteristics_replacement
from jetfuel.draw.rectangle2dshapecharacteristicsreplacement import \
                    rectangle_2d_shape_characteristics_replacement
from jetfuel.draw.circle2dshapecharacteristicsreplacement import \
                    circle_2d_shape_characteristics_replacement

from jetfuel.gui.button import button
from jetfuel.gui.checkbox import check_box
from jetfuel.gui.dropdownbox import drop_down_box
from jetfuel.gui.menu import menu
from jetfuel.gui.buttoncharacteristicsreplacement import \
     button_characteristics_replacement
from jetfuel.gui.progressbar import progress_bar
from jetfuel.gui.slider import slider

def create_slider(jetfuelso, scenemanager, pointerbridge, messagebus):
    slidertouse = slider(jetfuelso);
    
    scenemanager.attach_drawable(slidertouse, 52);
    
    rect2dchars = rectangle_2d_shape_characteristics_replacement(jetfuelso);
    rect2dchars.set_size(200, 20);
    rect2dchars.set_fill_color(color(jetfuelso, 0, 255, 0));
    slidertouse.set_slider_rail_characteristics(rect2dchars);
    print("Set slider rail");
    
    circle2dchars = circle_2d_shape_characteristics_replacement(jetfuelso);
    circle2dchars.set_radius(30);
    circle2dchars.set_filled_circle_status(True);
    circle2dchars.set_color(color(jetfuelso, 0, 0, 255));
    print("Set slider button chars");
    slidertouse.set_slider_button_characteristics(circle2dchars);
    print("Set slider button");
    
    slidercontrolscheme = uis_input_actions_replacement(jetfuelso);
    slidercontrolscheme.set_mouse_message_to_watch("Mouse_click");
    slidercontrolscheme.set_message_bus_to_send_message_to(messagebus);
    slidertouse.set_control_scheme(slidercontrolscheme);
    
    print("Set slider control scheme");
    
    slidertouse.set_number_of_statuses(50);
    slidertouse.set_current_status(0);
    slidertouse.set_position(1750, 400);
    
    pointerbridge.send_pointer("pythonslider", slidertouse.drawableref);
    
    print("Done with creating slider.");
    
def create_progress_bar(jetfuelso, scenemanager):
    progressbarimagefilepath = abspath("progressbarholder.png");
    
    progressbarimage = image(jetfuelso);
    progressbarimage.set_image_location(progressbarimagefilepath, scenemanager);
    
    print("Constructed image");
    
    progressbar = progress_bar(jetfuelso, progressbarimage, 
                               color(jetfuelso, 255, 0, 0), 20,20,20,20,400);
   
    scenemanager.attach_drawable(progressbar, 59);
    
    progressbar.set_position(1400,100);
    
    print("Attached progressbar");
    
    while(not progressbar.has_progress_bar_completed()):
        progressbar.set_progress_bar_progress(
                                    progressbar.get_progress_bar_progress()+1);
        print("Incrementing progress");
        sleep(0.1);

def create_menu(jetfuelso, scenemanager, pointerbridge, messagebus):
    print("Creating menu...");
    
    buttonimagefilepath = abspath("button.png");
    fontfilepath = abspath("default.ttf");
    
    currentmenu = menu(jetfuelso, 500, 100, 50);
    scenemanager.attach_drawable(currentmenu, 101);
    print("Menu attachment passed")
    
    currentmenu.set_position(1500,500);
    print("Menu position setting passed");
    
    buttonchars = button_characteristics_replacement(jetfuelso);
    buttonimage = image(jetfuelso);
    buttonfont = font(jetfuelso, fontfilepath);
    buttontextchars = text_characteristics_replacement(jetfuelso,
                                                       buttonfont);
    
    buttonimage.set_image_location(buttonimagefilepath, scenemanager);
    buttontextchars.set_font_size(15);
    buttontextchars.set_text_string("Quits maybe");
    
    print("Set universal values");
    
    buttonchars.set_image(buttonimage);
    buttonchars.set_color(color(jetfuelso, 150, 0, 150, 100));
    buttonchars.set_text_chars(buttontextchars);
    currentmenu.add_button(buttonchars, "Mouse_click", "quit", messagebus);
    
    print("First button passed");
    
    buttonimage = image(jetfuelso);
    buttonimage.set_image_location(buttonimagefilepath, scenemanager);
    
    buttontextchars.set_text_string("Pointless button");
    buttonchars.set_color(color(jetfuelso, 255, 0, 255, 100));
    buttonchars.set_text_chars(buttontextchars);
    currentmenu.add_button(buttonchars, "Mouse_click", "POINTLESS", messagebus);
    
    buttonchars.delete_ref();
    buttontextchars.delete_ref();
    #buttonimage.delete_ref();
    
    pointerbridge.send_pointer("pythonmenu", currentmenu.drawableref);
    
    print("Done with creating menu.");
    
def create_drop_down_box(jetfuelso, scenemanager, pointerbridge):
    dropdownboximagefilepath = abspath("dropdownbox.png");
    fontfilepath = abspath("default.ttf");
    
    dropdownboxfont = font(jetfuelso, fontfilepath);
    
    dropdownimage = image(jetfuelso);
    
    dropdownimage.set_image_location(dropdownboximagefilepath, scenemanager);
    
    dropdownbox = drop_down_box(jetfuelso);
    
    scenemanager.attach_drawable(dropdownbox, 98);
    
    print("Attached drop down box");
    
    dropdownbox.load_base_box_image(dropdownimage, color(jetfuelso,255,255,255),
                                    45, 20);
                                    
    print("Loaded base box image");
                                    
    dropdownbox.set_uis_action_to_listen_for("Mouse_click");
    
    print("Set UIS action to listen for");
    
    dropdownboxtextchars = text_characteristics_replacement(jetfuelso,
                                                            dropdownboxfont);
    dropdownboxtextchars.set_text_color(color(jetfuelso,255,0,0));
    dropdownboxtextchars.set_font_size(15);
    
    dropdownbox.set_option_text_characteristics(dropdownboxtextchars);
                                    
    dropdownbox.add_option("Why");
    dropdownbox.add_option("hello");
    dropdownbox.add_option("there");
    
    dropdownbox.set_position(1200,500);
    
    pointerbridge.send_pointer("pythondropdownbox", dropdownbox.drawableref);
    
    dropdownboxtextchars.delete_ref();

def create_check_box(jetfuelso, scenemanager, pointerbridge):
    checkboxactiveimagefilepath = abspath("checkboxactive.png");
    checkboxdisabledimagefilepath = abspath("checkboxdisabled.png");
    fontfilepath = abspath("default.ttf");
    
    currentcheckbox = check_box(jetfuelso);
    
    scenemanager.attach_drawable(currentcheckbox, 78);
    
    checkboxactiveimage = image(jetfuelso);
    checkboxdisabledimage = image(jetfuelso);
    
    checkboxactiveimage.set_image_location(checkboxactiveimagefilepath,
                                           scenemanager);
    checkboxdisabledimage.set_image_location(checkboxdisabledimagefilepath,
                                           scenemanager);                     
    currentcheckbox.load_check_box_images(checkboxactiveimage, 
                                          checkboxdisabledimage);
    
    checkboxlabelfont = font(jetfuelso, fontfilepath);
    checkboxlabeltextchars = text_characteristics_replacement(jetfuelso,
                                                              checkboxlabelfont);
    checkboxlabeltextchars.set_text_color(color(jetfuelso,0,0,0));
    checkboxlabeltextchars.set_text_string("Python Checkbox");
    currentcheckbox.set_label_characteristics(checkboxlabeltextchars, False, 50);
    
    currentcheckbox.set_position(800, 600);
    
    currentcheckbox.set_uis_action_to_watch("Mouse_click");
    
    pointerbridge.send_pointer("pythoncheckbox", currentcheckbox.drawableref);

def create_quit_button(jetfuelso, scenemanager, messagebus, pointerbridge):
    imagefilepath = abspath("button.png");
    fontfilepath = abspath("default.ttf");
    
    print("Passed paths");
    
    currentbutton = button(jetfuelso);
    
    scenemanager.attach_drawable(currentbutton, 55);
    
    print("Passed attachment");
    
    print("Pointer="+hex(currentbutton.drawableref));
    
    buttonimage = image(jetfuelso);
    
    buttonimage.set_image_location(imagefilepath, 
                                   scenemanager);
    
    if(currentbutton.load_base_button_image(buttonimage) == True):
        print("Passed base button loading");
    else:
        print("Failed base button loading");
    
    buttoncolor = color(jetfuelso, 15, 255, 135, 100);
    
    print("Passed image construction");
    
    buttonfont = font(jetfuelso, fontfilepath);
    
    textchars = text_characteristics_replacement(jetfuelso, buttonfont);
    
    print("Constructed textchars");
    
    textchars.set_text_string("Hello");
    
    print("Set textchars string");
    
    textcolor = color(jetfuelso,0,0,0);
    
    print("Done with textchars construction!");
    
    textchars.set_text_color(buttoncolor);
    
    print("Setting text color for textchars...");
    
    currentbutton.set_button_text_characteristics(textchars);
    
    print("Passed textchars button setting.");
    
    currentbutton.set_clicked_message("quit", messagebus);
    
    currentbutton.set_button_color(buttoncolor);
    
    currentbutton.set_position(1000,500);
    
    print("Passed clicked message set.")
    
    currentbutton.set_uis_action_to_watch("Mouse_click");
    
    pointerbridge.send_pointer("pythonbutton", currentbutton.drawableref);
    
def drawabledisplay(scenemanagerpointer, messagebuspointer, 
                    pointerbridgepointer):
    print("Passed imports")
    
    jetfuelpythonapiso = abspath("libJetfuel Game Engine Python API.so");
    imagefilepath = abspath("button.png");
    fontfilepath = abspath("default.ttf");
    
    print("jetfuelso="+jetfuelpythonapiso+" imagepath="+imagefilepath+"\n"+
          "scenemanagerpointer="+str(scenemanagerpointer));
    
    jetfuelso = jetfuelsoloader(jetfuelpythonapiso);
    
    scenemanager = scene_manager(jetfuelso, scenemanagerpointer);   
    
    messagebus = message_bus(jetfuelso, messagebuspointer);
    
    pointerbridge = pointer_bridge(jetfuelso, pointerbridgepointer);
    
    currentimage = image(jetfuelso);
    
    print("Passed image construction");
    
    currentimage.set_image_location(imagefilepath, scenemanager);
    
    print("Passed image location");
    
    print("currentimage path="+str(currentimage.get_image_location())+
           " currentimage size= ("+str(currentimage.get_image_size_x())+","+
           str(currentimage.get_image_size_y())+")");
          
    currentsprite = sprite(jetfuelso);
    
    print("Passed sprite construction");
    
    currentsprite.set_position(500,500);
    
    print("Passed set position");
    
    scenemanager.attach_drawable(currentsprite, 5);
    
    if(currentsprite.load_from_image(currentimage) == True):
        print("Passed image loading");
    else:
        print("Failed image loading");  
    
    currentrectangle = rectangle_2d_shape(jetfuelso, 600, 600, 100, 100);
    
    print("Passed rectangle construction");
    
    scenemanager.attach_drawable(currentrectangle, 6);
    
    currentcolor = color(jetfuelso, 0, 175, 255, 255);
    
    print("Passed color construction");
    
    currentrectangle.set_fill_color(currentcolor);
    
    currentcircle = circle_2d_shape(jetfuelso, 800, 800, 150);
    
    print("Passed circle construction");
    
    scenemanager.attach_drawable(currentcircle, 5);
    
    currentcircle.set_color(currentcolor);
    
    currentcircle.set_filled_circle_status(True);
    
    print("Passed circle drawing");
    
    currentfont = font(jetfuelso, fontfilepath);
    
    currenttext = text(jetfuelso, currentfont);
    
    scenemanager.attach_drawable(currenttext, 10);
    
    currenttext.set_position(800, 600);
    
    currenttext.set_text_color(currentcolor);
    
    currenttext.set_string("hello");
    
    print("Passed text construction");
    
    # Possible memory leak at create_quit_button
    create_quit_button(jetfuelso, scenemanager, messagebus,
                        pointerbridge);      
    create_check_box(jetfuelso, scenemanager, pointerbridge);
    create_drop_down_box(jetfuelso, scenemanager, pointerbridge);
    create_menu(jetfuelso, scenemanager, pointerbridge, messagebus);
    create_slider(jetfuelso, scenemanager, pointerbridge, messagebus);
    create_progress_bar(jetfuelso, scenemanager);
    
    print("Finished python tasks!")