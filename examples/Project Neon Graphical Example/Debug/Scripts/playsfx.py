#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from sys import path
from os.path import abspath

def playsfx():
    jetfuelpythonapi = abspath("../PythonAPIWrappers/");
    jetfuelpythonapiso = abspath("../libJetfuel Game Engine Python API.so");
    musicpath = abspath("../sfx.ogg");
    
    path.append(jetfuelpythonapi);
    
    print("jetfuelso="+jetfuelpythonapiso+" jetfuelapi="+jetfuelpythonapi+
      " musicpath="+musicpath);
    
    from jetfuel.media.sound_effect import sound_effect
    from jetfuel.jetfuelsoloader import jetfuelsoloader
    
    jetfuelso = jetfuelsoloader(jetfuelpythonapiso);
    
    currentsfx = sound_effect(jetfuelso);
    
    if(currentsfx.load_audio_file(musicpath) == False):
        print("Could not open sound effect. Error was: "+
              jetfuelso.get_sdl_error());
              
    if(currentsfx.play() == False):
        print("Could not play sound effect. Error was: "+
              jetfuelso.get_sdl_error());
              
    while(sound_effect.is_sound_effect_or_music_playing(jetfuelso) == True):
        print("Sound effect is playing");
    
if(__name__== "__main__"):
    playsfx()