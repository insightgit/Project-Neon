#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_int

from jetfuel.draw.drawable import drawable

class rectangle_interface(drawable):
    def get_size_width(self):
        self._jetfuel.Rectangle_interface_get_size_width.argtypes = [c_void_p];
        self._jetfuel.Rectangle_interface_get_size_width.restype = c_int;
        
        return self._jetfuel.Rectangle_interface_get_size_width(
                              self.drawableref);
    def get_size_height(self):
        self._jetfuel.Rectangle_interface_get_size_height.argtypes = [c_void_p];
        self._jetfuel.Rectangle_interface_get_size_height.restype = c_int;
        
        return self._jetfuel.Rectangle_interface_get_size_height(
                              self.drawableref); 