#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_int
from ctypes import c_wchar_p
from ctypes import c_bool

from jetfuel.draw.color import color 

class text_characteristics_replacement(object):
    _jetfuel = None;
    textcharsreplacementref = None;

    def __init__(self, jetfuelsoloader, font=None):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        
        if(font is not None):
            self._jetfuel.Text_characteristics_new_from_font.argtypes = [
                                                             c_void_p]
            
            self._jetfuel.Text_characteristics_new_from_font.restype = c_void_p;
            
            self.textcharsreplacementref = self._jetfuel.\
              Text_characteristics_new_from_font(font.fontref);
        else:
            self._jetfuel.Text_characteristics_new.restype = c_void_p;
        
            self.textcharsreplacementref = self._jetfuel.\
             Text_characteristics_new();       
    
    def delete_ref(self):
        if(self.textcharsreplacementref is not None): 
            self._jetfuel.Text_characteristics_delete.argtypes = [c_void_p];
            
            self._jetfuel.Text_characteristics_delete(
                                             self.textcharsreplacementref);
             
    def get_render_mode(self):
        self._jetfuel.Text_characteristics_get_render_mode.\
             argtypes = [c_void_p];
        self._jetfuel.Text_characteristics_get_render_mode.\
            restype = c_int;
            
        return self._jetfuel.Text_characteristics_get_render_mode(
                    self.textcharsreplacementref);
            
    def set_render_mode(self, rendermode):
        self._jetfuel.Text_characteristics_set_render_mode.\
             argtypes = [c_void_p, c_int];
             
        self._jetfuel.Text_characteristics_set_render_mode(
            self.textcharsreplacementref, rendermode);
             
    def get_text_string(self):
        self._jetfuel.Text_characteristics_get_text_string.\
             argtypes = [c_void_p];
        self._jetfuel.Text_characteristics_get_text_string.\
             restype = c_wchar_p;
             
        return self._jetfuel.Text_characteristics_get_text_string(
             self.textcharsreplacementref);
             
    def set_text_string(self, textstring):
        self._jetfuel.Text_characteristics_set_text_string.\
             argtypes = [c_void_p, c_wchar_p];
             
        self._jetfuel.Text_characteristics_set_text_string(
            self.textcharsreplacementref, textstring);
            
    def get_text_color(self, jetfuelsoloader):
        self._jetfuel.Text_characteristics_get_text_color.argtypes = [c_void_p];
        self._jetfuel.Text_characteristics_get_text_color.restype = c_void_p;
        
        currentcolor = color(jetfuelsoloader.jetfuelso);
        
        self._jetfuel.Color_delete.argtypes = [c_void_p];
        
        self._jetfuel.Color_delete(currentcolor.colorref);
        
        currentcolor.colorref = self._jetfuel.\
         Text_characteristics_get_text_color(self.textcharsreplacementref);
                                                 
        return currentcolor;
    
    def set_text_color(self, textcolor):
        self._jetfuel.Text_characteristics_set_text_color.argtypes = [c_void_p, 
                                                                      c_void_p];
        
        self._jetfuel.Text_characteristics_set_text_color(
                  self.textcharsreplacementref, textcolor.colorref);
                                          
    def get_background_color(self, jetfuelsoloader):
        self._jetfuel.Text_characteristics_get_background_color.argtypes = [
                                                                c_void_p];
        self._jetfuel.Text_characteristics_get_background_color.restype = \
                                                                c_void_p;
        
        currentcolor = color(jetfuelsoloader.jetfuelso);
        
        self._jetfuel.Color_delete.argtypes = [c_void_p];
        
        self._jetfuel.Color_delete(currentcolor.colorref);
        
        currentcolor.colorref = self._jetfuel.\
        Text_characteristics_get_background_color(self.textcharsreplacementref);
                                                 
        return currentcolor;
    
    def set_background_color(self, backgroundcolor):
        self._jetfuel.Text_set_background_color.argtypes = [c_void_p, c_void_p];
        
        self._jetfuel.Text_set_background_color(self.textcharsreplacementref,
                                                backgroundcolor.colorref);
                                                
    def get_font_outline_width(self):
        self._jetfuel.Text_characteristics_get_font_outline_width.\
             argtypes = [c_void_p];
        self._jetfuel.Text_characteristics_get_font_outline_width.\
             restype = c_int;
            
        return self._jetfuel.\
            Text_characteristics_get_font_outline_width(
                self.textcharsreplacementref);
            
    def set_font_outline_width(self, fontoutline):
        self._jetfuel.Text_characteristics_set_font_outline_width.\
             argtypes = [c_void_p, c_int];
             
        self._jetfuel.Text_characteristics_set_font_outline_width(
            self.textcharsreplacementref, fontoutline);
            
    def get_font_kerning_status(self):
        self._jetfuel.Text_characteristics_get_font_kerning_status.\
             argtypes = [c_void_p];
        self._jetfuel.Text_characteristics_get_font_kerning_status.\
             restype = c_bool;
            
        return self._jetfuel.\
            Text_characteristics_get_font_kerning_status(
                self.textcharsreplacementref);
            
    def set_font_kerning_status(self, kerningstatus):
        self._jetfuel.Text_characteristics_set_font_kerning_status.\
             argtypes = [c_void_p, c_bool];
             
        self._jetfuel.Text_characteristics_set_font_kerning_status(
            self.textcharsreplacementref, kerningstatus);
            
    def get_font_style(self):
        self._jetfuel.Text_characteristics_get_font_style.\
             argtypes = [c_void_p];
        self._jetfuel.Text_characteristics_get_font_style.\
             restype = c_int;
            
        return self._jetfuel.\
            Text_characteristics_get_font_style(
                self.textcharsreplacementref);
            
    def set_font_style(self, fontstyle):
        self._jetfuel.Text_characteristics_set_font_style.\
             argtypes = [c_void_p, c_int];
             
        self._jetfuel.Text_characteristics_set_font_style(
            self.textcharsreplacementref, fontstyle);
            
    def get_font_size(self):
        self._jetfuel.Text_characteristics_get_font_size.\
             argtypes = [c_void_p];
        self._jetfuel.Text_characteristics_get_font_size.\
             restype = c_int;
            
        return self._jetfuel.\
            Text_characteristics_get_font_size(
                self.textcharsreplacementref);
            
    def set_font_size(self, fontsize):
        self._jetfuel.Text_characteristics_set_font_size.\
             argtypes = [c_void_p, c_int];
             
        self._jetfuel.Text_characteristics_set_font_size(
            self.textcharsreplacementref, fontsize);
            
    def get_font_hinting(self):
        self._jetfuel.Text_characteristics_get_font_hinting.\
             argtypes = [c_void_p];
        self._jetfuel.Text_characteristics_get_font_hinting.\
             restype = c_int;
            
        return self._jetfuel.\
            Text_characteristics_get_font_hinting(
                self.textcharsreplacementref);
            
    def set_font_hinting(self, fonthinting):
        self._jetfuel.Text_characteristics_set_font_hinting.\
             argtypes = [c_void_p, c_int];
             
        self._jetfuel.Text_characteristics_set_font_hinting(
            self.textcharsreplacementref, fonthinting);
            
    def get_font(self):
        self._jetfuel.Text_characteristics_get_font.\
             argtypes = [c_void_p];
        self._jetfuel.Text_characteristics_get_font.\
             restype = c_void_p;
            
        return self._jetfuel.\
            Text_characteristics_get_font(
                self.textcharsreplacementref);
