from ctypes import c_void_p
from ctypes import c_int

class scene_manager(object):
    _jetfuel = None;
    scenemanagerref = None;

    def __init__(self, jetfuelsoloader, scenemanagerpointer):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        self.scenemanagerref = scenemanagerpointer;
        
    def attach_drawable(self, drawable, drawableweight):
        self._jetfuel.Scene_attach_drawable.argtypes = [c_void_p,
                                                        c_void_p,
                                                        c_int];
        self._jetfuel.Scene_attach_drawable(self.scenemanagerref,
                                           drawable.drawableref,
                                           drawableweight);
    def disable_drawable(self, drawable):
        self._jetfuel.Scene_disable_drawable.argtypes = [c_void_p,
                                                        c_void_p];
        
        self._jetfuel.Scene_disable_drawable(self.scenemanagerref,
                                             drawable.drawableref);