#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_int
from ctypes import c_uint
from ctypes import c_bool

from jetfuel.draw.rectangleinterface import rectangle_interface

class progress_bar(rectangle_interface):

    def __init__(self, jetfuelsoloader,
                 progressbarimage=None, progressbarcolor=None,
                 progressbarholderborderx=None, progressbarholderbordery=None,
                 progressbarholderborderwidth=None, 
                 progressbarholderborderheight=None, 
                 progressbarmax=None):
        self._jetfuel = jetfuelsoloader.jetfuelso; 
        
        if(progressbarimage is not None and progressbarcolor is not None
           and progressbarholderborderx is not None 
           and progressbarholderbordery is not None 
           and progressbarholderborderwidth is not None
           and progressbarholderborderheight is not None
           and progressbarmax is not None):
            self._jetfuel.Progress_bar_new_set_progress_bar.argtypes = [
                c_void_p, c_void_p, c_int, c_int, c_int, c_int, c_int];
            self._jetfuel.Progress_bar_new_set_progress_bar.restype = c_void_p;
            
            self.drawableref = self._jetfuel.Progress_bar_new_set_progress_bar(
                                                progressbarimage.imageref,
                                                progressbarcolor.colorref,
                                                progressbarholderborderx,
                                                progressbarholderbordery,
                                                progressbarholderborderwidth,
                                                progressbarholderborderheight,
                                                progressbarmax);
        else:
            self._jetfuel.Progress_bar_new.restype = c_void_p;
            
            self.drawableref = self._jetfuel.Progress_bar_new();
            
    def set_progress_bar(self, progressbarimage, progressbarcolor,
                         progressbarholderborderx, progressbarholderbordery,
                         progressbarholderborderwidth, 
                         progressbarholderborderheight, 
                         progressbarmax):
            self._jetfuel.Progress_bar_new_set_progress_bar.argtypes = [
                c_void_p, c_void_p, c_int, c_int, c_int, c_int, c_int];
            self._jetfuel.Progress_bar_new_set_progress_bar.restype = c_void_p;
            
            self.drawableref = self._jetfuel.Progress_bar_new_set_progress_bar(
                                                progressbarimage.imageref,
                                                progressbarcolor.colorref,
                                                progressbarholderborderx,
                                                progressbarholderbordery,
                                                progressbarholderborderwidth,
                                                progressbarholderborderheight,
                                                progressbarmax);
                                                
    def get_progress_bar_progress(self):
        self._jetfuel.Progress_bar_get_progress_bar_progress.argtypes = [
                                                                    c_void_p];
        self._jetfuel.Progress_bar_get_progress_bar_progress.restype = c_uint;       
        
        return self._jetfuel.Progress_bar_get_progress_bar_progress(
                                                            self.drawableref);
                                                            
    def set_progress_bar_progress(self, progressbarprogress):
        self._jetfuel.Progress_bar_set_progress_bar_progress.argtypes = [
                                                            c_void_p, c_uint];
        self._jetfuel.Progress_bar_set_progress_bar_progress(
                                                self.drawableref,
                                                progressbarprogress);                                                     
                                                    
    def get_max_progress_bar(self):
        self._jetfuel.Progress_bar_get_max_progress_bar.argtypes = [c_void_p];
        self._jetfuel.Progress_bar_get_max_progress_bar.restype = c_uint;       
        
        return self._jetfuel.Progress_bar_get_max_progress_bar(
                                                            self.drawableref);
                                                            
    def has_progress_bar_completed(self):
        self._jetfuel.Progress_bar_has_progress_bar_completed.argtypes = [
                                                                    c_void_p];
        self._jetfuel.Progress_bar_has_progress_bar_completed.restype = c_bool;
        
        return self._jetfuel.Progress_bar_has_progress_bar_completed(
                                                            self.drawableref);
                                                            
    def get_position_x(self):
        self._jetfuel.Progress_bar_get_position_x.argtypes = [c_void_p];
        self._jetfuel.Progress_bar_get_position_x.restype = c_int;
        
        return self._jetfuel.Progress_bar_get_position_x(self.drawableref);
    
    def get_position_y(self):
        self._jetfuel.Progress_bar_get_position_y.argtypes = [c_void_p];
        self._jetfuel.Progress_bar_get_position_y.restype = c_int;
        
        return self._jetfuel.Progress_bar_get_position_y(self.drawableref);
    
    def set_position(self, x, y):
        self._jetfuel.Progress_bar_set_position.argtypes = [c_void_p, c_int, 
                                                            c_int];
                                                            
        self._jetfuel.Progress_bar_set_position(self.drawableref, x, y);
        
    def get_size_width(self):
        self._jetfuel.Progress_bar_get_size_width.argtypes = [c_void_p];
        self._jetfuel.Progress_bar_get_size_width.restype = c_int;
        
        return self._jetfuel.Progress_bar_get_size_width(self.drawableref);
    
    def get_size_height(self):
        self._jetfuel.Progress_bar_get_size_height.argtypes = [c_void_p];
        self._jetfuel.Progress_bar_get_size_height.restype = c_int;
        
        return self._jetfuel.Progress_bar_get_size_height(self.drawableref);