#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p

from jetfuel.draw.image import image
from jetfuel.draw.color import color
from jetfuel.draw.textcharacteristicsreplacement import \
     text_characteristics_replacement

class button_characteristics_replacement(object):
    _jetfuel = None;
    buttoncharsref = None;
    
    def __init__(self, jetfuelsoloader):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        
        self._jetfuel.Button_characteristics_replacement_new.\
             restype = c_void_p;
        
        self.buttoncharsref = self._jetfuel.\
                Button_characteristics_replacement_new();
        
    def delete_ref(self):
        self._jetfuel.Button_characteristics_replacement_delete.\
                    argtypes = [c_void_p];
        
        self._jetfuel.Button_characteristics_replacement_delete(
                                            self.buttoncharsref);
        
    def get_image(self, jetfuelsoloader):    
        self._jetfuel.Button_characteristics_replacement_get_image.\
                                               argtypes = [c_void_p];
        self._jetfuel.Button_characteristics_replacement_get_image.\
                                                restype = c_void_p;
        
        returnvalue = image(jetfuelsoloader);
        
        returnvalue.delete_ref();
        
        returnvalue.imageref = self._jetfuel.\
            Button_characteristics_replacement_get_image(self.buttoncharsref);
        
        return returnvalue;
    
    def set_image(self, image):
        self._jetfuel.Button_characteristics_replacement_set_image.argtypes = [
                                                                   c_void_p, 
                                                                   c_void_p];
                                                                   
        self._jetfuel.Button_characteristics_replacement_set_image(
                                                       self.buttoncharsref,
                                                       image.imageref);
                                                       
    def get_color(self, jetfuelsoloader):
        self._jetfuel.Button_characteristics_replacement_get_color.\
             argtypes = [c_void_p];
        self._jetfuel.Button_characteristics_get_color.restype = c_void_p;
        
        returnvalue = color(jetfuelsoloader);
        
        returnvalue.delete_ref();
        
        returnvalue.colorref = self._jetfuel.Button_characteristics_get_color(
                                                          self.buttoncharsref);
        
        return returnvalue;
    
    def set_color(self, color):
        self._jetfuel.Button_characteristics_replacement_set_color.\
                                            argtypes = [c_void_p, c_void_p];
                                                                   
        self._jetfuel.Button_characteristics_replacement_set_color(
                                    self.buttoncharsref,color.colorref);
                                                       
    def get_text_chars(self, jetfuelsoloader):
        self._jetfuel.Button_characteristics_replacement_get_text_chars.\
                                                    argtypes = [c_void_p];
        self._jetfuel.Button_characteristics_replacement_get_text_chars.\
                                                    restype = c_void_p;
        
        returnvalue = text_characteristics_replacement(jetfuelsoloader);
        
        returnvalue.delete_ref();
        
        returnvalue.colorref = self._jetfuel.\
            Button_characteristics_replacement_get_color(self.buttoncharsref);
        
        return returnvalue;
    
    def set_text_chars(self, textchars):
        self._jetfuel.Button_characteristics_replacement_set_text_chars.\
                                                        argtypes = [c_void_p, 
                                                                    c_void_p];
                                                                   
        self._jetfuel.Button_characteristics_replacement_set_text_chars(
                        self.buttoncharsref, textchars.textcharsreplacementref);