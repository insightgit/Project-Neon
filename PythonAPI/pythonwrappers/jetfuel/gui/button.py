#    Project Neon- A moddable 2D RPG game engine
#    Copyright (C) 2017 InfernoStudios
# 
#   Project Neon is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   Project Neon is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.

from ctypes import c_void_p
from ctypes import c_bool
from ctypes import c_wchar_p
from ctypes import c_int

from jetfuel.draw.rectangleinterface import rectangle_interface


class button(rectangle_interface):
    
    def __init__(self, jetfuelsoloader):
        self._jetfuel = jetfuelsoloader.jetfuelso;
        
        self._jetfuel.Button_new.restype = c_void_p;
        
        self.drawableref = self._jetfuel.Button_new();
        
    def load_base_button_image(self, buttonimage):
        self._jetfuel.Button_load_base_button_image.argtypes = [c_void_p,
                                                                c_void_p];
        
        self._jetfuel.Button_load_base_button_image(self.drawableref,
                                                    buttonimage.imageref);
                                                           
    def set_button_text_characteristics(self, 
                                        buttontextcharacteristicsreplacement):
        self._jetfuel.Button_set_button_text_characteristics.argtypes = [
                                                    c_void_p, c_void_p];
        self._jetfuel.Button_set_button_text_characteristics.restype = c_bool;
        
        self._jetfuel.Button_set_button_text_characteristics(
                self.drawableref,
                buttontextcharacteristicsreplacement.textcharsreplacementref);
                
    def set_clicked_message(self, clickedmessage, messagebus):
        self._jetfuel.Button_set_clicked_message.argtypes = [c_void_p, c_wchar_p,
                                                             c_void_p];
        
        self._jetfuel.Button_set_clicked_message(self.drawableref,
                                                 clickedmessage,
                                                 messagebus.messagebusref);
                                                        
    def set_button_color(self, color):
        self._jetfuel.Button_set_button_color.argtypes = [c_void_p, 
                                                          c_void_p];
        
        self._jetfuel.Button_set_button_color(self.drawableref,
                                              color.colorref);
                
        
    def set_uis_action_to_watch(self, uisaction):
        self._jetfuel.Button_set_UIS_action_to_watch.argtypes = [c_void_p, 
                                                                 c_wchar_p];
        
        self._jetfuel.Button_set_UIS_action_to_watch(self.drawableref,
                                                            uisaction);                                             
    def get_position_x(self):
        self._jetfuel.Button_get_position_x.argtypes = [c_void_p];
        self._jetfuel.Button_get_position_x.restype = c_int;
        
        return self._jetfuel.Button_get_position_x(self.drawableref);
    
    def get_position_y(self):
        self._jetfuel.Button_get_position_y.argtypes = [c_void_p];
        self._jetfuel.Button_get_position_y.restype = c_int;
        
        return self._jetfuel.Button_get_position_y(self.drawableref);
    
    def set_position(self, x, y):
        self._jetfuel.Button_set_position.argtypes = [c_void_p, c_int, c_int];
        
        self._jetfuel.Button_set_position(self.drawableref, x, y);