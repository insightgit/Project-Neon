/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELCONTROL_UISINPUTACTIONSREPLACEMENT_H_
#define JETFUELCONTROL_UISINPUTACTIONSREPLACEMENT_H_

#include <string>
#include <locale>
#include <codecvt>

#include <jetfuelcontrol.h>

namespace jetfuel {
    namespace control {
        class Uis_input_actions_replacement {
        public:
            Uis_input_actions_replacement() {}

            Uis_input_actions_replacement(
                    jetfuel::control::UIS_input_actions inputactions){
                std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>
                                                    wstringconvertor;

                keyboardmessagetowatch = wstringconvertor.from_bytes(
                                inputactions.keyboardmessagetowatch);
                mousemessagetowatch = wstringconvertor.from_bytes(
                                inputactions.mousemessagetowatch);
                joystickmessagetowatch = wstringconvertor.from_bytes(
                                inputactions.joystickmessagetowatch);
                touchmessagetowatch = wstringconvertor.from_bytes(
                                inputactions.touchmessagetowatch);
                messagetosenduponclick = wstringconvertor.from_bytes(
                                inputactions.messagetosenduponclick);

                messagebustosendmessageto =
                inputactions.messagebustosendmessageto;
            }

            jetfuel::control::UIS_input_actions
              Convert_to_uis_input_actions(){
                jetfuel::control::UIS_input_actions returnvalue;

                returnvalue.keyboardmessagetowatch =
                        std::string(keyboardmessagetowatch.begin(),
                                    keyboardmessagetowatch.end());;
                returnvalue.mousemessagetowatch =
                        std::string(mousemessagetowatch.begin(),
                                    mousemessagetowatch.end());
                returnvalue.joystickmessagetowatch =
                        std::string(joystickmessagetowatch.begin(),
                                    joystickmessagetowatch.end());
                returnvalue.touchmessagetowatch =
                        std::string(touchmessagetowatch.begin(),
                                    touchmessagetowatch.end());

                returnvalue.messagetosenduponclick =
                        std::string(messagetosenduponclick.begin(),
                                    messagetosenduponclick.end());
                returnvalue.messagebustosendmessageto =
                              messagebustosendmessageto;

                return returnvalue;
            }

            std::wstring keyboardmessagetowatch;
            std::wstring mousemessagetowatch;
            std::wstring joystickmessagetowatch;
            std::wstring touchmessagetowatch;

            std::wstring messagetosenduponclick;
            jetfuel::core::Message_bus *messagebustosendmessageto;
        };
    } /* namespace control */
} /* namespace jetfuel */

#endif /* JETFUELCONTROL_UISINPUTACTIONSREPLACEMENT_H_ */
