/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <jetfuelgui.h>

#include "../jetfueldraw/textcharacteristicsreplacement.h"

namespace jetfuel{
    namespace gui{
        class Button_characteristics_replacement{
        public:
            Button_characteristics_replacement(){}

            Button_characteristics_replacement(jetfuel::gui::Menu::
                                      Button_characteristics buttonchars){
                image = buttonchars.image;
                color = buttonchars.color;

                textcharsreplacement =
                 jetfuel::draw::Text_characteristics_replacement(buttonchars.
                                                             buttontextchars);
            }

            jetfuel::gui::Menu::Button_characteristics
             Convert_to_button_chars_struct(){
                jetfuel::gui::Menu::Button_characteristics returnvalue;

                returnvalue.image = image;
                returnvalue.color = color;
                returnvalue.buttontextchars = textcharsreplacement.
                                        Convert_to_text_chars_struct();

                return returnvalue;
            }

            jetfuel::draw::Image image;
            jetfuel::draw::Color color;
            jetfuel::draw::Text_characteristics_replacement textcharsreplacement;
        };
    }
}

