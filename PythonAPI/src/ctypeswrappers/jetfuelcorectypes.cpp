/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <jetfuelcore.h>

#define MAX_MESSAGE_SIZE 131072

extern "C"{
    // Wrappers for jetfuel::core::Message_bus

    void Message_bus_post_message_to_message_bus(jetfuel::core::Message_bus *messagebus,
                                      const wchar_t *message){
        char cstringmessage[MAX_MESSAGE_SIZE];

        wcstombs(cstringmessage,message,
                 MAX_MESSAGE_SIZE);

        messagebus->Post_message(cstringmessage);
    }

    bool Message_bus_does_message_exist(jetfuel::core::Message_bus *messagebus,
                                        const wchar_t *message){
        char cstringmessage[MAX_MESSAGE_SIZE];

        wcstombs(cstringmessage,message,
                 MAX_MESSAGE_SIZE);

        return messagebus->Does_message_exist(cstringmessage);
    }
}
