/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <jetfuelmedia.h>

#define MAX_FILE_NAME_SIZE 4096

extern "C"{
    // Wrapper for jetfuel::media::Music

    jetfuel::media::Music *Music_new(){
        return new jetfuel::media::Music();
    }

    void Music_delete(jetfuel::media::Music *music){
        delete music;
    }

    bool Music_is_music_playing(){
        return Mix_PlayingMusic();
    }

    bool Music_is_music_paused(){
        return Mix_PausedMusic();
    }

    bool Music_load_audio_file(jetfuel::media::Music *music,
                               const wchar_t *musicfilepath){
        char musicfilepathchar[MAX_FILE_NAME_SIZE];

        wcstombs(musicfilepathchar,musicfilepath,
                MAX_FILE_NAME_SIZE);

        if(!music->Load_audio_file(musicfilepathchar)){
            return false;
        }

        return true;
    }

    bool Music_play(jetfuel::media::Music *music){
        return music->Play();
    }

    void Music_pause(jetfuel::media::Music *music){
        music->Pause();
    }

    void Music_resume(jetfuel::media::Music *music){
        music->Resume();
    }

   wchar_t *Get_sdl_error(){
        const char *sdlerror = Mix_GetError();

        wchar_t returnvalue[4096];

        mbstowcs(returnvalue, sdlerror, 4096);

        return returnvalue;
    }

    // Wrapper for jetfuel::media::Sound_effect

    jetfuel::media::Sound_effect *Sound_effect_new(){
        return new jetfuel::media::Sound_effect();
    }

    void Sound_effect_delete(jetfuel::media::Sound_effect *sfx){
        delete sfx;
    }

    bool Sound_effect_is_sound_effect_or_music_playing(){
        return jetfuel::media::Sound_effect::Is_sound_effect_or_music_playing();
    }

    wchar_t *Sound_effect_get_loaded_sound_effect_file(jetfuel::media::
                                                       Sound_effect *sfx){
        const char *soundeffectfile = sfx->Get_loaded_audio_file_path().c_str();
        wchar_t returnvalue[MAX_FILE_NAME_SIZE];

        mbstowcs(returnvalue, soundeffectfile, MAX_FILE_NAME_SIZE);

        return returnvalue;
    }

    bool Sound_effect_load_audio_file(jetfuel::media::Sound_effect *sfx,
                                      const wchar_t *musicfilepath){
        char musicfilepathchar[MAX_FILE_NAME_SIZE];

        wcstombs(musicfilepathchar,musicfilepath,
                MAX_FILE_NAME_SIZE);

        if(!sfx->Load_audio_file(musicfilepathchar)){
            return false;
        }

        return true;
    }

    unsigned int Sound_effect_get_times_to_repeat(jetfuel::media::Sound_effect
                                                  *sfx){
        return sfx->Get_times_to_repeat();
    }

    void Sound_effect_set_times_to_repeat(jetfuel::media::Sound_effect *sfx,
                                          unsigned int timestorepeat){
        return sfx->Set_times_to_repeat(timestorepeat);
    }

    bool Sound_effect_play(jetfuel::media::Sound_effect *sfx){
        return sfx->Play();
    }

    void Sound_effect_pause(jetfuel::media::Sound_effect *sfx){
        sfx->Pause();
    }

    void Sound_effect_resume(jetfuel::media::Sound_effect *sfx){
        sfx->Resume();
    }
}
