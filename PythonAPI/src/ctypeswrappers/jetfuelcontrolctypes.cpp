/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../jetfuelcontrol/uisinputactionsreplacement.h"

extern "C"{
    // jetfuel::control::Uis_input_actions_replacement

    jetfuel::control::Uis_input_actions_replacement
    *Uis_input_actions_replacement_new(){
        return new jetfuel::control::Uis_input_actions_replacement();
    }

    wchar_t *Uis_input_actions_replacement_get_keyboard_message_to_watch(
               jetfuel::control::Uis_input_actions_replacement *uisinputactions){
        std::wstring messagetowatch = uisinputactions->keyboardmessagetowatch;

        wchar_t *returnvalue = new wchar_t[messagetowatch.size()+1];

        wcscpy(returnvalue, messagetowatch.c_str());

        return returnvalue;
    }

    void Uis_input_actions_replacement_set_keyboard_message_to_watch(
            jetfuel::control::Uis_input_actions_replacement *uisinputactions,
            wchar_t *keyboardmessagetowatch){
        uisinputactions->keyboardmessagetowatch = std::wstring(
                                                     keyboardmessagetowatch);
    }

    wchar_t *Uis_input_actions_replacement_get_mouse_message_to_watch(
               jetfuel::control::Uis_input_actions_replacement *uisinputactions){
        std::wstring messagetowatch = uisinputactions->mousemessagetowatch;

        wchar_t *returnvalue = new wchar_t[messagetowatch.size()+1];

        wcscpy(returnvalue, messagetowatch.c_str());

        return returnvalue;
    }

    void Uis_input_actions_replacement_set_mouse_message_to_watch(
            jetfuel::control::Uis_input_actions_replacement *uisinputactions,
            wchar_t *mousemessagetowatch){
        uisinputactions->mousemessagetowatch = std::wstring(mousemessagetowatch);
    }

    wchar_t *Uis_input_actions_replacement_get_joystick_message_to_watch(
               jetfuel::control::Uis_input_actions_replacement *uisinputactions){
        std::wstring messagetowatch = uisinputactions->joystickmessagetowatch;

        wchar_t *returnvalue = new wchar_t[messagetowatch.size()+1];

        wcscpy(returnvalue, messagetowatch.c_str());

        return returnvalue;
    }

    void Uis_input_actions_replacement_set_joystick_message_to_watch(
            jetfuel::control::Uis_input_actions_replacement *uisinputactions,
            wchar_t *joystickmessagetowatch){
        uisinputactions->joystickmessagetowatch = std::wstring(
                                                    joystickmessagetowatch);
    }

    wchar_t *Uis_input_actions_replacement_get_touch_message_to_watch(
               jetfuel::control::Uis_input_actions_replacement *uisinputactions){
        std::wstring messagetowatch = uisinputactions->touchmessagetowatch;

        wchar_t *returnvalue = new wchar_t[messagetowatch.size()+1];

        wcscpy(returnvalue, messagetowatch.c_str());

        return returnvalue;
    }

    void Uis_input_actions_replacement_set_touch_message_to_watch(
            jetfuel::control::Uis_input_actions_replacement *uisinputactions,
            wchar_t *touchmessagetowatch){
        uisinputactions->touchmessagetowatch = std::wstring(touchmessagetowatch);
    }

    wchar_t *Uis_input_actions_replacement_get_message_to_send_upon_click(
               jetfuel::control::Uis_input_actions_replacement *uisinputactions){
        std::wstring messagetosend = uisinputactions->messagetosenduponclick;

        wchar_t *returnvalue = new wchar_t[messagetosend.size()+1];

        wcscpy(returnvalue, messagetosend.c_str());

        return returnvalue;
    }

    void Uis_input_actions_replacement_set_message_to_send_upon_click(
            jetfuel::control::Uis_input_actions_replacement *uisinputactions,
            wchar_t *messagetosenduponclick){
        uisinputactions->messagetosenduponclick = std::wstring(
                                                    messagetosenduponclick);
    }

    jetfuel::core::Message_bus
     *Uis_input_actions_replacement_get_message_bus_to_send_message_to(jetfuel::
                     control::Uis_input_actions_replacement *uisinputactions){
        return uisinputactions->messagebustosendmessageto;
    }

    void Uis_input_actions_replacement_set_message_bus_to_send_message_to(
            jetfuel::control::Uis_input_actions_replacement *uisinputactions,
                          jetfuel::core::Message_bus *messagebustosendmessageto){
        uisinputactions->messagebustosendmessageto = messagebustosendmessageto;
    }
}
