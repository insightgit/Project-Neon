/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <jetfuelinspire/pointerbridge.h>

extern "C"{
    void *Pointer_bridge_recieve_pointer(jetfuel::inspire::Pointer_bridge
                                         *pointerbridge, wchar_t *id,
                                         bool *found){
        std::wstring idwstring(id);

        std::string idstring(idwstring.begin(), idwstring.end());

        return pointerbridge->Recieve_pointer(idstring, found);
    }

    void Pointer_bridge_send_pointer(jetfuel::inspire::Pointer_bridge
                                     *pointerbridge, wchar_t *id,
                                     void *pointer){
        std::wstring idwstring(id);

        std::string idstring(idwstring.begin(), idwstring.end());

        return pointerbridge->Send_pointer(idstring, pointer);
    }
}
