/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <jetfuelgui.h>

#include <locale>
#include <codecvt>

#include "../jetfuelcontrol/uisinputactionsreplacement.h"
#include "../jetfueldraw/textcharacteristicsreplacement.h"
#include "../jetfueldraw/rectangle2dshapecharacteristicsreplacement.h"
#include "../jetfueldraw/circle2dshapecharacteristicsreplacement.h"
#include "../jetfuelgui/buttoncharacteristicsreplacement.h"

extern "C"{
    // jetfuel::gui::Button wrappers

    jetfuel::gui::Button *Button_new(){
        return new jetfuel::gui::Button();
    }

    void Button_load_base_button_image(jetfuel::gui::Button *button,
                                       jetfuel::draw::Image *buttonbaseimage){
        button->Dynamic_load_base_button_image(*buttonbaseimage);
    }

    void Button_set_button_color(jetfuel::gui::Button *button,
                                 jetfuel::draw::Color *color){
        button->Set_button_color(*color);
    }

    void Button_set_button_text_characteristics(jetfuel::gui::Button *button,
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        button->Set_button_text_characteristics(
                textcharsreplacement->Convert_to_text_chars_struct());
    }

    void Button_set_clicked_message(jetfuel::gui::Button *button,
                                    wchar_t *clickedmessage,
                                    jetfuel::core::Message_bus *messagebus){
        const size_t charsize = wcslen(clickedmessage)+1;

        char *clickedmessagecstring = new char(charsize)+1;

        wcstombs(clickedmessagecstring, clickedmessage,
                 charsize);

        button->Set_clicked_message(clickedmessagecstring, messagebus);
    }

    void Button_set_UIS_action_to_watch(jetfuel::gui::Button *button,
                                        wchar_t *uisaction){
        const size_t charsize = wcslen(uisaction)+1;

        char *uisactioncstring = new char(charsize)+1;

        wcstombs(uisactioncstring, uisaction,
                 charsize);

        button->Set_UIS_action_to_watch(uisactioncstring);
    }

    int Button_get_position_x(jetfuel::gui::Button *button){
        return button->Get_position().x;
    }

    int Button_get_position_y(jetfuel::gui::Button *button){
        return button->Get_position().y;
    }

    void Button_set_position(jetfuel::gui::Button *button, int x, int y){
        button->Set_position(jetfuel::draw::Vector2d_int(x,y));
    }

    // jetfuel::gui::Check_box wrappers

    jetfuel::gui::Check_box *Check_box_new(){
        return new jetfuel::gui::Check_box();
    }

    void Check_box_load_check_box_images(jetfuel::gui::Check_box *checkbox,
                                         jetfuel::draw::Image *activeimage,
                                         jetfuel::draw::Image *disabledimage){
        checkbox->Dynamic_load_check_box_images(*activeimage, *disabledimage);
    }

    bool Check_box_is_checked(jetfuel::gui::Check_box *checkbox){
        return checkbox->Is_checked();
    }

    int Check_box_get_position_x(jetfuel::gui::Check_box *checkbox){
        return checkbox->Get_position().x;
    }

    int Check_box_get_position_y(jetfuel::gui::Check_box *checkbox){
        return checkbox->Get_position().y;
    }

    void Check_box_set_position(jetfuel::gui::Check_box *checkbox, int x, int y){
        checkbox->Set_position(jetfuel::draw::Vector2d_int(x,y));
    }

    jetfuel::draw::Text_characteristics_replacement
     *Check_box_get_label_characteristics(jetfuel::gui::Check_box *checkbox){
        jetfuel::draw::Text_characteristics_replacement *returnvalue = new
                jetfuel::draw::Text_characteristics_replacement();

        *returnvalue = jetfuel::draw::Text_characteristics_replacement(
                                        checkbox->Get_label_characteristics());

        return returnvalue;
    }

    void Check_box_set_label_characteristics(jetfuel::gui::Check_box *checkbox,
                    jetfuel::draw::Text_characteristics_replacement *textchars,
                    bool left, unsigned int labelgap){
        jetfuel::gui::Check_box::Label_position labelpos;

        if(left){
            labelpos = jetfuel::gui::Check_box::Label_position::Left;
        }else{
            labelpos = jetfuel::gui::Check_box::Label_position::Right;
        }

        checkbox->Set_label_characteristics(
                textchars->Convert_to_text_chars_struct(), labelpos, labelgap);
    }

    void Check_box_set_UIS_action_to_watch(jetfuel::gui::Check_box *checkbox,
                                 wchar_t *action){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        checkbox->Set_UIS_action_to_watch(wcharconvertor.to_bytes(action));
    }

    int Check_box_get_checkbox_rect_to_draw_x(
                                       jetfuel::gui::Check_box *checkbox){
        return checkbox->Get_checkbox_rect_to_draw().x;
    }

    int Check_box_get_checkbox_rect_to_draw_y(
                                       jetfuel::gui::Check_box *checkbox){
        return checkbox->Get_checkbox_rect_to_draw().y;
    }

    int Check_box_get_checkbox_rect_to_draw_width(
                                       jetfuel::gui::Check_box *checkbox){
        return checkbox->Get_checkbox_rect_to_draw().width;
    }

    int Check_box_get_checkbox_rect_to_draw_height(
                                       jetfuel::gui::Check_box *checkbox){
        return checkbox->Get_checkbox_rect_to_draw().height;
    }
    
    jetfuel::gui::Drop_down_box *Drop_down_box_new(){
        return new jetfuel::gui::Drop_down_box();
    }
    
    void Drop_down_box_load_base_box_image(jetfuel::gui::
                                           Drop_down_box *dropdownbox, 
                                           jetfuel::draw::Image *baseboximage,
                                           jetfuel::draw::Color *color,
                                           unsigned int xbordersize, 
                                           unsigned int ybordersize){    
        dropdownbox->Dynamic_load_base_box_image(*baseboximage,*color,
                                                jetfuel::draw::Vector2d_uint(
                                                 xbordersize, ybordersize));
    }
    
    wchar_t *Drop_down_box_get_active_option(jetfuel::gui::Drop_down_box
                                             *dropdownbox){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wstringconverter;

        std::wstring activeoption = wstringconverter.from_bytes(
                                               dropdownbox->Get_active_option());

        wchar_t *returnvalue = new wchar_t[activeoption.length() + 1];

        wcscpy(returnvalue, activeoption.c_str());

        return returnvalue;
    }

    jetfuel::draw::Text_characteristics_replacement
     *Drop_down_box_get_option_text_characteristics(jetfuel::gui::Check_box
                                                    *dropdownbox){
        jetfuel::draw::Text_characteristics_replacement *returnvalue = new
                jetfuel::draw::Text_characteristics_replacement();

        *returnvalue = jetfuel::draw::Text_characteristics_replacement(
                                   dropdownbox->Get_label_characteristics());

        return returnvalue;
    }

    void Drop_down_box_set_option_text_characteristics(jetfuel::gui::
                                                        Drop_down_box
                                                        *dropdownbox,
      jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        dropdownbox->Set_option_text_characteristics(textcharsreplacement->
                                                Convert_to_text_chars_struct());
    }

    wchar_t *Drop_down_box_get_uis_action_to_listen_for(jetfuel::gui::
                                                        Drop_down_box
                                                        *dropdownbox){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wstringconverter;

        std::wstring uisaction = wstringconverter.from_bytes(dropdownbox->
                                   Get_UIS_action_to_listen_for());

        wchar_t *returnvalue = new wchar_t[uisaction.length() + 1];

        wcscpy(returnvalue, uisaction.c_str());

        return returnvalue;
    }

    void Drop_down_box_set_uis_action_to_listen_for(jetfuel::gui::Drop_down_box
                                                    *dropdownbox,
                                                    wchar_t *uisaction){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        dropdownbox->Set_UIS_action_to_listen_for(
                wcharconvertor.to_bytes(uisaction));
    }

    void Drop_down_box_add_option(jetfuel::gui::Drop_down_box *dropdownbox,
                                  wchar_t *option){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        dropdownbox->Add_option(wcharconvertor.to_bytes(option));
    }

    int Drop_down_box_get_position_x(jetfuel::gui::Drop_down_box *dropdownbox){
        return dropdownbox->Get_position().x;
    }

    int Drop_down_box_get_position_y(jetfuel::gui::Drop_down_box *dropdownbox){
        return dropdownbox->Get_position().y;
    }

    void Drop_down_box_set_position(jetfuel::gui::Drop_down_box *dropdownbox,
                                    int x, int y){
        dropdownbox->Set_position(jetfuel::draw::Vector2d_int(x,y));
    }

    int Drop_down_box_get_rect_to_draw_width(jetfuel::gui::Drop_down_box
                                             *dropdownbox){
        return dropdownbox->Get_rect_to_draw().width;
    }

    int Drop_down_box_get_rect_to_draw_height(jetfuel::gui::Drop_down_box
                                             *dropdownbox){
        return dropdownbox->Get_rect_to_draw().height;
    }

    jetfuel::gui::Button_characteristics_replacement *
     Button_characteristics_replacement_new(){
        return new jetfuel::gui::Button_characteristics_replacement();
    }

    void Button_characteristics_replacement_delete(jetfuel::gui::
                Button_characteristics_replacement *buttoncharsreplacement){
        delete buttoncharsreplacement;
    }

    jetfuel::draw::Image *Button_characteristics_replacement_get_image(
      jetfuel::gui::Button_characteristics_replacement *buttoncharsreplacement){
        jetfuel::draw::Image *returnvalue = new jetfuel::draw::Image();

        *returnvalue = buttoncharsreplacement->image;

        return returnvalue;
    }

    void Button_characteristics_replacement_set_image(jetfuel::gui::
            Button_characteristics_replacement *buttoncharsreplacement,
            jetfuel::draw::Image *image){
        buttoncharsreplacement->image = *image;
    }

    jetfuel::draw::Color *Button_characteristics_replacement_get_color(
      jetfuel::gui::Button_characteristics_replacement *buttoncharsreplacement){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        *returnvalue = buttoncharsreplacement->color;

        return returnvalue;
    }

    void Button_characteristics_replacement_set_color(jetfuel::gui::
            Button_characteristics_replacement *buttoncharsreplacement,
            jetfuel::draw::Color *color){
        buttoncharsreplacement->color = *color;
    }

    jetfuel::draw::Color *Button_characteristics_replacement_get_text_chars(
      jetfuel::gui::Button_characteristics_replacement *buttoncharsreplacement){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        *returnvalue = buttoncharsreplacement->color;

        return returnvalue;
    }

    void Button_characteristics_replacement_set_text_chars(jetfuel::gui::
            Button_characteristics_replacement *buttoncharsreplacement,
         jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        buttoncharsreplacement->textcharsreplacement = *textcharsreplacement;
    }

    jetfuel::gui::Menu *Menu_new(){
        return new jetfuel::gui::Menu();
    }

    jetfuel::gui::Menu *Menu_new_from_heights_and_gaps(unsigned int maxheight,
                                                       unsigned int columngap,
                                                       unsigned int buttongap){
        return new jetfuel::gui::Menu(maxheight, columngap, buttongap);
    }

    unsigned int Menu_get_max_height(jetfuel::gui::Menu *menu){
        return menu->Get_max_height();
    }

    void Menu_set_max_height(jetfuel::gui::Menu *menu, unsigned int maxheight){
        menu->Set_max_height(maxheight);
    }

    unsigned int Menu_get_column_gap(jetfuel::gui::Menu *menu){
        return menu->Get_column_gap();
    }

    void Menu_set_column_gap(jetfuel::gui::Menu *menu, unsigned int columngap){
        menu->Set_column_gap(columngap);
    }

    unsigned int Menu_get_button_gap(jetfuel::gui::Menu *menu){
        return menu->Get_button_gap();
    }

    void Menu_set_button_gap(jetfuel::gui::Menu *menu, unsigned int buttongap){
        menu->Set_button_gap(buttongap);
    }

    jetfuel::draw::Image *Menu_get_container_box_image(jetfuel::gui::Menu *menu){
        jetfuel::draw::Image *returnvalue = new jetfuel::draw::Image();

        *returnvalue = menu->Get_container_box_image();

        return returnvalue;
    }

    void Menu_set_container_box_image(jetfuel::gui::Menu *menu,
                                      jetfuel::draw::Image *image,
                                      unsigned int borderwidth,
                                      unsigned int borderheight){
        menu->Set_container_box_image(*image, jetfuel::draw::Vector2d_uint(
                                      borderwidth, borderheight));
    }

    unsigned int Menu_get_container_box_border_width(jetfuel::gui::Menu *menu){
        return menu->Get_container_box_border().x;
    }

    unsigned int Menu_get_container_box_border_height(jetfuel::gui::Menu *menu){
        return menu->Get_container_box_border().y;
    }

    bool Menu_add_button(jetfuel::gui::Menu *menu,
                    jetfuel::gui::Button_characteristics_replacement
                    *buttoncharsreplacement, wchar_t *uisactiontowatchfor,
                    wchar_t *messagetosenduponclick,
                    jetfuel::core::Message_bus *messagebus){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        return menu->Add_button(buttoncharsreplacement->
                         Convert_to_button_chars_struct(),
                         wcharconvertor.to_bytes(uisactiontowatchfor),
                         wcharconvertor.to_bytes(messagetosenduponclick),
                         messagebus, true);
    }

    int Menu_get_position_x(jetfuel::gui::Menu *menu){
        return menu->Get_position().x;
    }

    int Menu_get_position_y(jetfuel::gui::Menu *menu){
        return menu->Get_position().y;
    }

    void Menu_set_position(jetfuel::gui::Menu *menu, int x, int y){
        menu->Set_position(jetfuel::draw::Vector2d_int(x,y));
    }

    int Menu_get_rect_to_draw_height(jetfuel::gui::Menu *menu){
        return menu->Get_rect_to_draw().height;
    }

    int Menu_get_rect_to_draw_width(jetfuel::gui::Menu *menu){
        return menu->Get_rect_to_draw().width;
    }

    jetfuel::gui::Progress_bar *Progress_bar_new(){
        return new jetfuel::gui::Progress_bar();
    }

    jetfuel::gui::Progress_bar *Progress_bar_new_set_progress_bar(jetfuel::draw::
                                                        Image *progressbarimage,
                                 jetfuel::draw::Color *progressbarcolor,
                                          int progressbarholderborderx,
                                          int progressbarholderbordery,
                                          int progressbarholderborderwidth,
                                          int progressbarholderborderheight,
                                          int progressbarmax){
        return new jetfuel::gui::Progress_bar(*progressbarimage,
                    *progressbarcolor,
                    jetfuel::draw::Rect2d_int(progressbarholderborderx,
                        progressbarholderbordery, progressbarholderborderwidth,
                        progressbarholderborderheight),progressbarmax);
    }

    unsigned int Progress_bar_get_progress_bar_progress(jetfuel::gui::
                                              Progress_bar *progressbar){
        return progressbar->Get_progress_bar_progress();
    }

    void Progress_bar_set_progress_bar_progress(jetfuel::gui::Progress_bar
                                               *progressbar,
                                               unsigned int progressbarprogress){
        progressbar->Set_progress_bar_progress(progressbarprogress);
    }

    unsigned int Progress_bar_get_max_progress_bar(jetfuel::gui::Progress_bar *progressbar){
        return progressbar->Get_max_progress_bar();
    }

    bool Progress_bar_has_progress_bar_completed(jetfuel::gui::Progress_bar *progressbar){
        return progressbar->Has_progress_bar_completed();
    }

    int Progress_bar_get_position_x(jetfuel::gui::Progress_bar *progressbar){
        return progressbar->Get_position().x;
    }

    int Progress_bar_get_position_y(jetfuel::gui::Progress_bar *progressbar){
        return progressbar->Get_position().y;
    }

    void Progress_bar_set_position(jetfuel::gui::Progress_bar *progressbar,
                                   int x, int y){
        progressbar->Set_position(jetfuel::draw::Vector2d_int(x,y));
    }

    int Progress_bar_get_rect_to_draw_width(jetfuel::gui::Progress_bar
                                            *progressbar){
        return progressbar->Get_rect_to_draw().width;
    }

    int Progress_bar_get_rect_to_draw_height(jetfuel::gui::Progress_bar
                                            *progressbar){
        return progressbar->Get_rect_to_draw().height;
    }

    jetfuel::gui::Slider *Slider_new(){
        return new jetfuel::gui::Slider();
    }

    jetfuel::draw::
    Rectangle_2d_shape_characteristics_replacement
    Slider_get_slider_rail_characteristics(jetfuel::gui::Slider *slider){
        return jetfuel::draw::Rectangle_2d_shape_characteristics_replacement(
                                    slider->Get_slider_rail_characteristics());
    }

    void Slider_set_slider_rail_characteristics(jetfuel::gui::Slider *slider,
    jetfuel::draw::Rectangle_2d_shape_characteristics_replacement *rect2dchars){
        slider->Set_slider_rail_characteristics(rect2dchars->
                        Convert_to_rectangle_2d_shape_chars());
    }

    jetfuel::draw::
    Circle_2d_shape_characteristics_replacement
    Slider_get_slider_button_characteristics(jetfuel::gui::Slider *slider){
        return jetfuel::draw::Circle_2d_shape_characteristics_replacement(
                                    slider->Get_slider_button_characteristics());
    }

    void Slider_set_slider_button_characteristics(jetfuel::gui::Slider *slider,
                                                                jetfuel::draw::
                    Circle_2d_shape_characteristics_replacement *circle2dchars){
        slider->Set_slider_button_characteristics(circle2dchars->
                            Convert_to_circle_2d_shape_chars());
    }

    unsigned int Slider_get_number_of_statuses(jetfuel::gui::Slider *slider){
        return slider->Get_number_of_statuses();
    }

    void Slider_set_number_of_statuses(jetfuel::gui::Slider *slider,
                                        unsigned int statusnumber){
        slider->Set_number_of_statuses(statusnumber);
    }

    unsigned int Slider_get_current_status(jetfuel::gui::Slider *slider){
        return slider->Get_current_status();
    }

    void Slider_set_current_status(jetfuel::gui::Slider *slider,
                                   unsigned int currentstatus){
        slider->Set_current_status(currentstatus);
    }

    jetfuel::control::Uis_input_actions_replacement *Slider_get_control_scheme(
                                                jetfuel::gui::Slider *slider){
        jetfuel::control::Uis_input_actions_replacement *returnvalue = new
                        jetfuel::control::Uis_input_actions_replacement(
                                        slider->Get_control_scheme());

        return returnvalue;
    }

    void Slider_set_control_scheme(jetfuel::gui::Slider *slider,
            jetfuel::control::Uis_input_actions_replacement *uisinputactions){
        slider->Set_control_scheme(
        uisinputactions->Convert_to_uis_input_actions());
    }

    int Slider_get_position_x(jetfuel::gui::Slider *slider){
        return slider->Get_position().x;
    }

    int Slider_get_position_y(jetfuel::gui::Slider *slider){
        return slider->Get_position().y;
    }

    void Slider_set_position(jetfuel::gui::Slider *slider, int x, int y){
        slider->Set_position(jetfuel::draw::Vector2d_int(x,y));
    }

    int Slider_get_rect_to_draw_width(jetfuel::gui::Slider *slider){
        return slider->Get_rect_to_draw().width;
    }

    int Slider_get_rect_to_draw_height(jetfuel::gui::Slider *slider){
        return slider->Get_rect_to_draw().height;
    }
}
