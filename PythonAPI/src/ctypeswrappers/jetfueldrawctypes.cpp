/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <cstring>

#include <jetfueldraw.h>

#include "../jetfueldraw/textcharacteristicsreplacement.h"
#include "../jetfueldraw/rectangle2dshapecharacteristicsreplacement.h"
#include "../jetfueldraw/circle2dshapecharacteristicsreplacement.h"

extern "C"{
    // jetfuel::draw::Image wrappers

    jetfuel::draw::Image *Image_new(){
        return new jetfuel::draw::Image();
    }

    void Image_delete(jetfuel::draw::Image *image){
        delete image;
    }

    int Image_get_image_size_x(jetfuel::draw::Image *image){
        return image->Get_size_of_image().x;
    }

    int Image_get_image_size_y(jetfuel::draw::Image *image){
        return image->Get_size_of_image().y;
    }

    wchar_t *Image_get_image_location(jetfuel::draw::Image *image){
        char *getlocationcstring = new char[image->Get_image_location().size()
                                            + 1];

        strcpy(getlocationcstring, image->Get_image_location().c_str());

        const size_t wcharsize = strlen(getlocationcstring)+1;

        wchar_t *returnvalue = new wchar_t[wcharsize];

        mbstowcs(returnvalue,getlocationcstring,
                 wcharsize);

        delete getlocationcstring;

        return returnvalue;
    }

    void Image_set_image_location(jetfuel::draw::Image *image,
                                  jetfuel::draw::Scene_manager *scenemanager,
                                  wchar_t *imagelocation){
        std::wstring imagelocationwstring(imagelocation);

        std::string imagelocationstring(imagelocationwstring.begin(),
                                        imagelocationwstring.end());

        image->Set_image(imagelocationstring, scenemanager);
    }

    void Image_set_image_location_and_size(jetfuel::draw::Image *image,
                                  wchar_t *imagelocation,
                                  int imagesizex,
                                  int imagesizey){
        const size_t charsize = wcslen(imagelocation)+1;

        char *imagelocationcstring = new char(charsize)+1;

        wcstombs(imagelocationcstring, imagelocation,
                 charsize);


        image->Set_image(imagelocationcstring, jetfuel::draw::Vector2d_int(
                                               imagesizex,imagesizey));
    }

    // jetfuel::draw::Scene_manager/jetfuel::draw::Scene wrappers

    void Scene_attach_drawable(jetfuel::draw::Scene_manager *scenemanager,
                               jetfuel::draw::Drawable *drawable,
                               unsigned int drawableweight){
        scenemanager->Get_current_scene()->
                Attach_drawable(drawable, drawableweight);
    }

    void Scene_disable_drawable(jetfuel::draw::Scene_manager *scenemanager,
                                jetfuel::draw::Drawable *drawable){
        scenemanager->Get_current_scene()->Disable_drawable(drawable);
    }

    // jetfuel::draw::Drawable wrappers

    void Drawable_delete(jetfuel::draw::Drawable *drawable){
        delete drawable;
    }

    int Drawable_get_position_x(jetfuel::draw::Drawable *drawable){
        return drawable->Get_position().x;
    }

    int Drawable_get_position_y(jetfuel::draw::Drawable *drawable){
        return drawable->Get_position().y;
    }

    void Drawable_set_position(jetfuel::draw::Drawable *drawable, int x, int y){
        drawable->Set_position(jetfuel::draw::Vector2d_int(x, y));
    }

    bool Drawable_draw(jetfuel::draw::Drawable *drawable){
        return drawable->Draw();
    }

    // jetfuel::draw::Rectangle_interface wrappers

    int Rectangle_interface_get_size_width(jetfuel::draw::Rectangle_interface
                                           *rectangle){
        return rectangle->Get_rect_to_draw().width;
    }

    int Rectangle_interface_get_size_height(jetfuel::draw::Rectangle_interface
                                            *rectangle){
        return rectangle->Get_rect_to_draw().height;
    }

    // jetfuel::draw::Sprite wrappers

    jetfuel::draw::Sprite *Sprite_new(){
        return new jetfuel::draw::Sprite();
    }

    void Sprite_delete(jetfuel::draw::Sprite *sprite){
        delete sprite;
    }

    void Sprite_dynamic_load_from_image(jetfuel::draw::Sprite *sprite,
                                jetfuel::draw::Image *image){
        sprite->Dynamic_load_from_image(*image);
    }

    // jetfuel::draw::Color wrappers

    jetfuel::draw::Color *Color_new(){
        return new jetfuel::draw::Color();
    }

    jetfuel::draw::Color *Color_new_from_rgba(unsigned int red,
               unsigned int green, unsigned int blue, unsigned int alpha){
        return new jetfuel::draw::Color(red, green, blue, alpha);
    }

    void Color_delete(jetfuel::draw::Color *color){
        delete color;
    }

    int Color_get_red(jetfuel::draw::Color *color){
        return color->r;
    }

    void Color_set_red(jetfuel::draw::Color *color,
                       unsigned int red){
        color->r = red;
    }

    int Color_get_green(jetfuel::draw::Color *color){
        return color->g;
    }

    void Color_set_green(jetfuel::draw::Color *color,
                       unsigned int green){
        color->g = green;
    }

    int Color_get_blue(jetfuel::draw::Color *color){
        return color->b;
    }

    void Color_set_blue(jetfuel::draw::Color *color,
                       unsigned int blue){
        color->b = blue;
    }

    int Color_get_alpha(jetfuel::draw::Color *color){
        return color->a;
    }

    void Color_set_alpha(jetfuel::draw::Color *color,
                         unsigned int alpha){
        color->a = alpha;
    }

    // jetfuel::draw::Rectangle_2d_shape_characteristics replacement wrappers

    jetfuel::draw::
    Rectangle_2d_shape_characteristics_replacement
     *Rectangle_2d_shape_characteristics_replacement_new(){
        return new
         jetfuel::draw::Rectangle_2d_shape_characteristics_replacement();
    }

    int Rectangle_2d_shape_characteristics_replacement_get_position_x(
          jetfuel::draw::Rectangle_2d_shape_characteristics_replacement
          *rect2dshapechars){
        return rect2dshapechars->rectpositionx;
    }

    int Rectangle_2d_shape_characteristics_replacement_get_position_y(
          jetfuel::draw::Rectangle_2d_shape_characteristics_replacement
          *rect2dshapechars){
        return rect2dshapechars->rectpositiony;
    }

    void Rectangle_2d_shape_characteristics_replacement_set_position(
          jetfuel::draw::Rectangle_2d_shape_characteristics_replacement
          *rect2dshapechars, int x, int y){
        rect2dshapechars->rectpositionx = x;
        rect2dshapechars->rectpositiony = y;
    }

    int Rectangle_2d_shape_characteristics_replacement_get_size_width(
          jetfuel::draw::Rectangle_2d_shape_characteristics_replacement
          *rect2dshapechars){
        return rect2dshapechars->rectsizewidth;
    }

    int Rectangle_2d_shape_characteristics_replacement_get_size_height(
          jetfuel::draw::Rectangle_2d_shape_characteristics_replacement
          *rect2dshapechars){
        return rect2dshapechars->rectsizeheight;
    }

    void Rectangle_2d_shape_characteristics_replacement_set_size(
          jetfuel::draw::Rectangle_2d_shape_characteristics_replacement
          *rect2dshapechars, int width, int height){
        rect2dshapechars->rectsizewidth = width;
        rect2dshapechars->rectsizeheight = height;
    }

    jetfuel::draw::Color
    *Rectangle_2d_shape_characteristics_replacement_get_fill_color(
            jetfuel::draw::Rectangle_2d_shape_characteristics_replacement
            *rect2dshapechars){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        *returnvalue = rect2dshapechars->fillcolor;

        return returnvalue;
    }

    void Rectangle_2d_shape_characteristics_replacement_set_fill_color(
            jetfuel::draw::Rectangle_2d_shape_characteristics_replacement
            *rect2dshapechars, jetfuel::draw::Color *fillcolor){
        rect2dshapechars->fillcolor = *fillcolor;
    }

    jetfuel::draw::Color
    *Rectangle_2d_shape_characteristics_replacement_get_outline_color(
            jetfuel::draw::Rectangle_2d_shape_characteristics_replacement
            *rect2dshapechars){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        *returnvalue = rect2dshapechars->outlinecolor;

        return returnvalue;
    }

    void Rectangle_2d_shape_characteristics_replacement_set_outline_color(
            jetfuel::draw::Rectangle_2d_shape_characteristics_replacement
            *rect2dshapechars, jetfuel::draw::Color *outlinecolor){
        rect2dshapechars->outlinecolor = *outlinecolor;
    }

    // jetfuel::draw::Rectangle_2d_shape wrappers

    jetfuel::draw::Rectangle_2d_shape *Rectangle_2d_shape_new(){
        return new jetfuel::draw::Rectangle_2d_shape();
    }

    jetfuel::draw::Rectangle_2d_shape *Rectangle_2d_shape_new_from_rect(int x,
                                                             int y,
                                                             int width,
                                                             int height){
        return new jetfuel::draw::Rectangle_2d_shape(jetfuel::draw::Rect2d_int(x,
                                                     y, width, height));
    }

    void Rectangle_2d_shape_delete(jetfuel::draw::Rectangle_2d_shape *rectangle){
        delete rectangle;
    }

    jetfuel::draw::Color *Rectangle_2d_shape_get_fill_color(
                           jetfuel::draw::Rectangle_2d_shape *rectangle){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        returnvalue->r = rectangle->Get_fill_color().r;

        returnvalue->g = rectangle->Get_fill_color().g;

        returnvalue->b = rectangle->Get_fill_color().b;

        returnvalue->a = rectangle->Get_fill_color().a;

        return returnvalue;
    }

    void Rectangle_2d_shape_set_fill_color(jetfuel::draw::Rectangle_2d_shape
                                           *rectangle, jetfuel::draw::Color
                                           *fillcolor){
        rectangle->Set_fill_color(*fillcolor);
    }

    jetfuel::draw::Color *Rectangle_2d_shape_get_outline_color(
                           jetfuel::draw::Rectangle_2d_shape *rectangle){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        returnvalue->r = rectangle->Get_outline_color().r;

        returnvalue->g = rectangle->Get_outline_color().g;

        returnvalue->b = rectangle->Get_outline_color().b;

        returnvalue->a = rectangle->Get_outline_color().a;

        return returnvalue;
    }

    void Rectangle_2d_shape_set_outline_color(jetfuel::draw::Rectangle_2d_shape
                                           *rectangle, jetfuel::draw::Color
                                           *outlinecolor){
        rectangle->Set_outline_color(*outlinecolor);
    }

    void Rectangle_2d_shape_disable_drawing_outline_color(jetfuel::draw::Rectangle_2d_shape
                                                          *rectangle){
        rectangle->Disable_drawing_outline_color();
    }

    int Rectangle_2d_shape_get_position_x(jetfuel::draw::Rectangle_2d_shape
                                          *rectangle){
        return rectangle->Get_position().x;
    }

    int Rectangle_2d_shape_get_position_y(jetfuel::draw::Rectangle_2d_shape
                                          *rectangle){
        return rectangle->Get_position().y;
    }

    void Rectangle_2d_shape_set_position(jetfuel::draw::Rectangle_2d_shape
                                         *rectangle, int x, int y){
        rectangle->Set_position(jetfuel::draw::Vector2d_int(x,y));
    }

    int Rectangle_2d_shape_get_size_width(jetfuel::draw::Rectangle_2d_shape
                                          *rectangle){
        return rectangle->Get_size().x;
    }

    int Rectangle_2d_shape_get_size_height(jetfuel::draw::Rectangle_2d_shape
                                          *rectangle){
        return rectangle->Get_size().y;
    }

    void Rectangle_2d_shape_set_size(jetfuel::draw::Rectangle_2d_shape
                                         *rectangle, int width, int height){
        rectangle->Set_size(jetfuel::draw::Vector2d_int(width, height));
    }

    // jetfuel::draw::Circle_interface wrappers

    int Circle_interface_get_radius(jetfuel::draw::Circle_interface
                                     *circleinterface){
        return circleinterface->Get_circle_to_draw().radius;
    }

    // jetfuel::draw::Circle_2d_shape_characteristics_replacement wrappers

    jetfuel::draw::Circle_2d_shape_characteristics_replacement
     *Circle_2d_shape_characteristics_replacement_new(){
        return new jetfuel::draw::Circle_2d_shape_characteristics_replacement();
    }

    int Circle_2d_shape_characteristics_replacement_get_position_x(jetfuel::
            draw::Circle_2d_shape_characteristics_replacement *circlechars){
        return circlechars->positionx;
    }

    int Circle_2d_shape_characteristics_replacement_get_position_y(jetfuel::
            draw::Circle_2d_shape_characteristics_replacement *circlechars){
        return circlechars->positiony;
    }

    void Circle_2d_shape_characteristics_replacement_set_position(jetfuel::draw::
            Circle_2d_shape_characteristics_replacement *circlechars, int x,
            int y){
        circlechars->positionx = x;
        circlechars->positiony = y;
    }

    int Circle_2d_shape_characteristics_replacement_get_radius(jetfuel::draw::
            Circle_2d_shape_characteristics_replacement *circlechars){
        return circlechars->radius;
    }

    void Circle_2d_shape_characteristics_replacement_set_radius(jetfuel::draw::
            Circle_2d_shape_characteristics_replacement *circlechars,
            int radius){
        circlechars->radius = radius;
    }

    bool Circle_2d_shape_characteristics_replacement_get_aa_status(jetfuel::
            draw::Circle_2d_shape_characteristics_replacement *circlechars){
        return circlechars->antialiasingstatus;
    }

    void Circle_2d_shape_characteristics_replacement_set_aa_status(jetfuel::
            draw::Circle_2d_shape_characteristics_replacement *circlechars,
            bool aastatus){
        circlechars->antialiasingstatus = aastatus;
    }

    bool Circle_2d_shape_characteristics_replacement_get_filled_circle_status(
            jetfuel::draw::
            Circle_2d_shape_characteristics_replacement *circlechars){
        return circlechars->filledcirclestatus;
    }

    void Circle_2d_shape_characteristics_replacement_set_filled_circle_status(
            jetfuel::draw::
            Circle_2d_shape_characteristics_replacement *circlechars,
            bool filledcirclestatus){
        circlechars->filledcirclestatus = filledcirclestatus;
    }

    jetfuel::draw::Color *Circle_2d_shape_characteristics_replacement_get_color(
        jetfuel::draw::Circle_2d_shape_characteristics_replacement *circlechars){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        *returnvalue = circlechars->color;

        return returnvalue;
    }

    void Circle_2d_shape_characteristics_replacement_set_color(jetfuel::draw::
            Circle_2d_shape_characteristics_replacement *circlechars, jetfuel::
            draw::Color *color){
        circlechars->color = *color;
    }

    // jetfuel::draw::Circle_2d_shape wrappers

    jetfuel::draw::Circle_2d_shape *Circle_2d_shape_new(){
        return new jetfuel::draw::Circle_2d_shape();
    }

    jetfuel::draw::Circle_2d_shape *Circle_2d_shape_new_from_circle(int x, int y,
                                                                    int radius){
        return new jetfuel::draw::Circle_2d_shape(jetfuel::draw::Circle2d_int(x,
                                                     y, radius));
    }

    void Circle_2d_shape_delete(jetfuel::draw::Circle_2d_shape *circle){
        delete circle;
    }

    int Circle_2d_shape_get_position_x(jetfuel::draw::Circle_2d_shape *circle) {
        return circle->Get_position().x;
    }

    int Circle_2d_shape_get_position_y(jetfuel::draw::Circle_2d_shape *circle) {
        return circle->Get_position().y;
    }

    void Circle_2d_shape_set_radius(jetfuel::draw::Circle_2d_shape *circle,
                                    int radius){
        circle->Set_radius(radius);
    }

    bool Circle_2d_shape_get_anti_aliasing_status(jetfuel::draw::Circle_2d_shape
                                                  *circle){
        return circle->Get_anti_aliasing_status();
    }

    void Circle_2d_shape_set_anti_aliasing_status(jetfuel::draw::Circle_2d_shape
                                                  *circle, bool aastatus){
        circle->Set_anti_aliasing_status(aastatus);
    }

    bool Circle_2d_shape_get_filled_circle_status(jetfuel::draw::Circle_2d_shape
                                                 *circle){
        return circle->Get_filled_circle_status();
    }

    void Circle_2d_shape_set_filled_circle_status(jetfuel::draw::Circle_2d_shape
                                                  *circle, bool filledincircle){
        circle->Set_filled_circle_status(filledincircle);
    }

    jetfuel::draw::Color *Circle_2d_shape_get_color(
                           jetfuel::draw::Circle_2d_shape *circle){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        returnvalue->r = circle->Get_color().r;

        returnvalue->g = circle->Get_color().g;

        returnvalue->b = circle->Get_color().b;

        returnvalue->a = circle->Get_color().a;

        return returnvalue;
    }

    void Circle_2d_shape_set_color(jetfuel::draw::Circle_2d_shape
                                   *circle, jetfuel::draw::Color
                                   *color){
        circle->Set_color(*color);
    }

    // jetfuel::draw::Font wrappers

    jetfuel::draw::Font *Font_new(){
        return new jetfuel::draw::Font();
    }

    jetfuel::draw::Font *Font_new_from_file_name(wchar_t *fontfilename){
        std::wstring fontfilenamewstring(fontfilename);

        std::string fontfilenamestring(fontfilenamewstring.begin(),
                                       fontfilenamewstring.end());

        return new jetfuel::draw::Font(fontfilenamestring);
    }

    jetfuel::draw::Font *Font_new_from_face_index(wchar_t *fontfilename,
                                                 long faceindex){
        const size_t charsize = wcslen(fontfilename)+1;

        char *fontfilenamecstring = new char(charsize)+1;

        wcstombs(fontfilenamecstring, fontfilename,
                 charsize);

        return new jetfuel::draw::Font(fontfilenamecstring, faceindex);
    }

    void Font_delete(jetfuel::draw::Font *font){
        delete font;
    }

    bool Font_is_font_loaded(jetfuel::draw::Font *font){
        return font->Is_font_loaded();
    }

    wchar_t *Font_get_file_name(jetfuel::draw::Font *font){
        char *fontfilename = new char[font->Get_file_name().size()
                                            + 1];

        strcpy(fontfilename, font->Get_file_name().c_str());

        const size_t wcharsize = strlen(fontfilename)+1;

        wchar_t *returnvalue = new wchar_t[wcharsize];

        mbstowcs(returnvalue, fontfilename,
                 wcharsize);

        delete fontfilename;

        return returnvalue;
    }

    long Font_get_face_index(jetfuel::draw::Font *font){
        return font->Get_face_index();
    }

    void Font_load_font(jetfuel::draw::Font *font, wchar_t *fontfilename){
        const size_t charsize = wcslen(fontfilename)+1;

        char *fontfilenamecstring = new char(charsize)+1;

        wcstombs(fontfilenamecstring, fontfilename,
                 charsize);

        font->Load_font(fontfilenamecstring);
    }

    void Font_load_font_face_index(jetfuel::draw::Font *font,
                      wchar_t *fontfilename, long faceindex){
        const size_t charsize = wcslen(fontfilename)+1;

        char *fontfilenamecstring = new char(charsize)+1;

        wcstombs(fontfilenamecstring, fontfilename,
                 charsize);

        font->Load_font(fontfilenamecstring, faceindex);
    }

    // jetfuel::draw::Text_characteristics_replacement wrappers

    jetfuel::draw::Text_characteristics_replacement *Text_characteristics_new(){
       jetfuel::draw::Text_characteristics_replacement *returnvalue =
               new jetfuel::draw::Text_characteristics_replacement();

       returnvalue->previousfont = returnvalue->font;

       return returnvalue;
    }

    jetfuel::draw::Text_characteristics_replacement
     *Text_characteristics_new_from_font(jetfuel::draw::Font *font){
        jetfuel::draw::Text_characteristics_replacement *returnvalue =
                new jetfuel::draw::Text_characteristics_replacement(font);

        returnvalue->previousfont = returnvalue->font;

        return returnvalue;
    }

    void Text_characteristics_delete(jetfuel::draw::Text_characteristics_replacement
                                     *textcharsreplacement){
        delete textcharsreplacement;
    }

    int Text_characteristics_get_render_mode(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        return textcharsreplacement->rendermode;
    }

    void Text_characteristics_set_render_mode(
           jetfuel::draw::Text_characteristics_replacement *textcharsreplacement,
           int textrendermode){
        textcharsreplacement->rendermode = textrendermode;
    }

    wchar_t *Text_characteristics_get_text_string(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        char *textstringname = new char[textcharsreplacement->textstring.size()
                                        + 1];

        strcpy(textstringname, textcharsreplacement->textstring.c_str());

        const size_t wcharsize = strlen(textstringname)+1;

        wchar_t *returnvalue = new wchar_t[wcharsize];

        mbstowcs(returnvalue, textstringname,
                 wcharsize);

        return returnvalue;
    }

    void Text_characteristics_set_text_string(
           jetfuel::draw::Text_characteristics_replacement *textcharsreplacement,
           wchar_t *textstring){
        const size_t charsize = wcslen(textstring)+1;

        char *textstringcstring = new char(charsize)+1;

        wcstombs(textstringcstring, textstring,
                 charsize);

        textcharsreplacement->textstring = textstringcstring;
    }

    jetfuel::draw::Color *Text_characteristics_get_text_color(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        *returnvalue = textcharsreplacement->textcolor;

        return returnvalue;
    }

    void Text_characteristics_set_text_color(
        jetfuel::draw::Text_characteristics_replacement *textcharsreplacement,
        jetfuel::draw::Color *color){
        textcharsreplacement->textcolor = *color;
    }

    jetfuel::draw::Color *Text_characteristics_get_background_color(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        *returnvalue = textcharsreplacement->backgroundcolor;

        return returnvalue;
    }

    void Text_characteristics_set_background_color(
        jetfuel::draw::Text_characteristics_replacement *textcharsreplacement,
        jetfuel::draw::Color *color){
        textcharsreplacement->backgroundcolor = *color;
    }

    int Text_characteristics_get_font_outline_width(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        return textcharsreplacement->fontoutlinewidth;
    }

    void Text_characteristics_set_font_outline_width(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement,
          unsigned int fontoutlinewidth){
        textcharsreplacement->fontoutlinewidth = fontoutlinewidth;
    }

    bool Text_characteristics_get_font_kerning_status(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        return textcharsreplacement->kerningstatus;
    }

    void Text_characteristics_set_font_kerning_status(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement,
          bool kerningstatus){
        textcharsreplacement->kerningstatus = kerningstatus;
    }

    int Text_characteristics_get_font_style(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        return textcharsreplacement->fontstyle;
    }

    void Text_characteristics_set_font_style(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement,
          int fontstyle){
        textcharsreplacement->fontstyle = fontstyle;
    }

    int Text_characteristics_get_font_size(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        return textcharsreplacement->fontsize;
    }

    void Text_characteristics_set_font_size(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement,
          int fontsize){
        textcharsreplacement->fontsize = fontsize;
    }

    int Text_characteristics_get_font_hinting(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        return textcharsreplacement->fonthinting;
    }

    void Text_characteristics_set_font_hinting(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement,
          int fonthinting){
        textcharsreplacement->fonthinting = fonthinting;
    }

    jetfuel::draw::Font *Text_characteristics_get_font(
          jetfuel::draw::Text_characteristics_replacement *textcharsreplacement){
        jetfuel::draw::Font *font = new jetfuel::draw::Font();

        textcharsreplacement->font = textcharsreplacement->previousfont;

        *font = textcharsreplacement->font;

        return font;
    }

    // jetfuel::draw::Text wrappers

    jetfuel::draw::Text *Text_new(){
        return new jetfuel::draw::Text();
    }

    jetfuel::draw::Text *Text_new_from_font(jetfuel::draw::Font *font){
        return new jetfuel::draw::Text(*font);
    }

    int Text_get_text_render_mode(jetfuel::draw::Text *text){
        if(text->Get_render_mode() == jetfuel::draw::Text::Render_mode::Solid){
            return 1;
        }else if(text->Get_render_mode() ==
                 jetfuel::draw::Text::Render_mode::Shaded){
            return 2;
        }else{
            return 3;
        }
    }

    void Text_set_text_render_mode(jetfuel::draw::Text *text, int rendermode){
        if(rendermode == 1){
            text->Set_render_mode(jetfuel::draw::Text::Render_mode::Solid);
        }else if(rendermode == 2){
            text->Set_render_mode(jetfuel::draw::Text::Render_mode::Shaded);
        }else{
            text->Set_render_mode(jetfuel::draw::Text::Render_mode::Blended);
        }
    }

    wchar_t *Text_get_string(jetfuel::draw::Text *text){
        char *textstringname = new char[text->Get_string().size()
                                        + 1];

        strcpy(textstringname, text->Get_string().c_str());

        const size_t wcharsize = strlen(textstringname)+1;

        wchar_t *returnvalue = new wchar_t[wcharsize];

        mbstowcs(returnvalue, textstringname,
                 wcharsize);

        return returnvalue;
    }

    void Text_set_string(jetfuel::draw::Text *text, wchar_t *textstring){
        const size_t charsize = wcslen(textstring)+1;

        char *textstringcstring = new char(charsize)+1;

        wcstombs(textstringcstring, textstring,
                 charsize);

        text->Set_string(textstringcstring);
    }

    jetfuel::draw::Color *Text_get_text_color(
                           jetfuel::draw::Text *text){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        *returnvalue = text->Get_text_color();

        return returnvalue;
    }

    void Text_set_text_color(jetfuel::draw::Text *text,
                             jetfuel::draw::Color *color){
        text->Set_text_color(*color);
    }

    jetfuel::draw::Color *Text_get_background_color(
                           jetfuel::draw::Text *text){
        jetfuel::draw::Color *returnvalue = new jetfuel::draw::Color();

        *returnvalue = text->Get_background_color();

        return returnvalue;
    }

    void Text_set_background_color(jetfuel::draw::Text *text,
                             jetfuel::draw::Color *color){
        text->Set_background_color(*color);
    }

    int Text_get_font_outline_width(jetfuel::draw::Text *text){
        return text->Get_font_outline_width();
    }

    void Text_set_font_outline_width(jetfuel::draw::Text *text, int fontoutline){
       text->Set_font_outline_width(fontoutline);
    }

    bool Text_get_kerning_status(jetfuel::draw::Text *text){
        return text->Get_kerning_status();
    }

    void Text_set_kerning_status(jetfuel::draw::Text *text, bool kerningon){
        return text->Set_kerning_status(kerningon);
    }

    int Text_get_font_style(jetfuel::draw::Text *text){
        return text->Get_font_style();
    }

    void Text_set_font_style(jetfuel::draw::Text *text, int fontstyle){
        text->Set_font_style(fontstyle);
    }

    int Text_get_font_size(jetfuel::draw::Text *text){
        return text->Get_font_size();
    }

    void Text_set_font_size(jetfuel::draw::Text *text, int fontsize){
        text->Set_font_size(fontsize);
    }

    int Text_get_font_hinting(jetfuel::draw::Text *text){
        if(text->Get_font_hinting() == jetfuel::draw::Text::Font_hinting::None){
            return 1;
        }else if(text->Get_font_hinting() ==
                 jetfuel::draw::Text::Font_hinting::Light){
            return 2;
        }else if(text->Get_font_hinting() ==
                jetfuel::draw::Text::Font_hinting::Normal){
            return 3;
        }else{
            return 4;
        }
    }

    void Text_set_text_font_hinting(jetfuel::draw::Text *text, int fonthinting){
        if(fonthinting == 1){
            text->Set_font_hinting(jetfuel::draw::Text::Font_hinting::None);
        }else if(fonthinting == 2){
            text->Set_font_hinting(jetfuel::draw::Text::Font_hinting::Light);
        }else if(fonthinting == 3){
            text->Set_font_hinting(jetfuel::draw::Text::Font_hinting::Normal);
        }else{
            text->Set_font_hinting(jetfuel::draw::Text::Font_hinting::Mono);
        }
    }

    void Text_set_font(jetfuel::draw::Text *text,
                       jetfuel::draw::Font *font){
        text->Set_font(*font);
    }
}
