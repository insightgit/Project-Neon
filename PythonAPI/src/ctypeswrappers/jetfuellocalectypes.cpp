/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <locale>
#include <codecvt>

#include <jetfuellocale.h>

extern "C"{

    // jetfuel::locale::Locale_string wrappers

    jetfuel::locale::Locale_string *Locale_string_new(){
        return new jetfuel::locale::Locale_string();
    }

    wchar_t *Locale_string_get_locale_string_id(jetfuel::locale::Locale_string
                                                *localestring){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        std::wstring wstringid = wcharconvertor.from_bytes(
                                        localestring->stringid);

        wchar_t *returnvalue = new wchar_t[wstringid.length() + 1];

        wcscpy(returnvalue, wstringid.c_str());

        return returnvalue;
    }

    void Locale_string_set_locale_string_id(jetfuel::locale::Locale_string
                                            *localestring, wchar_t *stringid){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        localestring->stringid = wcharconvertor.to_bytes(stringid);
    }

    wchar_t *Locale_string_get_locale_string(jetfuel::locale::Locale_string
                                             *localestring){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        std::wstring wstring = wcharconvertor.from_bytes(localestring->string);

        wchar_t *returnvalue = new wchar_t[wstring.length() + 1];

        wcscpy(returnvalue, wstring.c_str());

        return returnvalue;
    }

    void Locale_string_set_locale_string(jetfuel::locale::Locale_string
                                            *localestring, wchar_t *string){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        localestring->string = wcharconvertor.to_bytes(string);
    }

    // jetfuel::locale::String_locale_file wrappers

    jetfuel::locale::String_locale_file *String_locale_file_new(){
        return new jetfuel::locale::String_locale_file();
    }

    wchar_t *String_locale_file_load_string_locale_file(jetfuel::locale::
            String_locale_file *stringlocalefile, wchar_t *filename,
            wchar_t *localename){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        std::string errorstring;

        stringlocalefile->Load_string_locale_file(
                wcharconvertor.to_bytes(filename),
                wcharconvertor.to_bytes(localename),
                &errorstring);

        std::wstring errorwstring = wcharconvertor.from_bytes(errorstring);

        wchar_t *returnvalue = new wchar_t[errorwstring.length() + 1];

        wcscpy(returnvalue, errorwstring.c_str());

        return returnvalue;
    }

    bool String_locale_file_is_locale_file_set(jetfuel::locale::
            String_locale_file *stringlocalefile){
        return stringlocalefile->Is_locale_file_set();
    }

    int String_locale_file_get_locale_string_vector_size(jetfuel::locale::
                                        String_locale_file *stringlocalefile){
        return stringlocalefile->Get_locale_string_vector().size();
    }

    jetfuel::locale::Locale_string
    *String_locale_file_get_locale_string_vector_element(jetfuel::locale::
            String_locale_file *stringlocalefile, int place){
        jetfuel::locale::Locale_string *localestring = new jetfuel::locale::
                                                       Locale_string();

        if(stringlocalefile->Get_locale_string_vector().size() <= place){
            throw jetfuel::core::exceptions::Out_of_vector_range_exception();
        }
        *localestring = stringlocalefile->Get_locale_string_vector().at(place);

        return localestring;
    }

    wchar_t *String_locale_file_get_string_locale_file_name(jetfuel::locale::
                                        String_locale_file *stringlocalefile){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        std::wstring stringlocalefilename = wcharconvertor.
                from_bytes(stringlocalefile->Get_string_locale_file_name());

        wchar_t *returnvalue = new wchar_t[stringlocalefilename.length() + 1];

        wcscpy(returnvalue, stringlocalefilename.c_str());

        return returnvalue;
    }

    wchar_t *String_locale_file_get_string_locale_file_locale_name(jetfuel::
                                locale::String_locale_file *stringlocalefile){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        std::wstring stringlocalefilename = wcharconvertor.
            from_bytes(stringlocalefile->Get_string_locale_file_locale_name());

        wchar_t *returnvalue = new wchar_t[stringlocalefilename.length() + 1];

        wcscpy(returnvalue, stringlocalefilename.c_str());

        return returnvalue;
    }

    // jetfuel::locale::String_locale_manager wrappers

    jetfuel::locale::String_locale_manager *String_locale_manager_new(){
        return new jetfuel::locale::String_locale_manager();
    }

    bool String_locale_manager_load_string_locale_file(jetfuel::locale::
                            String_locale_manager *stringlocalemanager,
                            jetfuel::locale::String_locale_file
                            *stringlocalefile){
        return stringlocalemanager->Load_string_locale_file(*stringlocalefile);
    }

    wchar_t *String_locale_manager_get_active_locale(jetfuel::locale::
                                    String_locale_manager *stringlocalemanager){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        std::wstring wstringactivelocale = wcharconvertor.
                        from_bytes(stringlocalemanager->Get_active_locale());

        wchar_t *returnvalue = new wchar_t[wstringactivelocale.length() + 1];

        wcscpy(returnvalue, wstringactivelocale.c_str());

        return returnvalue;
    }

    bool String_locale_manager_set_active_locale(jetfuel::locale::
                                String_locale_manager *stringlocalemanager,
                                wchar_t *localename){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        return stringlocalemanager->Set_active_locale(
                            wcharconvertor.to_bytes(localename));
    }

    jetfuel::locale::String_locale_file
    *String_locale_manager_get_active_string_locale_file(jetfuel::locale::
                                String_locale_manager *stringlocalemanager){
        jetfuel::locale::String_locale_file *returnvalue =
                new jetfuel::locale::String_locale_file();

        *returnvalue = stringlocalemanager->Get_active_string_locale_file();

        return returnvalue;
    }

    wchar_t *String_locale_manager_get_string_from_id(jetfuel::locale::
                                String_locale_manager *stringlocalemanager,
                                wchar_t *stringid){
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wcharconvertor;

        std::wstring wstringid =
        wcharconvertor.from_bytes(stringlocalemanager->Get_string_from_id(
                                    wcharconvertor.to_bytes(stringid)));

        wchar_t *returnvalue = new wchar_t[wstringid.length() + 1];

        wcscpy(returnvalue, wstringid.c_str());

        return returnvalue;
    }
}
