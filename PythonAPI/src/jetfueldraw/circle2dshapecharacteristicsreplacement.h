/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELDRAW_CIRCLE2DSHAPECHARACTERISTICSREPLACEMENT_H_
#define JETFUELDRAW_CIRCLE2DSHAPECHARACTERISTICSREPLACEMENT_H_

#include <jetfueldraw.h>

namespace jetfuel {
    namespace draw {
        class Circle_2d_shape_characteristics_replacement {
        public:
            Circle_2d_shape_characteristics_replacement() {}

            Circle_2d_shape_characteristics_replacement(Circle_2d_shape::
                                Circle_2d_shape_characteristics circlechars){
                positionx = circlechars.position.x;
                positiony = circlechars.position.y;
                radius = circlechars.radius;
                color = circlechars.color;
                antialiasingstatus = circlechars.antialiasingstatus;
                filledcirclestatus = circlechars.filledcirclestatus;
            }

            Circle_2d_shape::Circle_2d_shape_characteristics
             Convert_to_circle_2d_shape_chars(){
                Circle_2d_shape::Circle_2d_shape_characteristics returnvalue;

                returnvalue.position = Vector2d_int(positionx, positiony);
                returnvalue.radius = radius;
                returnvalue.color = color;
                returnvalue.antialiasingstatus = antialiasingstatus;
                returnvalue.filledcirclestatus = filledcirclestatus;

                return returnvalue;
            }

            int positionx = 0;
            int positiony = 0;
            int radius = 0;
            Color color;
            bool antialiasingstatus = false;
            bool filledcirclestatus = false;
        };
    } /* namespace draw */
} /* namespace jetfuel */

#endif /* JETFUELDRAW_CIRCLE2DSHAPECHARACTERISTICSREPLACEMENT_H_ */
