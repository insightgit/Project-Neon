/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELDRAW_RECTANGLE2DSHAPECHARACTERISTICSREPLACEMENT_H_
#define JETFUELDRAW_RECTANGLE2DSHAPECHARACTERISTICSREPLACEMENT_H_

#include <jetfueldraw.h>

namespace jetfuel {
    namespace draw {

        class Rectangle_2d_shape_characteristics_replacement {
        public:
            Rectangle_2d_shape_characteristics_replacement() {}

            Rectangle_2d_shape_characteristics_replacement(
            jetfuel::draw::Rectangle_2d_shape::Rectangle_2d_shape_characteristics
            rectchars){
                rectpositionx = rectchars.position.x;
                rectpositiony = rectchars.position.y;
                rectsizewidth = rectchars.size.x;
                rectsizeheight = rectchars.size.y;

                fillcolor = rectchars.fillcolor;
                outlinecolor = rectchars.outlinecolor;
            }

            jetfuel::draw::Rectangle_2d_shape::Rectangle_2d_shape_characteristics
             Convert_to_rectangle_2d_shape_chars(){
                jetfuel::draw::Rectangle_2d_shape::
                 Rectangle_2d_shape_characteristics returnvalue;

                returnvalue.position = jetfuel::draw::Vector2d_int(rectpositionx,
                                                                   rectpositiony);
                returnvalue.size = jetfuel::draw::Vector2d_int(rectsizewidth,
                                                               rectsizeheight);
                returnvalue.fillcolor = fillcolor;

                returnvalue.outlinecolor = outlinecolor;

                return returnvalue;
            }

            int rectpositionx = 0;
            int rectpositiony = 0;
            int rectsizewidth = 0;
            int rectsizeheight = 0;

            Color fillcolor;
            Color outlinecolor;
        };

    } /* namespace draw */
} /* namespace jetfuel */

#endif /* JETFUELDRAW_RECTANGLE2DSHAPECHARACTERISTICSREPLACEMENT_H_ */
