/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELDRAW_TEXTCHARACTERISTICSREPLACEMENT_H_
#define JETFUELDRAW_TEXTCHARACTERISTICSREPLACEMENT_H_

#include <string>

#include <jetfueldraw/text.h>

namespace jetfuel {
    namespace draw {

        class Text_characteristics_replacement {
        public:
            Text_characteristics_replacement(){}

            Text_characteristics_replacement(jetfuel::draw::Font *fonttouse) {
                font = *fonttouse;
            }

            Text_characteristics_replacement(jetfuel::draw::Text::
                                             Text_characteristics textchars){
                switch(textchars.rendermode){
                    case jetfuel::draw::Text::Render_mode::Solid:
                        rendermode = 1;
                        break;
                    case jetfuel::draw::Text::Render_mode::Shaded:
                        rendermode = 2;
                        break;
                    case jetfuel::draw::Text::Render_mode::Blended:
                        rendermode = 3;
                        break;
                }

                textstring = textchars.textstring;
                textcolor = textchars.textcolor;
                backgroundcolor = textchars.backgroundcolor;
                fontoutlinewidth = textchars.fontoutlinewidth;
                kerningstatus = textchars.kerningstatus;
                fontstyle = textchars.fontstyle;
                fontsize = textchars.fontsize;

                switch(textchars.fonthinting){
                    case jetfuel::draw::Text::Font_hinting::Normal:
                        rendermode = 1;
                        break;
                    case jetfuel::draw::Text::Font_hinting::Light:
                        rendermode = 2;
                        break;
                    case jetfuel::draw::Text::Font_hinting::Mono:
                        rendermode = 3;
                        break;
                    case jetfuel::draw::Text::Font_hinting::None:
                        rendermode = 3;
                        break;
                }

                font = textchars.font;

                previousfont = font;
            }

            jetfuel::draw::Text::Text_characteristics
             Convert_to_text_chars_struct(){
                jetfuel::draw::Text::Text_characteristics returnvalue;

                switch(rendermode){
                    case 1:
                        returnvalue.rendermode = jetfuel::draw::Text::
                                                 Render_mode::Solid;
                        break;
                    case 2:
                        returnvalue.rendermode = jetfuel::draw::Text::
                                                 Render_mode::Shaded;
                        break;
                    case 3:
                        returnvalue.rendermode = jetfuel::draw::Text::
                                                 Render_mode::Blended;
                        break;
                }

                returnvalue.textstring = textstring;
                returnvalue.textcolor = textcolor;
                returnvalue.backgroundcolor = backgroundcolor;
                returnvalue.fontoutlinewidth = fontoutlinewidth;
                returnvalue.kerningstatus = kerningstatus;
                returnvalue.fontstyle = fontstyle;
                returnvalue.fontsize = fontsize;

                switch(fonthinting){
                    case 1:
                        returnvalue.fonthinting = jetfuel::draw::Text::
                                                  Font_hinting::Normal;
                        break;
                    case 2:
                        returnvalue.fonthinting = jetfuel::draw::Text::
                                                 Font_hinting::Light;
                        break;
                    case 3:
                        returnvalue.fonthinting = jetfuel::draw::Text::
                                                  Font_hinting::Mono;
                        break;
                    case 4:
                        returnvalue.fonthinting = jetfuel::draw::Text::
                                                  Font_hinting::None;
                        break;
                }

                returnvalue.font = previousfont;

                return returnvalue;
            }

            int rendermode = 1;
            std::string textstring;

            jetfuel::draw::Color textcolor;
            jetfuel::draw::Color backgroundcolor;
            unsigned int fontoutlinewidth = 0;
            bool kerningstatus = true;
            int fontstyle = FONTSTYLE_NORMAL;
            unsigned int fontsize = 20;
            int fonthinting = 1;
            Font font;

            Font previousfont;
        };



    } /* namespace draw */
} /* namespace jetfuel */

#endif /* JETFUELDRAW_TEXTCHARACTERISTICSREPLACEMENT_H_ */
