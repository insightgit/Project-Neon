/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "circle2dshape.h"

namespace jetfuel{
    namespace draw{
        Circle_2d_shape::Circle_2d_shape(const Circle2d_int circletodraw){
            m_circletodraw = circletodraw;
        }
        
        bool Circle_2d_shape::Draw(){
                if(Get_filled_circle_status()){
                    // Draws a filled circle and returns the results.

                    if(filledCircleRGBA(Get_renderer(), Get_position().x,    
                                        Get_position().y, Get_radius(),
                                        Get_color().r, Get_color().g,
                                        Get_color().b, Get_color().a) != 0){
                        return false;                   
                    }else{
                        return true;
                    }
                                        
                }
        
            if(Get_anti_aliasing_status()){
                // Draws an anti-aliased hollow circle and returns the
                // results.

                if(aacircleRGBA(Get_renderer(), Get_position().x,    
                                        Get_position().y, Get_radius(),
                                        Get_color().r, Get_color().g,
                                        Get_color().b, Get_color().a) != 0){
                    return false;
                }else{
                    return true;
                }
            }else{
                // Draws a hollow circle and returns the results.

                if(circleRGBA(Get_renderer(), Get_position().x,    
                                        Get_position().y, Get_radius(),
                                        Get_color().r, Get_color().g,
                                        Get_color().b, Get_color().a) != 0){  
                    return false;
                }else{
                    return true;
                }     
            }
        }
    }
}

