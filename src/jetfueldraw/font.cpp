/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "font.h"

namespace jetfuel {
    namespace draw {
        Font::Font(const std::string filename) {
            if(TTF_Init() < 0){
                throw exceptions::SDL_ttf_exception("TTF_Init()");
            }

            m_filename = filename;

            m_isfontloaded = true;
        }

        Font::Font(const std::string filename, const long faceindex) {
            if(TTF_Init() < 0){
                throw exceptions::SDL_ttf_exception("TTF_Init()");
            }

            m_filename = filename;
            m_faceindex = faceindex;

            m_isfontloaded = true;
        }

        void Font::Load_font(const std::string filename){
            Set_file_name(filename);

            Set_is_font_loaded(true);
        }

        void Font::Load_font(const std::string filename, const long faceindex){
            Set_file_name(filename);
            Set_face_index(faceindex);

            Set_is_font_loaded(true);
        }

        void Font::Change_size(const unsigned int size){
            // Open Font with font index if index is set.

            if(Get_file_name()!=""){
                Set_ttf_font(nullptr);

                if(Get_face_index()!=-1){
                    Set_ttf_font(TTF_OpenFontIndex(Get_file_name().c_str(),size,
                                                   Get_face_index()));
                }else{
                    Set_ttf_font(TTF_OpenFont(Get_file_name().c_str(),size));
                }

                if(Get_ttf_font()==nullptr){
                    throw exceptions::SDL_ttf_exception("TTF_OpenFont()");
                }
            }
        }
    } /* namespace draw */
} /* namespace jetfuel */
