/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "color.h"

namespace jetfuel {
    namespace draw {
        const Color Color::Black(0,0,0); //< Black predefined color
        const Color Color::White(255,255,255); //< White predefined
        //^ color
        const Color Color::Red(255,0,0); //< Red predefined color
        const Color Color::Green(0,255,0); //< Green predefined color
        const Color Color::Blue(0,0,255); //< Blue predefined color
        const Color Color::Yellow(255,255,0); //< Yellow predefined
        //^ color
        const Color Color::Magenta(255,0,255); //< Magenta predefined
        //^ color
        const Color Color::Cyan(0,255,255); //< Cyan predefined color

        Color::Color() : Color(0,0,0,255){}

        Color::Color(unsigned int red, unsigned int green, unsigned int blue,
                     unsigned int alpha){
            r = red;
            g = green;
            b = blue;
            a = alpha;
        }

        bool operator ==(const Color left, const Color right){
            return (left.r == right.r && left.g == right.g &&
                    left.b == right.b && left.a == right.a);
        }

        bool operator !=(const Color left, const Color right){
            return (left.r != right.r && left.g != right.g &&
                    left.b != right.b && left.a != right.a);
        }

        Color operator +(const Color left, const Color right){
            // Clamps result to 255 if above 255

            return Color(std::max<unsigned int>(255,left.r + right.r),
                         std::max<unsigned int>(255,left.g + right.g),
                         std::max<unsigned int>(255,left.b + right.b),
                         std::max<unsigned int>(255, left.a + right.a));
        }

        Color operator -(const Color left, const Color right){
            // Clamps result to 0 if below 0

            return Color(std::min<unsigned int>(0,left.r - right.r),
                         std::min<unsigned int>(0,left.g - right.g),
                         std::min<unsigned int>(0,left.b - right.b),
                         std::min<unsigned int>(0, left.a - right.a));
        }

        Color operator +=(Color left, Color right){
            return left = left + right;
        }

        Color operator -=(Color left, Color right){
            return left = left - right;
        }

    } /* namespace draw */
} /* namespace jetfuel */
