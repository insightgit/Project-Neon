/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "scene.h"

#include "scenemanager.h"

namespace jetfuel {
    namespace draw {
        Scene::Scene(const int id, const std::string label) {
            m_id = id;
            m_label = label;
        }

        Scene::~Scene(){
            for(int i = 0; m_drawables.size() > i; ++i){
                if(&m_drawables[i] != nullptr){
                    m_drawables.erase(m_drawables.begin()+i);
                }
            }

        }

        void Scene::Attach_drawable(Drawable *drawable,
                                    const unsigned int drawingweight){
            // Assigns the scene_manager's renderer to
            // the drawable, then adds the drawable
            // to the internal private m_sprites vector.

            if(!drawable->Assigned_renderer()){
                drawable->Assign_renderer(Get_scene_manager_renderer());
            }

            Add_drawable_placement_to_drawable_placement_vector(drawingweight);
            Add_drawable_to_drawable_vector(drawable);
        }

        void Scene::Disable_drawable(Drawable *drawable){
            // Locates drawable by UUID and then, if found disables
            // the drawable.

            for(int i = 0; m_drawables.size() > i; ++i){
                if(*drawable == *m_drawables[i]){
                    Disable_drawable_in_vector(i);
                }
            }
        }

        void Scene::Enable_drawable(Drawable *drawable){
            for(int i = 0; m_drawables.size() > i; ++i){
                if(*drawable == *m_drawables[i]){
                    Enable_drawable_in_vector(i);
                }
            }
        }

        void Scene::Draw_scene(){
            // Draws background color (if there is any), and then
            // Draws drawables based on drawing placement
            // (or drawingweight)

            // Draws background color

            if(Is_background_color_set()){
                SDL_SetRenderDrawColor(Get_scene_manager_renderer(),
                                       Get_background_color().r,
                                       Get_background_color().g,
                                       Get_background_color().b,
                                       Get_background_color().a);

                SDL_RenderClear(Get_scene_manager_renderer());
            }

            // Draws drawables based on drawing placement
            // (or drawingweight).

            if(Get_highest_placement()!=0){
                int currentint = 1;

                while(currentint<=Get_highest_placement()){
                    for(int i = 0; Get_size_of_drawables_vector() > i; ++i){
                        Drawable *currentdrawable = Get_drawable_in_vector(i);

                        if(Get_drawable_placement_in_vector(i) == currentint &&
                           Get_drawable_status(i) && currentdrawable != nullptr){
                            SDL_SetRenderDrawBlendMode(
                            Get_scene_manager_renderer(),SDL_BLENDMODE_BLEND);

                            currentdrawable->Draw();
                        }
                    }
                    ++currentint;
                }
            }

            SDL_RenderPresent(Get_scene_manager_renderer());
        }
    } /* namespace draw */
} /* namespace jetfuel */
