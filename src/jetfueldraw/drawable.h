/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELDRAW_DRAWABLE_H_
#define JETFUELDRAW_DRAWABLE_H_
#include <SDL2/SDL.h>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <jetfueldraw/rect2d.h>

namespace jetfuel {
    namespace draw {
        class Drawable {
        public:
            /// \brief Returns internal m_position variable.
            ///
            /// Returns private internal m_position
            /// variable used for positioning.
            virtual Vector2d_int Get_position(){
                return m_position;
            }

            /// \brief Set the internal m_position variable.
            ///
            /// Set the internal private m_position variable
            /// used for positioning.
            ///
            /// \param Vector2d_int position
            virtual void Set_position(const Vector2d_int position){
                m_position = position;

                m_dest.x = m_position.x;
                m_dest.y = m_position.y;
            }

            /// \brief Using the Drawable's UUID, compare this
            /// Drawable with another.
            ///
            /// Using the Drawable's UUID, compare this Drawable with
            /// another. The UUID ensures that different Drawables
            /// with the same exact position and renderer don't get
            /// falsely marked as equals.
            ///
            /// \param Drawable &drawable
            bool operator ==(Drawable &drawable){
                return m_UUID == drawable.m_UUID;
            }

            /// \brief Returns whether this Drawable was assigned a
            /// renderer.
            ///
            /// Returns whether this Drawable was assigned a renderer
            /// based on a internal private boolean variable set to
            /// true when the function Assign_renderer has been called
            /// and false otherwise.
            bool Assigned_renderer() const{
                return m_rendererset;
            }

            /// \brief Assigns the renderer to be used by
            /// the object.
            ///
            /// Assigns the renderer to be used by the
            /// object while drawing.
            ///
            /// This function should only be used by the
            /// Scene_manager if you are using the Scene_manager and
            /// Scene objects.
            ///
            /// \param SDL_Renderer *renderer
            virtual void Assign_renderer(SDL_Renderer *renderer){
                m_renderer = renderer;

                m_rendererset = true;
            }

            /// \brief Draws the object.
            ///
            /// This function (which should draw the object)
            /// is a pure virtual one that must be implemented
            /// by any class that inherits this class.
            virtual bool Draw() = 0;
        protected:
            /// \brief Gets the internal destination SDL_Rect
            /// variable.
            ///
            /// Gets the internal private destination SDL_Rect
            /// variable to be used when drawing
            /// sand positioning a Drawable.
            SDL_Rect Get_dest() const {
                return m_dest;
            }

            /// \brief Sets the internal destination SDL_Rect
            /// variable.
            ///
            /// Sets the internal private destination SDL_Rect
            /// variable to be used when drawing
            /// and positioning a Drawable.
            ///
            /// \param SDL_Rect dest
            void Set_dest(const SDL_Rect dest) {
                m_dest = dest;
            }

            /// \brief Sets the variable that tells whether the
            /// renderer was assigned or not.
            ///
            /// Sets the variable that tells whether the
            /// renderer was assigned or not.
            ///
            /// \param bool rendererassigned
            void Set_assigned_renderer(bool rendererassigned){
                m_rendererset = rendererassigned;
            }

            /// \brief Gets the renderer to be used for
            /// drawing.
            ///
            /// Gets the renderer to be used for drawing
            /// purposes.
            virtual SDL_Renderer *Get_renderer(){
                return m_renderer;
            }
        private:
            ///////////////////
            /// member data ///
            ///////////////////

            SDL_Renderer *m_renderer;///< Renderer to be used for
                                     ///< drawing.

            bool m_rendererset = false;///< Whether the renderer was
                                       ///< set or not.

            SDL_Rect m_dest;///< SDL dest rect for drawing and
                            ///< positioning.

            boost::uuids::uuid m_UUID =
             boost::uuids::uuid(boost::uuids::random_generator()());
                                       ///< Boost UUID used for
                                       ///< uniquely identifying
                                       ///< Drawables

            Vector2d_int m_position;  
                                    ///< Position to be used for
                                    ///< positioning.
        };

        /// \class jetfuel::draw::Drawable
        ///
        /// A simple header-only base class that provides a base
        /// if you want to make a sprite that uses the renderer.
        ///
        /// You must inherit from this class if you want to draw it
        /// to the screen via jetfuel::draw::Scene.
        ///
        /// \see jetfuel::draw::Sprite
        /// \see jetfuel::draw::Text
        /// \see jetfuel::draw::Rectangle_2d_shape

    } /* namespace draw */
} /* namespace jetfuel */

#endif /* JETFUELDRAW_DRAWABLE_H_ */
