/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELDRAW_CIRCLEINTERFACE_H_
#define JETFUELDRAW_CIRCLEINTERFACE_H_

#include <jetfueldraw/circle2d.h>

namespace jetfuel {
    namespace draw {

        class Circle_interface {
        public:
            /// \brief Gets the circle that will be drawn.
            ///
            /// This is a pure virtual function (that should get the
            /// circle that will be drawn) that must be implemented
            /// by any class that inherits this class.
            virtual Circle2d_int Get_circle_to_draw() = 0;
        };
        /// jetfuel::draw::Circle_interface
        ///
        /// A simple interface that specifies unique functions
        /// Drawable circles should implement.
        ///
        /// \see jetfuel::draw::Circle_2d_shape

    } /* namespace draw */
} /* namespace jetfuel */

#endif /* JETFUELDRAW_CIRCLEINTERFACE_H_ */
