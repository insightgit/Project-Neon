/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <jetfueldraw/text.h>

namespace jetfuel {
    namespace draw {

        void Text::Pre_render_text(){
            // Renders text into a SDL_Surface that will later be
            // converted to a SDL_Texture to be drawn on the screen.

            switch(Get_render_mode()){
                case Render_mode::Solid:
                    Set_text_surface(TTF_RenderUTF8_Solid(
                                         Get_internal_font().Get_ttf_font(),
                                         Get_string().c_str(),
                                         Convert_Color_to_SDL_Color(
                                                 Get_text_color())));
                    break;
                case Render_mode::Shaded:
                    Set_text_surface(TTF_RenderUTF8_Shaded(
                                         Get_internal_font().Get_ttf_font(),
                                         Get_string().c_str(),
                                         Convert_Color_to_SDL_Color(
                                                 Get_text_color()),
                                         Convert_Color_to_SDL_Color(
                                                 Get_background_color())));
                    break;
                case Render_mode::Blended:
                    Set_text_surface(TTF_RenderUTF8_Blended(
                                         Get_internal_font().Get_ttf_font(),
                                         Get_string().c_str(),
                                         Convert_Color_to_SDL_Color(
                                                 Get_text_color())));
                    break;
            }

        }

        void Text::Pre_render(){
            // Changes font size if it has changed

            if(Get_past_font_size() != Get_font_size()){
                Font font = Get_internal_font();
                font.Change_size(Get_font_size());
                Set_internal_font(font);

                Set_is_font_created(true);
            }

            // If font is ready, set the font style,
            // create the SDL_TTF text surface, set the kerning status
            // and set font hinting and font kerning if not set.

            if(Is_ready_for_pre_rendering() && Is_font_created() &&
               Get_internal_font().Get_ttf_font() != nullptr){
                Set_is_ready_to_be_drawn(false);

                TTF_SetFontStyle(Get_internal_font().Get_ttf_font(),
                                                  Get_font_style());

                int hinting = static_cast<typename
                 std::underlying_type<Font_hinting>::type>(Get_font_hinting());
                  TTF_SetFontHinting(Get_internal_font().Get_ttf_font(),
                                     hinting);

                Pre_render_text();

                if(Get_text_surface() == nullptr){
                    throw exceptions::Nullptr_SDL_ttf_exception();
                }

                if(Waiting_to_set_kerning_status()){
                    Set_past_kerning_status();
                }

                if(Waiting_to_set_font_hinting()){
                    Set_past_font_hinting();
                }

                Set_is_ready_to_be_drawn(true);
                Set_is_surface_copied(false);
            }

            Set_past_font_size(Get_font_size());
        }

        Text::Text(Font font) {
            // Load font if initialized, otherwise throw exception.

            if(font.Is_font_loaded()){
                m_font = font;
            }else{
               throw exceptions::Font_not_init_exception();
            }

            Pre_render();
        }

        Text::~Text(){
            if(Get_text_surface() != nullptr && Is_surface_copied() &&
               Get_string() != ""){
                SDL_FreeSurface(Get_text_surface());
            }
        }

        void Text::Set_font(Font font){
            // Load font if initialized, otherwise throw exception.

            if(font.Is_font_loaded()){
                font.Change_size(Get_font_size());
                Set_internal_font(font);
            }else{
               throw exceptions::Font_not_init_exception();
            }

            Pre_render();
        }

        bool Text::Draw(){
            // If ready to be drawn, create SDL_TTF texture from
            // SDL_TTF surface (if not done yet), calculate
            // pixel size of text, then draw texture onto screen.

            if(Is_ready_to_be_drawn()){
                SDL_Rect dest;
                if(!Is_surface_copied() && Get_text_surface() != nullptr){
                    if(Is_font_rendering_for_first_time()){
                        Set_font_rendering_for_first_time(false);
                        return true;
                    }

                    SDL_Texture *texturefromsurface =
                     SDL_CreateTextureFromSurface(Get_renderer(),
                                                  Get_text_surface());

                    Set_text_rendered(texturefromsurface);

                    if(Get_text_rendered() != nullptr){
                        Set_is_surface_copied(true);
                    }else{
                        throw exceptions::Nullptr_SDL_ttf_exception();
                    }
                }


                dest.x = Get_dest().x;
                dest.y = Get_dest().y;

                int width;
                int height;

                if(Get_internal_font().Get_ttf_font() != nullptr){
                    TTF_SizeUTF8(Get_internal_font().Get_ttf_font(),
                                 Get_string().c_str(), &width, &height);
                }

                dest.w = width;
                dest.h = height;

                Set_rect_to_draw(dest.x,dest.y,dest.w,dest.h);

                SDL_RenderCopy(Get_renderer(),Get_text_rendered(), NULL, &dest);

                return true;
            }
            return false;
        }
    } /* namespace draw */
} /* namespace jetfuel */
