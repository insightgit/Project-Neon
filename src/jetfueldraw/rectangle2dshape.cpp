/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rectangle2dshape.h"

namespace jetfuel {
    namespace draw {
        Rectangle_2d_shape::Rectangle_2d_shape(Vector2d_int position,
                                               Vector2d_int size) {
            m_recttodraw.x = position.x;
            m_recttodraw.y = position.y;

            m_recttodraw.width = size.x;
            m_recttodraw.height = size.y;
        }

        Rectangle_2d_shape::Rectangle_2d_shape(Rect2d_int rect){
            m_recttodraw = rect;
        }

        bool Rectangle_2d_shape::Draw(){
            if(Get_renderer() == nullptr){
                return false;
            }

            // Sets up drawing variables

            SDL_Rect sdlrect;

            sdlrect.x = Get_rect_to_draw().x;
            sdlrect.y = Get_rect_to_draw().y;

            sdlrect.h = Get_rect_to_draw().height;
            sdlrect.w = Get_rect_to_draw().width;

            // Sets fill color for rectangle.

            SDL_SetRenderDrawColor(Get_renderer(), Get_fill_color().r, 
             Get_fill_color().g, Get_fill_color().b, Get_fill_color().a);

            // Renders a filled rectangle with fill color.

            SDL_RenderFillRect(Get_renderer(), &sdlrect);

            // Draws outline if it needs to.

            if(Is_draw_outline()){
                SDL_SetRenderDrawColor(Get_renderer(), Get_outline_color().r,
                 Get_outline_color().g, Get_outline_color().b,
                 Get_outline_color().a);

                SDL_RenderDrawRect(Get_renderer(), &sdlrect);
            }

            // Resets renderer drawing color.

            SDL_SetRenderDrawColor(Get_renderer(), 0, 0, 0, 255);

            return true;
        }
    } /* namespace draw */
} /* namespace jetfuel */
