/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELDRAW_RECTANGLEINTERFACE_H_
#define JETFUELDRAW_RECTANGLEINTERFACE_H_

#include <jetfueldraw/rect2d.h>

namespace jetfuel {
    namespace draw {
        class Rectangle_interface {
        public:
            /// \brief Returns the Rect2d to be drawn.
            ///
            /// Returns the jetfuel::draw::Rect2d to be drawn when
            /// Draw() is called.
            /// This is a pure virtual function that must be
            /// implemented by the children. This Rect2d_int
            /// will be a rectangle of the entire Drawable.
            virtual Rect2d_int Get_rect_to_draw() = 0;
        };
        /// \class jetfuel::draw::Rectangle_interface
        ///
        /// A simple interface that specifies unique functions
        /// Drawable rectangles should implement.
        ///
        /// \see jetfuel::draw::Sprite
        /// \see jetfuel::draw::Rectangle_2d_shape
        /// \see jetfuel::draw::Text

    } /* namespace draw */
} /* namespace jetfuel */
#endif /* JETFUELDRAW_RECTANGLEINTERFACE_H_ */

