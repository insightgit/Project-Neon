/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELDRAW_VECTOR2D_H_
#define JETFUELDRAW_VECTOR2D_H_

namespace jetfuel {
    namespace draw {
        template <class T>
        class Vector2d {
        public:
            /// \brief Constructs an empty Vector2d.
            ///
            /// Constructs an empty Vector2d with empty x and y
            /// coordinates.
            Vector2d(){}

            /// \brief Constructs a Vector2d with an x and y
            /// coordinates.
            ///
            /// Constructs a Vector2d with an x and y
            /// coordinates.
            ///
            /// \param T X
            /// \param T Y
            Vector2d(T X, T Y){
                x=X;
                y=Y;
            }
            ///////////////////
            /// member data ///
            ///////////////////

            T x; ///< x coordinate
            T y; ///< y coordinate
        };


        template <typename T>
        Vector2d<T> operator +(const Vector2d<T> left,const Vector2d<T> right){
            return Vector2d<T>(left.x + right.x, left.y + right.y);
        }

        template <typename T>
        Vector2d<T> operator +=(Vector2d<T> &left,const Vector2d<T> right){
            left.x += right.x;
            left.y += right.y;

            return left;
        }

        template <typename T>
        Vector2d<T> operator -(const Vector2d<T> left,const Vector2d<T> right){
            return Vector2d<T>(left.x - right.x, left.y - right.y);
        }

        template <typename T>
        Vector2d<T> operator -=(Vector2d<T> &left,const Vector2d<T> right){
            left.x -= right.x;
            left.y -= right.y;

            return left;
        }

        template <typename T>
        Vector2d<T> operator *(const Vector2d<T> left, const Vector2d<T> right){
            return Vector2d<T>(right.x * left.x, right.y * left.y);
        }

        template <typename T>
        Vector2d<T> operator *=(Vector2d<T> &left, const Vector2d<T> right){
            left.x * right.x;
            left.y * right.y;

            return left;
        }

        template <typename T>
        Vector2d<T> operator /(const Vector2d<T> left, const Vector2d<T> right){
            return Vector2d<T>(left.x/right.x, left.y/right.y);
        }

        template <typename T>
        Vector2d<T> operator /=(Vector2d<T> &left, const Vector2d<T> right){
            left.x /= right.x;
            left.y /= right.y;

            return left;
        }

        template <typename T>
        Vector2d<T> operator *(T left, const Vector2d<T> right){
            return Vector2d<T>(right.x * left, right.y * left);
        }

        template <typename T>
        Vector2d<T> operator *=(Vector2d<T> &left, const T right){
            left.x * right;
            left.y * right;

            return left;
        }

        template <typename T>
        Vector2d<T> operator /(const Vector2d<T> left, const T right){
            return Vector2d<T>(left.x/right, left.y/right);
        }

        template <typename T>
        Vector2d<T> operator /=(Vector2d<T> &left, const T right){
            left.x /= right;
            left.y /= right;

            return left;
        }

        template <typename T>
        Vector2d<T> operator +(const Vector2d<T> left, const T right){
            return Vector2d<T>(left.x + right, left.y + right);
        }

        template <typename T>
        Vector2d<T> operator +=(Vector2d<T> &left, const T right){
            left.x += right;
            left.y += right;

            return left;
        }

        template <typename T>
        Vector2d<T> operator -(const Vector2d<T> left,const T right){
            return Vector2d<T>(left.x - right, left.y - right);
        }

        template <typename T>
        Vector2d<T> operator -=(Vector2d<T> &left,const T right){
            left.x -= right;
            left.y -= right;

            return left;
        }

        template <typename T>
        bool operator ==(const Vector2d<T> left, const Vector2d<T> right){
            return (left.x == right.x) && (left.y == right.y);
        }

        template <typename T>
        bool operator !=(const Vector2d<T> left, const Vector2d<T> right){
            return (left.x != right.x) && (left.y != right.y);
        }

        ////////////////////////////////
        /// Common Vector2d typedefs ///
        ////////////////////////////////

        typedef Vector2d<unsigned int> Vector2d_uint;
        typedef Vector2d<int> Vector2d_int;
        typedef Vector2d<float> Vector2d_float;


        /// \class jetfuel::draw::Vector2d
        ///
        /// A simple 2d vector class that stores an x coordinate and a
        /// y coordinate. This class is used for the position of
        /// drawable objects on the screen in Jetfuel.
        ///
        /// Code Example:
        ///     Vector2d_int myvector(5,8);
        ///
        ///     std::cout << "My x coordinate is" << myvector.x <<
        ///                  "My y coordinate is" << myvector.y <<
        ///     std::endl;
    } /* namespace draw */

} /* namespace jetfuel */

#endif /* JETFUELDRAW_VECTOR2D_H_ */
