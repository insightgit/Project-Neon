/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sprite.h"

namespace jetfuel {
    namespace draw {
        Sprite::Sprite(){}

        Sprite::~Sprite(){
            // If a texture exists, destroy it.

            if(Get_texture() != nullptr && Get_renderer() == nullptr
               && Get_texture() != 0x555500000000 &&
               Get_texture() != 0xde100000000){
                SDL_DestroyTexture(m_texture);
            }
        }

        bool Sprite::Load_from_image(const Image image){
            // Get the renderer, and load the image into a
            // SDL_Texture. If there is no texture after that,
            // return false.

            Set_texture(IMG_LoadTexture(Get_renderer(),
                        image.Get_image_location().c_str()));

            if(Get_texture() == nullptr){
                return false;
            }

            Set_dest_width_height(image.Get_size_of_image());
            Set_size_of_sprite(image.Get_size_of_image());

            Set_dynamic_image_loaded(true);

            return true;
        }

        void Sprite::Dynamic_load_from_image(const Image image){
            Set_dynamic_image(image);

            Set_dynamic_image_loaded(false);
        }

        bool Sprite::Draw(){
           // Load image if dynamic and not loaded yet.

           if(!Is_dynamic_image_loaded() &&
               Get_dynamic_image().Get_image_location() != ""){
               Load_from_image(Get_dynamic_image());

               Set_dynamic_image_loaded(true);
           }
           
           // If there is no renderer or texture, return false.
           // Otherwise, draw the Sprite.

           SDL_Renderer *renderer = Get_renderer();
           SDL_Texture *texture = m_texture;

           if(renderer == nullptr || texture == nullptr){
               return false;
           }else{
               Set_dest_position(Get_position());
               SDL_Rect dest = Get_dest_rect();

               if(Is_src_view_rect_set()){
                   SDL_RenderCopy(Get_renderer(), Get_texture(), &Get_src_rect(),
                                  &dest);
               }else{
                   SDL_RenderCopy(Get_renderer(), Get_texture(), NULL, &dest);
               }

               return true;
           }
        }
    } /* namespace draw */
} /* namespace jetfuel */
