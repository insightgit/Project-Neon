/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "image.h"

namespace jetfuel {
    namespace draw {
        Image::Image(const std::string imagepath, Scene_manager *scenemanager) {
            // Set image path to internal variable, then load a texture
            // with the given scenemanager's renderer
            // to find the size of the image,
            // then destroy that texture.

            m_imagepath = imagepath;

            SDL_Texture *imagetexture = IMG_LoadTexture(scenemanager->
                                                        Get_renderer(),
                                                        m_imagepath.c_str());
            SDL_QueryTexture(imagetexture, NULL, NULL, &m_sizeofimage.x,
                             &m_sizeofimage.y);

            SDL_DestroyTexture(imagetexture);
        }

        Image::Image(const std::string imagepath, const Vector2d_int imagesize){
            m_imagepath = imagepath;
            m_sizeofimage = imagesize;
        }

        void Image::Set_image(const std::string imagepath, 
                              Scene_manager *scenemanager){
            // Set image path to internal variable, then load a texture
            // with the given scenemanager's renderer
            // to find the size of the image,
            // then destroy that texture.                  
                              
            m_imagepath = imagepath;

            scenemanager->Get_window_id();

            SDL_Texture *imagetexture = IMG_LoadTexture(scenemanager->
                                                        Get_renderer(),
                                                        m_imagepath.c_str());

            SDL_QueryTexture(imagetexture, NULL, NULL, &m_sizeofimage.x,
                             &m_sizeofimage.y);

            SDL_DestroyTexture(imagetexture);
        }

        void Image::Set_image(const std::string imagepath, 
                              const Vector2d_int imagesize){
            m_imagepath = imagepath;
            m_sizeofimage = imagesize;
        }
    } /* namespace draw */
} /* namespace jetfuel */
