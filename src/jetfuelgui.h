/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef JETFUELGUI_H_
#define JETFUELGUI_H_

#include "jetfuelcore.h"

#include "jetfuelgui/menu.h"
#include "jetfuelgui/checkbox.h"
#include "jetfuelgui/dropdownbox.h"
#include "jetfuelgui/progressbar.h"
#include "jetfuelgui/slider.h"

#endif /* JETFUELGUI_H_ */
