/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELMEDIA_MUSIC_H_
#define JETFUELMEDIA_MUSIC_H_

#include <jetfuelmedia/sound.h>

#include "../jetfueldraw/scenemanager.h"


namespace jetfuel {
    namespace media {
        class Music : public Sound {
        public:
            Music();

            bool Is_music_playing(){
                if(Mix_PlayingMusic() == 1){
                    m_playing = true;
                }else{
                    m_playing = false;
                }

                return m_playing;
            }

            bool Is_music_paused() const{
                return m_paused;
            }

            std::string Get_loaded_music_file_path() const{
                return m_musicfilepath;
            }

            bool Load_audio_file(const std::string musicfilepath)override{
                m_music = Mix_LoadMUS(musicfilepath.c_str());
                if(m_music == NULL){
                    return false;
                }else{
                    m_musicfilepath = musicfilepath;
                    return true;
                }
            }

            bool Play()override;

            void Pause()override;

            void Resume()override;
        protected:
            bool Is_music_loaded() const{
                return m_music != NULL;
            }

            bool Play_music(){
                if(m_music != NULL){
                    Mix_PlayMusic(m_music, Get_active_channel_num());

                    m_playing = true;
                    return true;
                }else{
                    return false;
                }
            }

            void Pause_music(){
                Mix_PauseMusic();

                m_playing = false;
                m_paused = true;
            }

            void Resume_music(){
                Mix_ResumeMusic();

                m_playing = true;
                m_paused = false;
            }

        private:
            Mix_Music *m_music;

            bool m_playing = false;
            bool m_paused = false;

            std::string m_musicfilepath;
        };
    } /* namespace media */
} /* namespace jetfuel */

#endif /* JETFUELMEDIA_MUSIC_H_ */
