/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "music.h"

namespace jetfuel {
    namespace media {
        Music::Music() {
            if(SDL_Init(SDL_INIT_AUDIO) != 0){
                throw jetfuel::draw::exceptions::SDL_Init_exception(Mix_GetError());
            }

            Set_frequency(Get_frequency());
        }

        bool Music::Play(){
            if(Is_music_loaded() && !Is_music_playing() && !Is_music_paused()){
                return Play_music();
            }else{
                return false;
            }
        }

        void Music::Pause(){
            if(Is_music_loaded() && Is_music_playing()){
                Pause_music();
            }
        }

        void Music::Resume(){
            if(Is_music_loaded() && Is_music_paused()){
                Resume_music();
            }
        }
    } /* namespace media */
} /* namespace jetfuel */
