/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "soundeffect.h"

namespace jetfuel{

    namespace media{
        Sound_effect::Sound_effect(){
            if(SDL_Init(SDL_INIT_AUDIO) != 0){
                throw jetfuel::draw::exceptions::SDL_Init_exception(Mix_GetError());
            }

            Set_frequency(Get_frequency());
        }

        Sound_effect::~Sound_effect(){
            if(m_soundfx != nullptr){
                Mix_FreeChunk(m_soundfx);
            }
        }

        bool Sound_effect::Play(){
            if(Is_sound_effect_loaded()){
                return Play_sound_effect();
            }else{
                return false;
            }
        }

        void Sound_effect::Resume(){
            Resume_sound_effect();
        }

        void Sound_effect::Pause(){
            Pause_sound_effect();
        }
    }

}
