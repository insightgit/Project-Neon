/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NEONCONTROL_SOUNDEFFECT_H_
#define NEONCONTROL_SOUNDEFFECT_H_
#include <jetfuelmedia/sound.h>

#include <jetfueldraw/scenemanager.h>


namespace jetfuel{
   
   namespace media{
   
       class Sound_effect : public Sound{
       public:
           Sound_effect();
           
           virtual ~Sound_effect();

           static bool Is_sound_effect_or_music_playing(){
               return Mix_Playing(-1) > 0;
           }

           std::string Get_loaded_audio_file_path() const{
               return m_musicfilepath;
           }

           bool Load_audio_file(const std::string musicfilepath)override{
               m_soundfx = Mix_LoadWAV(musicfilepath.c_str());
               if(m_soundfx == NULL){
                   return false;
               }else{
                   m_musicfilepath = musicfilepath;
                   return true;
               }
           }

           unsigned int Get_times_to_repeat() const{
               return m_repeattimes;
           }

           void Set_times_to_repeat(const unsigned int repeattimes){
               m_repeattimes = repeattimes;
           }

           bool Play()override;
           
           void Pause()override;
           
           void Resume()override;
       protected:
           bool Is_sound_effect_loaded()const{
               return m_soundfx != NULL;
           }

           bool Play_sound_effect(){
               if(m_soundfx != NULL){
                   Mix_PlayChannel(Get_active_channel_num(), m_soundfx,
                                   Get_times_to_repeat());
                   return true;
               }else{
                   return false;
               }
           }

           void Pause_sound_effect(){
               Mix_Pause(Get_active_channel_num());
           }

           void Resume_sound_effect(){
               Mix_Resume(Get_active_channel_num());
           }
       private:
           Mix_Chunk *m_soundfx;

           unsigned int m_repeattimes = 0;

           std::string m_musicfilepath;
       };

   }
   
}

#endif
