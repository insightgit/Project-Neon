/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dropdownbox.h"

namespace jetfuel {
    namespace gui {
        void Drop_down_box::Add_option(const std::string option){
            if(Get_size_of_dropdown_options_vector() <= 0 && !Is_base_box_set()){
                Set_base_box_option(option);
            }else{
                Drop_down_option newoptiontoadd;

                if(Get_renderer() != nullptr){
                    newoptiontoadd.containerbox.Assign_renderer(Get_renderer());
                    newoptiontoadd.text.Assign_renderer(Get_renderer());
                }

                newoptiontoadd.containerbox.Set_fill_color(Get_box_color());
                newoptiontoadd.text.Set_string(option);

                if(Is_text_chars_set()){
                    Set_text_characteristics_to_text_object(&newoptiontoadd.text);
                }

                jetfuel::draw::Vector2d_int containerboxposition(Get_position());

                containerboxposition.y += Get_size_of_base_box().y;
                containerboxposition.y += Get_size_of_dropdown_options_vector()*
                                          Get_size_of_base_box().y;

                newoptiontoadd.containerbox.Set_position(containerboxposition);
                newoptiontoadd.containerbox.Set_size(Get_size_of_base_box());
                newoptiontoadd.text.Set_position(containerboxposition+10);

                Add_drop_down_option_to_vector(newoptiontoadd);
            }
            Add_option_to_vector(option);
        }

        void Drop_down_box::Check_for_clicks(jetfuel::control::Action
                                             UISinterpreterdata){
            if(!Is_active()){
                if(UISinterpreterdata.action ==
                   Get_UIS_action_to_listen_for()
                   && UISinterpreterdata.inputstate ==
                   UISinterpreterdata.Down &&
                   Get_rect_to_draw().Has_mouse_collided()){
                    Set_active(true);
                }
            }else if(UISinterpreterdata.action ==
                     Get_UIS_action_to_listen_for()
                     && UISinterpreterdata.inputstate ==
                     UISinterpreterdata.Down){
                for(int i = 0; Get_size_of_dropdown_options_vector() > i;
                    ++i){
                    if(Get_dropdown_option_in_vector(i).
                       containerbox.Get_rect_to_draw().Has_mouse_collided()){
                         Set_active_drop_down(i);
                         break;
                    }
                }
                Set_active(false);
            }
        }

        bool Drop_down_box::Draw(){
            if(Is_image_dynamically_loaded()){
                bool dynamicloadingreturnvalue = Load_dynamic_image();

                if(!dynamicloadingreturnvalue){
                    return dynamicloadingreturnvalue;
                }
            }

            bool returnvalue = Draw_base_box();

            if(Is_active() && returnvalue){
                returnvalue = Draw_all_option_boxes();
            }

            return returnvalue;
        }

    } /* namespace gui */
} /* namespace jetfuel */
