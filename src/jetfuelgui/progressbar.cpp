/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "progressbar.h"

namespace jetfuel {
    namespace gui {
    
        Progress_bar::Progress_bar(const jetfuel::draw::Image 
                         progressbarholderimage, 
                         const jetfuel::draw::Color progressbarcolor,
                        const jetfuel::draw::Rect2d_int progressbarholderborders,
                         const unsigned int progressbarmax){
            m_progressbarholder.Dynamic_load_from_image(
                                             progressbarholderimage);

            m_progressbar.Set_fill_color(progressbarcolor);
            m_progressbarholderborders = progressbarholderborders;
            m_progressbarmax = progressbarmax;
            
            m_progressbarholderdynamicloading = true;

            Set_position(Get_position());               
        }
        
        void Progress_bar::Set_progress_bar(const jetfuel::draw::Image 
                         progressbarholderimage, 
                         const jetfuel::draw::Color progressbarcolor,
                        const jetfuel::draw::Rect2d_int progressbarholderborders,
                         const unsigned int progressbarmax){
            m_progressbarholder.Dynamic_load_from_image(
                                         progressbarholderimage);
            m_progressbar.Set_fill_color(progressbarcolor);
            m_progressbarholderborders = progressbarholderborders;
            m_progressbarmax = progressbarmax;
            
            m_progressbarholderdynamicloading = true;

            Set_position(Get_position());                
        }
        
        bool Progress_bar::Draw(){
            if(!Draw_progress_bar_holder()){
                return false;
            }else{
                Mark_progress_bar_size();
            }

            Set_width_of_progress_bar((Get_max_progress_bar_width()
                                      /Get_max_progress_bar())
                                      *Get_progress_bar_progress());
            if(!Draw_progress_bar()){
                return false;
            }
            
            return true;
        }
 
    }
}
