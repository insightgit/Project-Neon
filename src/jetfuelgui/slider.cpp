/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "slider.h"

namespace jetfuel{

    namespace gui{
    
        void Slider::Check_for_clicks(jetfuel::control::Action
                                          UISinterpreterdata){
            if(UISinterpreterdata.inputtype ==
               UISinterpreterdata.Mouse){
                if(UISinterpreterdata.action == Get_control_scheme().
                   mousemessagetowatch &&
                   UISinterpreterdata.inputstate ==
                   UISinterpreterdata.Down &&
                   Get_rect_to_draw().Has_mouse_collided()){
                    // TODO(Bobby): Make this work without
                    // -fpermissive on g++
                    int mousex, mousey;
                    SDL_GetMouseState(&mousex,&mousey);

                    const int currentstatus = (mousex-Get_position().x)/
                                              (Get_width_of_slider_rail()/
                                              Get_number_of_statuses());
                    Set_current_status(currentstatus);
                }
            }                                 
        }
    
        bool Slider::Draw(){
            jetfuel::draw::Vector2d_int sliderbuttonposition = Get_position();
            
            sliderbuttonposition.x += (Get_width_of_slider_rail()/
                                      Get_number_of_statuses())*
                                      Get_current_status();
            Set_slider_button_position(sliderbuttonposition);
            
            return Draw_slider_member_objects();
        }
    
    }

}
