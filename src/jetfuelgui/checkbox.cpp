/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "checkbox.h"

namespace jetfuel {
    namespace gui {
        Check_box::Check_box(bool checked) {
            m_checkboxstatus = checked;
        }

        void Check_box::Check_for_clicks(jetfuel::control::Action
                                         UISinterpreterdata){
            if(UISinterpreterdata.action == Get_action_to_listen_for()
               && UISinterpreterdata.inputstate ==
               UISinterpreterdata.Down &&
               Get_checkbox_rect_to_draw().Has_mouse_collided()){
                Set_checked(!Is_checked());
            }
        }

        bool Check_box::Draw(){
            if(Is_checked()){
                Set_checkbox_sprite_image(Get_active_checkbox_image());
            }else{
                Set_checkbox_sprite_image(Get_disabled_checkbox_image());
            }

            return Draw_drawables();
        }
    } /* namespace gui */
} /* namespace jetfuel */
