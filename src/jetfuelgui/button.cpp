/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "button.h"

namespace jetfuel {
    namespace gui {
        void Button::Set_button_color(jetfuel::draw::Color color){
            m_buttonoverlay.Set_fill_color(color);
        }

        void Button::Assign_renderer(SDL_Renderer *renderer) {
            if(renderer != nullptr){
                m_buttonsprite.Assign_renderer(renderer);
                m_buttonoverlay.Assign_renderer(renderer);
                m_buttontext.Assign_renderer(renderer);
                m_renderready = true;

                Set_assigned_renderer(true);
            }
        }

        void Button::Check_for_clicks(jetfuel::control::Action
                                      UISinterpreterdata){
            if(UISinterpreterdata.action == m_actiontolistenfor
               && UISinterpreterdata.inputstate ==
               UISinterpreterdata.Down &&
               UISinterpreterdata.inputtype == UISinterpreterdata.Mouse
               && Get_rect_to_draw().Has_mouse_collided()){
                if(m_messagebus != nullptr){
                    m_messagebus->Post_message(m_messagetopostonclick);
                }
            }
        }

        bool Button::Draw(){
            if(Is_ready_to_draw() && Is_render_ready()){
                if(Draw_button_sprite()){
                    if(Draw_button_overlay()){
                        if(Draw_button_text()){
                            return true;
                        }
                    }
                    return false;
                }

            }

            return false;
        }
    } /* namespace gui */
} /* namespace jetfuel */
