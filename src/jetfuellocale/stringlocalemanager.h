/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELLOCALE_STRINGLOCALEMANAGER_H
#define JETFUELLOCALE_STRINGLOCALEMANAGER_H

#include <jetfuellocale/stringlocalefile.h>

namespace jetfuel {
    namespace locale {

        class String_locale_manager {
        public:
            String_locale_manager(){}

            bool Load_string_locale_file(String_locale_file localefile){
                if(localefile.Is_locale_file_set()){
                    m_localefiles.push_back(localefile);

                    return true;
                }

                return false;
            }

            std::string Get_active_locale(){
                return m_activelocale;
            }

            String_locale_file Get_active_string_locale_file() const{
                return *m_currentlocalefile;
            }

            bool Set_active_locale(const std::string localename){
                bool foundlocale = false;

                for(int i = 0; m_localefiles.size() > i; ++i){
                    if(localename == m_localefiles[i].
                       Get_string_locale_file_locale_name()){
                        m_currentlocalefile = &m_localefiles[i];
                        m_activelocale =
                         m_currentlocalefile->
                         Get_string_locale_file_locale_name();
                        foundlocale=true;
                        break;
                    }
                }

                return foundlocale;
            }

            std::string Get_string_from_id(const std::string stringid) const{
                std::vector<Locale_string> stringidindex = m_currentlocalefile->
                                                      Get_locale_string_vector();
                for(int i = 0; stringidindex.size() > i; ++i){
                    if(stringidindex[i].stringid == stringid){
                        return stringidindex[i].string;
                    }
                }

                return "";
            }
        private:
            std::string m_activelocale = "NoLocale";

            std::vector<String_locale_file> m_localefiles;

            String_locale_file *m_currentlocalefile;
        };

    } /* namespace locale */
} /* namespace jetfuel */

#endif /* JETFUELLOCALE_STRINGLOCALEMANAGER_H */
