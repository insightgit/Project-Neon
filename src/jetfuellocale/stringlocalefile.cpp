/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <jetfuellocale/stringlocalefile.h>

namespace jetfuel {
    namespace locale {

        bool String_locale_file::Load_string_locale_file(
                const std::string filename,
                const std::string localename,
                std::string *error){
            rapidjson::Document jsondoc;

            m_filename = filename;

            if(localename != "NoLocale"){
                m_localename = localename;
            }else{
                *error = "Cannot name locale \"NoLocale\".";
                return false;
            }

            std::string jsonfilecontents =
                    Convert_json_file_to_string(m_filename);

            jsondoc.Parse(jsonfilecontents.c_str());

            Set_locale_file_set(false);

            Clear_locale_string_vector();

            if(jsondoc.IsObject()){
                if(jsondoc["strings"].IsArray()){
                    for(rapidjson::SizeType i = 0; jsondoc["strings"].Size() > i;
                        ++i){
                        if(jsondoc["strings"][i]["stringid"].IsString()){
                            if(jsondoc["strings"][i]["string"].IsString()){
                                Locale_string currentlocalestring;
                                currentlocalestring.stringid =
                                 jsondoc["strings"][i]["stringid"].GetString();
                                currentlocalestring.string =
                                 jsondoc["strings"][i]["string"].GetString();

                                Push_back_locale_string_into_vector(
                                                            currentlocalestring);
                            }else{
                                *error =
                                 std::string("Could not find \"string\"")+
                                 "JSON String in "+mingwtostringpatch
                                 ::to_string(i)+
                                 " array position";
                                return false;
                            }
                        }else{
                            *error =
                             std::string("Could not find \"stringid\"")+
                             "JSON String in "+mingwtostringpatch
                             ::to_string(i)+
                             " array position";
                            return false;
                        }
                    }

                    Set_locale_file_set(true);
                    return true;
                }else{
                    *error = "No \"strings\" JSON array found";
                }
            }else{
                *error = "No root JSON object found";
            }

            return false;
        }
    } /* namespace locale */
} /* namespace jetfuel */
