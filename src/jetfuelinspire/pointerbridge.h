/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JETFUELINSPIRE_POINTERBRIDGE_H_
#define JETFUELINSPIRE_POINTERBRIDGE_H_

#include <vector>
#include <string>

#include <iostream>

#include <mutex>

namespace jetfuel {
    namespace inspire {
        class Pointer_bridge {
        public:
            Pointer_bridge() {}

            void *Recieve_pointer(std::string id, bool *found){
                m_vectormutex.lock();

                for(int i = 0; m_pointerids.size() > i; ++i){
                    if(id == m_pointerids[i]){
                        void *returnvalue = m_pointers[i];

                        std::clog << "Pointer recieved. ID="+id+"\n";

                        m_pointers.erase(m_pointers.begin()+i);
                        m_pointerids.erase(m_pointerids.begin()+i);

                        *found = true;

                        m_vectormutex.unlock();

                        return returnvalue;
                    }
                }

                m_vectormutex.unlock();

                *found = false;

                return nullptr;
            }

            void Send_pointer(std::string id, void *pointer){
                m_vectormutex.lock();

                std::clog << "Pointer sent. ID="+id+"\n";

                m_pointers.push_back(pointer);
                m_pointerids.push_back(id);

                m_vectormutex.unlock();
            }
        private:
            std::mutex m_vectormutex;

            std::vector<std::string> m_pointerids;

            std::vector<void*> m_pointers;
        };
    } /* namespace inspire */
} /* namespace jetfuel */

#endif /* JETFUELINSPIRE_POINTERBRIDGE_H_ */
