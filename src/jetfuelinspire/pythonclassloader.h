/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef JETFUELINSPIRE_PYTHONCLASSLOADER_H_
#define JETFUELINSPIRE_PYTHONCLASSLOADER_H_

#ifdef _WIN32
    #include <Python.h>
#else
    #include <python3.6/Python.h>
#endif

#include <jetfuelinspire/pythonmoduleloader.h>

namespace jetfuel {
    namespace inspire {
        struct Python_class{
            PyObject *constructorargs;
            std::string filename;
            std::string classname;
            std::string directoryoffilename;
        };

        class Python_class_loader {
        public:
            Python_class_loader(PyObject *constructorargs, std::string *filename,
                                std::string *classname, bool *executed, std::string *error,
                                std::string *directoryoffilename);

            Python_class_loader(Python_class pythonclass, bool *executed,
                                std::string *error);

            virtual ~Python_class_loader();

            PyObject *pythonclass;

        private:
            void Base_execute_py_function(PyObject *pythonfunction, PyObject *args, bool *executed,
                                          std::string *error);

            const wchar_t *Get_wchar(const char *chartouse)
            {
                const size_t charsize = strlen(chartouse)+1;
                wchar_t* widechar = new wchar_t[charsize];
                mbstowcs (widechar, chartouse, charsize);

                return widechar;
            }
        };
    } /* namespace inspire */
} /* namespace jetfuel */

#endif /* JETFUELINSPIRE_PYTHONCLASSLOADER_H_ */
