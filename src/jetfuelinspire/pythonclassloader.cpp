/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "pythonclassloader.h"

namespace jetfuel {
    namespace inspire {
        void Python_class_loader::Base_execute_py_function(PyObject *pythonfunction, PyObject *args, bool *executed,
                                                           std::string *error){
            if(pythonfunction==NULL){
                PyObject *pythonerrortype, *pythonerrorvalue, *pythonerrortraceback;
                PyErr_Fetch(&pythonerrortype, &pythonerrorvalue, &pythonerrortraceback);

                *error = Python_module_loader::Py_err_to_cstring(pythonerrortype,pythonerrorvalue,pythonerrortraceback);
                *executed = false;
                return;
            }
            if(!PyCallable_Check(pythonfunction)){
                PyObject *pythonerrortype, *pythonerrorvalue, *pythonerrortraceback;
                PyErr_Fetch(&pythonerrortype, &pythonerrorvalue, &pythonerrortraceback);

                *executed = false;
                *error = Python_module_loader::Py_err_to_cstring(pythonerrortype,pythonerrorvalue,pythonerrortraceback);
                return;
            }
            *executed=true;
        }

        Python_class_loader::Python_class_loader(PyObject *constructorargs, std::string *filename,
                                                std::string *classname, bool *executed, std::string *error,
                                                std::string *directoryoffilename) {
            Py_Initialize();

            Python_module_loader::Add_to_py_path(*directoryoffilename);

            PyObject *pythonfile, *pythonmodule, *pythonfunction;
            pythonfile = PyUnicode_DecodeFSDefault(filename->c_str());
            pythonmodule = PyImport_Import(pythonfile);
            pythonfunction = PyObject_GetAttrString(pythonmodule,classname->c_str());

            Base_execute_py_function(pythonfunction,constructorargs,executed,error);

            if(!*executed){
                return;
            }

            pythonclass = PyObject_CallObject(pythonfunction,constructorargs);

            PyObject *pythonerrortype, *pythonerrorvalue, *pythonerrortraceback;
            PyErr_Fetch(&pythonerrortype, &pythonerrorvalue, &pythonerrortraceback);

            if(pythonerrortype==NULL){
                *executed = true;
            }else{
                *error = Python_module_loader::Py_err_to_cstring(pythonerrortype,pythonerrorvalue,pythonerrortraceback);
                *executed = false;
            }
        }

        Python_class_loader::Python_class_loader(Python_class pythonclasstouse
                                                 , bool *executed,
                                                 std::string *error){
            Py_Initialize();

            Python_module_loader::Add_to_py_path(pythonclasstouse.directoryoffilename);

            PyObject *pythonfile, *pythonmodule, *pythonfunction;
            pythonfile = PyUnicode_DecodeFSDefault(pythonclasstouse.filename.c_str());
            pythonmodule = PyImport_Import(pythonfile);
            pythonfunction = PyObject_GetAttrString(pythonmodule,
                                                    pythonclasstouse.classname.c_str());

            Base_execute_py_function(pythonfunction,pythonclasstouse.constructorargs,
                                     executed,error);

            if(!*executed){
                return;
            }

            pythonclass = PyObject_CallObject(pythonfunction,
                                              pythonclasstouse.constructorargs);

            PyObject *pythonerrortype, *pythonerrorvalue, *pythonerrortraceback;
            PyErr_Fetch(&pythonerrortype, &pythonerrorvalue,
                       &pythonerrortraceback);

            if(pythonerrortype==NULL){
                *executed = true;
            }else{
                *error = Python_module_loader::Py_err_to_cstring(pythonerrortype,
                                                                 pythonerrorvalue,
                                                                 pythonerrortraceback);
                *executed = false;
            }
        }

        Python_class_loader::~Python_class_loader() {
            Py_Finalize();
        }
    } /* namespace inspire */
} /* namespace jetfuel */
