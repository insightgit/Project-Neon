/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H_
#define CONFIG_H_

// This is the configuration header file. Preprocessor macros are defined here.
// These macros provide various useful stuff, including the library version number,
// the Operating System, and whether the program was compiled in Debug mode.

// Version number definitions
#define NEON_VERSION_MAJOR 0
#define NEON_VERSION_MINOR 1
#define NEON_VERSION_PATCH 0

// Operating System definitions

#if defined(_WIN32)
    #define NEON_COMPILED_OS "Windows"
#elif defined(unix) || defined(__unix__) || defined(__unix)
  #if defined(__APPLE__) || defined(__MACH__)
    #include "TargetConditionals.h"
    #if defined(TARGET_IPHONE_SIMULATOR)
         #define NEON_COMPILED_OS "iOS Simulator"
    #elif defined(TARGET_OS_IPHONE)
        #define NEON_COMPILED_OS "iOS"
    #elif defined(TARGET_OS_MAC)
          #define NEON_COMPILED_OS "macOS"
    #else
        #error "Unknown Apple platform"
    #endif

  #elif defined(__linux__) || defined(linux) || defined(__linux)
    #if defined(__ANDROID__)
        #define NEON_COMPILED_OS "Android"
    #else
        #define NEON_COMPILED_OS "Linux"
    #endif
  #elif defined(__FreeBSD__)
    #define NEON_COMPILED_OS "FreeBSD"
  #endif
#else
    #error "Unknown platform"
#endif


#endif /* CONFIG_H_ */
