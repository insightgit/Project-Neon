################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/neondraw/circle2dshape.cpp \
../src/neondraw/color.cpp \
../src/neondraw/font.cpp \
../src/neondraw/image.cpp \
../src/neondraw/rectangle2dshape.cpp \
../src/neondraw/scene.cpp \
../src/neondraw/scenemanager.cpp \
../src/neondraw/sprite.cpp \
../src/neondraw/text.cpp 

OBJS += \
./src/neondraw/circle2dshape.o \
./src/neondraw/color.o \
./src/neondraw/font.o \
./src/neondraw/image.o \
./src/neondraw/rectangle2dshape.o \
./src/neondraw/scene.o \
./src/neondraw/scenemanager.o \
./src/neondraw/sprite.o \
./src/neondraw/text.o 

CPP_DEPS += \
./src/neondraw/circle2dshape.d \
./src/neondraw/color.d \
./src/neondraw/font.d \
./src/neondraw/image.d \
./src/neondraw/rectangle2dshape.d \
./src/neondraw/scene.d \
./src/neondraw/scenemanager.d \
./src/neondraw/sprite.d \
./src/neondraw/text.d 


# Each subdirectory must supply rules for building sources it contributes
src/neondraw/%.o: ../src/neondraw/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I"/media/bobby/PVSERVER01/CentralProjectStorage/eclipseworkspace/Project Neon/src" -O0 -g3 -Wall -c -fmessage-length=0 -fpermissive -Werror=suggest-override -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


