/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <neondraw.h>

extern "C" {
    // Vector2d (only int) wrapper

    neon::draw::Vector2d_int *Vector2d_int_new() {
        return new neon::draw::Vector2d_int();
    }

    /*int Vector2d_int_get_x(neon::draw::Vector2d_int *vector2dint){
        return vector2dint->x;
    }

    void Vector2d_int_set_x(neon::draw::Vector2d_int *vector2dint,
                            const int x) {
        vector2dint->x = x;
    }

    int Vector2d_int_get_y(neon::draw::Vector2d_int *vector2dint){
        return vector2dint->y;
    }

    void Vector2d_int_set_y(neon::draw::Vector2d_int *vector2dint,
                            const int y) {
        vector2dint->y = y;
    }*/

    // Image wrapper

    neon::draw::Image *Image_new(){
        return new neon::draw::Image();
    }

    void Image_set_image(neon::draw::Image *image,
                         const std::string imagepath,
                         neon::draw::Vector2d_int *imagesize){
        image->Set_image(imagepath, *imagesize);
    }

    std::string Image_get_image_location(neon::draw::Image *image) {
        return image->Get_image_location();
    }

    neon::draw::Vector2d_int Get_image_size(neon::draw::Image *image) {
        return image->Get_size_of_image();
    }

    // Color wrapper

    neon::draw::Color *Color_new() {
        return new neon::draw::Color();
    }

    // Font wrapper

    neon::draw::Font *Font_new(){
        return new neon::draw::Font();
    }

    bool Font_is_loaded(neon::draw::Font *font){
        return font->Is_font_loaded();
    }

    std::string Font_get_file_name(neon::draw::Font *font) {
        return font->Get_file_name();
    }

    long Font_get_face_index(neon::draw::Font *font) {
        return font->Get_face_index();
    }

    void Font_load_font(neon::draw::Font *font,const std::string filename){
        font->Load_font(filename);
    }

    void Font_load_font_index(neon::draw::Font *font,const std::string filename,
                        const long faceindex){
        font->Load_font(filename, faceindex);
    }

    void Font_change_size(neon::draw::Font *font, const int size){
        font->Change_size(size);
    }
}
