/*
 *   Project Neon- A moddable 2D RPG game engine
 *   Copyright (C) 2017 InfernoStudios
 *
 *  Project Neon is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Project Neon is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Project Neon.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "button.h"


extern "C" {
    //Button wrapper
    neon::gui::Button *Button_new() {
        return new neon::gui::Button();
    }

    bool Button_load_base_button_image(neon::gui::Button *button,
                                       neon::draw::Image *image,
                                       const int border) {
        return button->Load_base_button_image(*image, border);
    }

    void Button_set_button_color(neon::gui::Button *button,
                                 neon::draw::Color *color) {
        button->Set_button_color(*color);
    }

    void Button_set_button_font(neon::gui::Button *button,
                               neon::draw::Font *font) {
        button->Set_button_font(*font);
    }

    void Button_set_button_text_render_mode(neon::gui::Button *button,
                                           const unsigned int rendermode) {
        if(rendermode == 0) {
            button->Set_button_text_render_mode(neon::draw::Text::Render_mode::Solid);
        }else if(rendermode == 1){
            button->Set_button_text_render_mode(neon::draw::Text::Render_mode::Shaded);
        }else if(rendermode == 2){
            button->Set_button_text_render_mode(neon::draw::Text::Render_mode::Blended);
        }
    }

    void Button_set_button_font_size(neon::gui::Button *button,
                                     const unsigned int fontsize) {
        button->Set_button_font_size(fontsize);
    }

    void Button_set_button_string(neon::gui::Button *button,
                                  const std::string string) {
        button->Set_button_text_string(string);
    }

    void Button_set_clicked_message(neon::gui::Button *button,
                                    const std::string clickedmessage) {
        button->Set_clicked_message(clickedmessage);
    }
}
