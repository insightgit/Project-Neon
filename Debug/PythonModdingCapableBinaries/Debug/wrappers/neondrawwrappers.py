from ctypes import cdll
engine = cdll.LoadLibrary("../libProject Neon Python API.so");

class Vector2d_int(object):
    def __init__(self):
        self.obj = engine.Vector2d_int_new();

class Color(object):
    def __init__(self):
        self.obj = engine.Color_new();

class Font(object):
    def __init__(self):
        self.obj = engine.Font_new();
    def is_loaded(self):
        return engine.Font_is_loaded(self.obj);
    def get_file_name(self):
        return engine.Font_get_file_name(self.obj);
    def get_face_index(self):
        return engine.Font_get_face_index(self.obj);
    def load_font(self, filename):
        engine.Font_load_font(self.obj, filename);
    def load_font_index(self, filename, faceindex):
        engine.Font_load_font_index(self.obj, filename, faceindex);
    def change_size(self, size):
        engine.Font_change_size(self.obj, size);

class Image(object):
    def __init__(self):
        self.obj = engine.Image_new();
    def set_image(self, imagepath, imagesize):
        engine.Image_set_image(self.obj, imagepath, imagesize);
    def get_image_location(self):
        engine.Get_image_location(self.obj);
    def get_image_size(self):
        engine.Get_image_size(self.obj);