from ctypes import cdll
engine = cdll.LoadLibrary("../libProject Neon Python API.so");


class Button(object):
    def __init__(self):
        self.obj = engine.Button_new();

    def load_base_button_image(self,image,border):
         return engine.Button_load_base_button_image(self.obj, image, border);

    def set_button_color(self,color):
        engine.Button_set_button_color(self.obj, color);

    def set_button_font(self,font):
        engine.Button_set_button_font(self.obj, font);

    def set_button_text_render_mode(self,rendermode):
        engine.Button_set_button_text_render_mode(self.obj, rendermode);

    def set_button_font_size(self,fontsize):
        engine.Button_set_button_font_size(self.obj, fontsize);

    def set_button_string(self, string):
        engine.Button_set_button_string(self.obj, string);

    def set_clicked_message(self, clickedmessage):
        engine.Button_set_clicked_message(self.obj, clickedmessage);